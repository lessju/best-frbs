#include <iostream>
#include <unistd.h>
#include <fcntl.h>
#include <getopt.h>
#include <ctime>

#include <cstdlib>

#include "DAQ.h"
#include "Filterbank.h"

using namespace std;
using namespace chrono;

// ---- Paramters which can be updated through command line ------
std::string base_directory = "/data/";
std::string interface = "eth1";
std::string ip = "10.0.10.100";
unsigned int port = 4660;

uint32_t nof_samples    = 109 * 6000 * 2;
uint16_t nof_channels   = 2;
uint16_t nof_antennas   = 64;

// ---- Runtime tracking: Do not change ------
int channel_fd = 0;
int channel_counter = 0;
int current_channel_id = -1;


void pharos_channel_callback(void *data, double timestamp, unsigned int channel_id, unsigned int nof_samples)
{
    // Check if we have started a new channel
    if (current_channel_id == -1)
        current_channel_id = channel_id;
    else if (current_channel_id != channel_id) {
        current_channel_id = channel_id;
        channel_counter = 0;
    }

    if (++channel_counter > 2)
    {
        // Create output file for this channel
        if (channel_counter == 3) {
            std::string path = base_directory + "channel_" + std::to_string(channel_id) + "_" + std::to_string(timestamp) + ".dat";

            channel_fd = open(path.c_str(), O_WRONLY | O_CREAT | O_SYNC | O_TRUNC, (mode_t) 0600);

            // Tell the kernel how the file is going to be accessed (sequentially)
            posix_fadvise(channel_fd, 0, 0, POSIX_FADV_SEQUENTIAL);
        }

        if (write(channel_fd, data, nof_antennas * nof_channels * nof_samples * sizeof(uint16_t)) < 0)
        {
            perror("Failed to write buffer to disk");
            fsync(channel_fd);
            close(channel_fd);
            exit(-1);
        }

        auto now = std::chrono::system_clock::now();
	    auto datetime = std::chrono::system_clock::to_time_t(now);
	    auto date_text = strtok(ctime(&datetime), "\n");
	    cout << date_text <<  ": Written channel " << channel_id << " to disk [" << nof_samples << " samples]" << endl;
    }
    else {

        auto now = std::chrono::system_clock::now();
	    auto datetime = std::chrono::system_clock::to_time_t(now);
	    auto date_text = strtok(ctime(&datetime), "\n");
	    cout << date_text <<  ": Ignoring first two buffers" << endl;
    }
}

void continuous_data()
{
    startReceiver(interface.c_str(), ip.c_str(), 8000, 1024, 256);
    addReceiverPort(4660);

    json j = {
            {"nof_channels", nof_channels},
            {"nof_samples", nof_samples},
            {"nof_antennas", nof_antennas},
            {"nof_antennas_per_tile", 32},
            {"max_packet_size", 9000}
    };

    loadConsumer("/opt/frbs/lib/libpharos.so", "pharoschannelised");
    initialiseConsumer("pharoschannelised", j.dump().c_str());
    startConsumer("pharoschannelised", pharos_channel_callback);
}

static void print_usage(char *name)
{
std::cerr << "Usage: " << name << " <option(s)>\n"
          << "Options:\n"
          << "\t-d DIRECTORY \t\tBase directory where to store data\n"
          << "\t-c NOF_CHANNELS\tNumber of channels\n"
          << "\t-a NOF_ANTENNAS\tNumber of antennas\n"
          << "\t-s NOF_SAMPLES\tNumber of samples\n"
          << "\t-e ETHERNET_INTERFACE\tNetwork interface\n"
          << "\t-i IP\tInterface IP\n"
          << "\t-p PORT\tPORT\n"
          << std::endl;
}

// Parse command line arguments
void parse_arguments(int argc, char *argv[])
{
    int opt;
    while ((opt = getopt(argc, argv, "a:d:s:c:e:i:p:")) != -1) {
        switch (opt) {
            case 'd':
                base_directory = string(optarg);
                break;
            case 'a':
                nof_antennas = (uint32_t) atoi(optarg);
                break;
            case 's':
                nof_samples = (uint32_t) atoi(optarg);
                break;
            case 'c':
                nof_channels = (uint16_t) atoi(optarg);
                break;
            case 'e':
                interface =  string(optarg);;
                break;
            case 'i':
                ip = string(optarg);
                break;
            case 'p':
                port = (uint32_t) atoi(optarg);
                break;
            default: /* '?' */
                print_usage(argv[0]);
                exit(EXIT_FAILURE);
        }
    }

    printf("Running pharos_channel with %d samples, %d channels and saving in directory %s\n",
            nof_samples, nof_channels, base_directory.c_str());
}

// Program entry point
int main(int argc, char *argv[])
{
    // Process command line arguments
    parse_arguments(argc, argv);

    startReceiver(interface.c_str(), ip.c_str(), 8192, 1024, 256);
    addReceiverPort(port);

    continuous_data();

    sleep(100000);
}

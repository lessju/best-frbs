#include <iostream>
#include <unistd.h>
#include <fcntl.h>

#include "DAQ.h"
#include "Filterbank.h"

using namespace std;

#define NORTHERN_CROSS_TELECOPE_ID  30;

// ---- Parameters which can be updated through command line ------
std::string base_directory = "/data/";
std::string interface = "eth1";
std::string ip = "10.0.10.100";
unsigned int port = 4661;

uint32_t nof_samples    = 11 * 100000;
uint16_t nof_channels   = 352;
uint16_t nof_tiles      = 2;

// Flag to determine whether beams will be combined
bool combine_beams = false;

// Channel selection
uint16_t start_channel  = 0;
uint16_t stop_channel   = 351;

// ---- Do not change ------

// Channel bandwidth
double channel_bandwidth = 400 / 512.0;

// Filterbank object
std::vector<Filterbank *> filterbank;

// Create buffer to store data when saving a subset of channels
std::vector<void *> temp_data_buffer;

// Runtime tracking
int counter = 0;

// Keep track of timestamp, as it is used to determine whether buffer belong to the same
// buffer originating from different tiles
double current_timestamp;

// Generate file header for filterbank file
void generate_filterbank_header(FILE_HEADER *header, double timestamp)
{
    // Write filterbank header
    header->fch1 = channel_bandwidth * start_channel;
    header->foff = channel_bandwidth;
    header->nchans = stop_channel - start_channel + 1;
    header->machine_id = 1;
    header->telescope_id = NORTHERN_CROSS_TELECOPE_ID;
    header->src_raj = 1;
    header->src_dej = 1;
    header->az_start = 1;
    header->za_start = 1;
    header->nbeams = 1;
    header->ibeam = 1;
    header->tstart = timestamp;
    header->tsamp = 1.08e-6;
}

// Initialise files for storing beams
void __inline__ initialise_files(double timestamp)
{
    int nof_subset_channels = stop_channel - start_channel + 1; // Number of channels to write

    if (combine_beams) {
        // Generate file path
        auto path = base_directory + "beam_" + std::to_string(timestamp) + ".fil";
        filterbank.push_back(new Filterbank(path, 32));

        // Generate default filterbank header
        FILE_HEADER header;
        generate_filterbank_header(&header, timestamp);
        filterbank[0]->writeHeader(&header);

        // Advise sequential access for file
        posix_fadvise(filterbank[0]->getFileDescriptor(), 0, 0, POSIX_FADV_SEQUENTIAL);

        // Allocate temporary buffer if not all channels are being written to disk
        if (nof_subset_channels != nof_channels) {
            // Increase the size of the datatype to avoid overflow
            auto *ptr = malloc(nof_subset_channels * nof_samples * sizeof(uint32_t));
            temp_data_buffer.push_back(ptr);
        }

        return;
    }

    // Generate a file for each tile when not combining beams
    for(unsigned i = 0; i < nof_tiles; i++) {

        // Generate filename and initialise filterbank file
        auto path = base_directory + "tile_" + std::to_string(i) + "_beam_" + std::to_string(timestamp) + ".fil";
            filterbank.push_back(new Filterbank(path, 16));

        // Generate filterbank file
        FILE_HEADER header;
        generate_filterbank_header(&header, timestamp);
        filterbank[i]->writeHeader(&header);

        // Advise sequential access for file
        posix_fadvise(filterbank[i]->getFileDescriptor(), 0, 0, POSIX_FADV_SEQUENTIAL);

        // If only saving a subset of the channel, create buffer
        if (nof_subset_channels != nof_channels) {
            auto *ptr = malloc(nof_subset_channels * nof_samples * sizeof(uint16_t));
            temp_data_buffer.push_back(ptr);
        }
    }
}

// Combine tile beams into station beam
void __inline__ combine_tile_beams(void *data, unsigned int tile)
{
    // Number of channels to write
    int nof_subset_channels = stop_channel - start_channel + 1;

    // Generate station beam
    if (combine_beams) {
        // Helper pointer to source and destination buffers
        auto *buffer = reinterpret_cast<signed short *>(temp_data_buffer[0]);
        auto *data_ptr = reinterpret_cast<signed char *>(data);

        // Loop over all values in array (x2 for complex components)
        for (unsigned i = 0; i < 2 * nof_samples * nof_subset_channels; ++i)
            buffer[i] += static_cast<signed short>(data_ptr[i]);
    }

    // Store each tile separately
    else
        memcpy(temp_data_buffer[tile], data, nof_samples * nof_subset_channels * sizeof(uint16_t));
}

// Write beams to disk
void __inline__ write_beams_to_disk(unsigned int nof_packets)
{
    // Number of channels to write
    int nof_subset_channels = stop_channel - start_channel + 1;

    // Determine data type size factor
    int data_type_size = sizeof(uint16_t);
    if (combine_beams)
        data_type_size = sizeof(uint32_t);

    // Write combined beam
    if (combine_beams) {
        if (filterbank[0]->writeBlock(temp_data_buffer[0],
                                      nof_subset_channels * nof_samples * data_type_size) < 0) {
            printf("Failed to write buffer to disk");
            filterbank[0]->closeFile();
            exit(0);
        }

        // Clear temporary buffer
        memset(temp_data_buffer[0], 0, nof_subset_channels * nof_samples * data_type_size);

        // Sync filterbank file
        filterbank[0]->syncFile();
    }
    else {

        // Write individual beams
        for (unsigned i = 0; i < nof_tiles; i++) {

            if (filterbank[i]->writeBlock(temp_data_buffer[i],
                                          nof_subset_channels * nof_samples * data_type_size) < 0) {
                perror("Failed to write buffer to disk");
                filterbank[i]->closeFile();
                exit(0);
            }

            // Clear temporary buffer
            memset(temp_data_buffer[i], 0, nof_subset_channels * nof_samples * data_type_size);

            // Sync filterbank file
            filterbank[i]->syncFile();
        }
    }
    // Get time for timing
    auto now = std::chrono::system_clock::now();
    auto datetime = std::chrono::system_clock::to_time_t(now);
    auto date_text = strtok(ctime(&datetime), "\n");

    cout << date_text << ": Written beam buffer to disk (" << nof_packets << " packets)" << endl;
}

// PHAROS beamformed data callback
void pharos_beam_callback(void *data, double timestamp, unsigned int tile, unsigned int nof_packets)
{
    // Skip first two iteration
    if (++counter <= 3 * nof_tiles) {
       return;
    }

    // If tile is invalid, ignore
    if (tile == numeric_limits<uint16_t>::max()) {
        // printf("Ignoring data from invalid tile %d. Check station configuration [%lf]\n", tile, timestamp);
        return;
    }

    // Create output file(s) once initial buffers are ignored
    if (counter == 3 * nof_tiles + 1)
        initialise_files(timestamp);

    // If timestamp is not initialised, initialise it with current value
    if (current_timestamp == 0)
      current_timestamp = timestamp;

    // Check timestamp (this callback will be called once per TPM, so a change in timestamp corresponds
    // to a change in acquisition buffer, meaning that we can write all accumulated buffers to disk
    if (timestamp > current_timestamp) {
        // Skipped timestamp boundaries, save temporary buffer to disk and reset_buffer
        current_timestamp = timestamp;
        write_beams_to_disk(nof_packets);
    }

    // Write data to buffer
    combine_tile_beams(data, tile);

    // Increment counter
    counter++;
}

void beamformed_data()
{
    json j = {
            {"nof_tiles", nof_tiles},
            {"total_nof_channels", nof_channels},
            {"nof_samples", nof_samples},
            {"nof_channels", stop_channel - start_channel + 1},
            {"start_channel", start_channel},
            {"max_packet_size", 8192}
    };

    loadConsumer("/opt/frbs/lib/libpharos.so", "pharosburstbeam");
    initialiseConsumer("pharosburstbeam", j.dump().c_str());
    startConsumer("pharosburstbeam", pharos_beam_callback);
}

static void print_usage(char *name)
{
    std::cerr << "Usage: " << name << " <option(s)>\n"
              << "Options:\n"
              << "\t-d DIRECTORY \t\t\tBase directory where to store data\n"
              << "\t-c NOF_CHANNELS\t\t\tNumber of channels\n"
              << "\t-s NOF_SAMPLES\t\t\tNumber of samples\n"
              << "\t-t NOF_TILES\t\t\tNumber of tiles\n"
              << "\t-f FIRST_CHANNEL\t\tStart channel (to write to disk)\n"
              << "\t-l LAST_CHANNEL\t\t\tStop channel (to write to disk)\n"
              << "\t-e ETHERNET_INTERFACE\tNetwork interface\n"
              << "\t-i IP\t\t\t\tInterface IP\n"
              << "\t-p PORT\t\t\t\tPORT\n"
              << "\t-b\t\t\t\tCombine tile beams to for a single station\n"
              << std::endl;
}

// Parse command line arguments
void parse_arguments(int argc, char *argv[])
{
    int opt;
    while ((opt = getopt(argc, argv, "d:s:c:t:f:l:e:i:p:b")) != -1) {
        switch (opt) {
            case 'd':
                base_directory = std::string(optarg);
                break;
            case 's':
                nof_samples = (uint32_t) atoi(optarg);
                break;
            case 'c':
                nof_channels = (uint16_t) atoi(optarg);
                break;
            case 't':
                nof_tiles = (uint16_t) atoi(optarg);
                break;
            case 'f':
                start_channel = (uint16_t) atoi(optarg);
                break;
            case 'l':
                stop_channel = (uint16_t) atoi(optarg);
                break;
            case 'e':
                interface =  string(optarg);;
                break;
            case 'i':
                ip = string(optarg);
                break;
            case 'p':
                port = (uint32_t) atoi(optarg);
                break;
            case 'b':
                combine_beams = true;
                break;
            default: /* '?' */
                print_usage(argv[0]);
                exit(EXIT_FAILURE);
        }
    }

    // If combine is enabled but only using 1 tile, disable it
    if (combine_beams && nof_tiles == 1)
        combine_beams = false;

    printf("Running pharos_beam with %d samples, %d channels (saving from %d to %d) and saving in directory %s\n",
           nof_samples, nof_channels, start_channel, stop_channel, base_directory.c_str());

    if (combine_beams)
        printf("All tile (%d) beams will be combined\n", nof_tiles);
    else
        printf("Tile beams (%d) will be stored separately to disk\n", nof_tiles);
}


int main(int argc, char* argv[])
{
    // Process command line arguments
    parse_arguments(argc, argv);

    startReceiver(interface.c_str(), ip.c_str(), 8192, 1024, 256);
    addReceiverPort(port);

    beamformed_data();

    sleep(1000000);
}

//
// Created by Alessio Magro on 31/10/2018.
//

#ifndef SIGPROC_WRITER_FILTERBANK_H
#define SIGPROC_WRITER_FILTERBANK_H

#include <cstdio>
#include <string>
#include <cstring>

typedef struct {
    char rawdatafile[80], source_name[80];
    int machine_id, telescope_id, data_type, nchans, nbits, nifs, scan_number;
    int barycentric,pulsarcentric;
    double tstart,mjdobs,tsamp,fch1,foff,refdm,az_start,za_start,src_raj,src_dej;
    double gal_l,gal_b,header_tobs,raw_fch1,raw_foff;
    int nbeams, ibeam;

    double srcl,srcb;
    double ast0, lst0;
    long wapp_scan_number;
    char project[8];
    char culprits[24];
    double analog_power[2];

    double frequency_table[4096];
    long int npuls;

    double period;
    int nbins, itmp;
    int total_bytes;

} FILE_HEADER;

class Filterbank
{

public:

    Filterbank(std::string filepath, uint32_t nbits);

    FILE_HEADER *readHeader();

    int getFileDescriptor();

    void writeHeader(FILE_HEADER *header);

    ssize_t writeBlock(void *block, size_t size);

    void syncFile();

    void closeFile();

private:

    void sendString(const std::string &string);

    void sendFloat(const std::string &name, float value);

    void sendDouble(const std::string &name, double value);

    void sendInt(const std::string &name, int value);

    void sendChar(const std::string &name, char value);

    void sendLong(const std::string &name, long value);

    void sendCoords(double raj, double dej, double az, double za);

    int fd;
    uint32_t nbits;
};

#endif //SIGPROC_WRITER_FILTERBANK_H
//
// Created by Alessio Magro on 14/05/2018.
//

#ifndef BESTFRB_BEAMFORMEDDATA_H
#define BESTFRB_BEAMFORMEDDATA_H

#include <cstdlib>
#include <cstring>
#include <fcntl.h>
#include <unistd.h>
#include <sstream>
#include <cmath>
#include <netinet/in.h>
#include <unordered_map>
#include <cfloat>

#include "DAQ.h"

// ----------------------- Beam Data Container and Helpers ---------------------------------
// Class which will hold the raw antenna data
template <class T> class PharosIntegratedBeamDataContainer
{
public:

    struct BeamStructure
    {
        double   timestamp;
        uint32_t first_sample_index;
        uint32_t nof_packets;
        T*       data;
        uint16_t tile;
    };

    // Class constructor
    PharosIntegratedBeamDataContainer(uint16_t nof_tiles,
                      uint32_t nof_beams,
                      uint32_t nof_samples,
                      uint16_t nof_channels)
    {
        // Set local variables
        this->nof_samples  = nof_samples;
        this->nof_tiles    = nof_tiles;
        this->nof_beams    = nof_beams;
        this->nof_channels = nof_channels;

        // Allocate buffer
        beam_data = (BeamStructure *) malloc(nof_tiles * sizeof(BeamStructure));
        for(unsigned i = 0; i < nof_tiles; i++)
            beam_data[i].data = (T *) malloc(nof_beams * nof_samples * nof_channels * sizeof(T));

        // Reset beam data and information
        clear();

        // Reserve required number of bucket in map
        tile_map.reserve(nof_tiles);
    }

    // Class destructor
    ~PharosIntegratedBeamDataContainer()
    {
        for(unsigned i = 0; i < nof_tiles; i++)
            free(beam_data[i].data);
        free(beam_data);
    }

public:
    // Set callback function
    void setCallback(DataCallback callback)
    {
        this->callback = callback;
    }


    // Add data to buffer
    void add_data(uint16_t tile, uint32_t beam_id, uint16_t start_channel_id, uint16_t nof_included_channels,
                  uint32_t start_sample_index, uint32_t samples,
                  T* data_ptr, double timestamp)
    {
        // Get current tile index
        unsigned int tile_index = 0;
        if (tile_map.find(tile) != tile_map.end())
            tile_index = tile_map[tile];
        else {
            tile_index = (unsigned int) tile_map.size();

            if (tile_index == nof_tiles) {
                LOG(WARN, "Cannot process tile %d, beam consumer configured for %d tiles", tile, nof_tiles);
                return;
            }

            tile_map[tile] = tile_index;
            beam_data[tile_index].tile = tile;
        }

        // Get buffer pointer
        T* pol0_ptr = beam_data[tile_index].data + beam_id * nof_channels * nof_samples + start_sample_index * samples * nof_channels;

        // Copy data to buffer
        for(unsigned i = 0; i < samples; i++)
            for(unsigned j = 0; j < nof_included_channels; j++)
            {
                unsigned int index = i  * nof_channels + start_channel_id + j;
                pol0_ptr[index] = data_ptr[i * nof_included_channels + j];
            }

        // Update timing
        if (beam_data[tile_index].first_sample_index > start_sample_index)
        {
            beam_data[tile_index].timestamp = timestamp;
            beam_data[tile_index].first_sample_index = start_sample_index;
        }
    }

    //  Clear buffer and antenna information
    void clear()
    {
        // Clear buffer, set all content to 0
        for(unsigned i = 0; i < nof_tiles; i++) {
            memset(beam_data[i].data, 0, nof_beams * nof_channels * nof_samples * sizeof(T));

            // Clear BeamStructure
            beam_data[i].first_sample_index = UINT32_MAX;
            beam_data[i].timestamp = 0;
        }
    }

    // Save data to disk
    void persist_container()
    {
        // If a callback is defined, call it and return
        if (callback != NULL)
        {
            // Call callback for every tile (if buffer has some content)
            for(unsigned i = 0; i < nof_tiles; i++)
                if (beam_data[i].first_sample_index != UINT32_MAX)
                    callback((uint32_t *) beam_data[i].data, beam_data[i].timestamp,
                              i, beam_data[i].nof_packets);
            clear();
            return;
        }

        LOG(WARN, "No callback for beam data defined");
    }


private:
    // Parameters
    uint16_t nof_tiles;
    uint32_t nof_samples;
    uint16_t nof_channels;
    uint32_t nof_beams;

    // Tile map
    std::unordered_map<uint16_t, unsigned int> tile_map;

    // Beam data and info
    BeamStructure *beam_data;

    // Callback function
    DataCallback callback = nullptr;
};

template <class T> class PharosBurstBeamDataContainer
{
public:

    struct BeamStructure
    {
        T*       data;
        uint16_t tile;
        uint32_t nof_packets;

    };

    // Class constructor
    PharosBurstBeamDataContainer(uint16_t nof_tiles,
                                 uint32_t nof_samples,
                                 uint16_t nof_channels,
                                 uint16_t total_nof_channels,
                                 uint16_t start_channel)
    {
        // Set local variables
        this->nof_samples  = nof_samples;
        this->nof_tiles    = nof_tiles;
        this->nof_channels = nof_channels;
        this->total_nof_channels = total_nof_channels;
        this->start_channel = start_channel;


        // Allocate buffer
        allocate_aligned((void **) &beam_data, CACHE_ALIGNMENT, nof_tiles * sizeof(BeamStructure));
        for(unsigned i = 0; i < nof_tiles; i++) {
            allocate_aligned((void **) &(beam_data[i].data), CACHE_ALIGNMENT, nof_samples * nof_channels * sizeof(T));
            beam_data[i].tile = numeric_limits<uint16_t>::max();
        }

        // Reset beam data and information
        clear();

        // Reserve required number of buckets in map
        tile_map.reserve(nof_tiles);
    }

    // Class destructor
    ~PharosBurstBeamDataContainer()
    {
        for(unsigned i = 0; i < nof_tiles; i++)
            free(beam_data[i].data);
        free(beam_data);
    }

public:
    // Set callback function
    void setCallback(DataCallback callback)
    {
        this->callback = callback;
    }


    // Add data to buffer
    void add_data(uint16_t tile, uint64_t index, uint64_t samples_in_packet,  T* data_ptr, double timestamp)
    {
        // Get current tile index
        unsigned int tile_index;
        if (tile_map.find(tile) != tile_map.end())
            tile_index = tile_map[tile];
        else {
            tile_index = (unsigned int) tile_map.size();

            if (tile_index == nof_tiles) {
                LOG(WARN, "Cannot process tile %d, beam consumer configured for %d tiles", tile, nof_tiles);
                return;
            }

            tile_map[tile] = tile_index;
            beam_data[tile_index].tile = tile;
        }

        // Copy required channels from packets
        auto *dst = beam_data[tile_index].data + index * samples_in_packet * nof_channels;

        auto *src = data_ptr + start_channel;
        for(unsigned i = 0; i < samples_in_packet; i++) {
            memcpy(dst, src, nof_channels * sizeof(T));
            src += total_nof_channels;
            dst += nof_channels;
        }

        // Update timing
        if (this->timestamp > timestamp) {
            this->timestamp = timestamp;
        }

        // Update number of packets in container
        beam_data[tile_index].nof_packets++;
    }

    //  Clear buffer and antenna information
    void clear()
    {
        // Clear buffer, set all content to 0
        for(unsigned i = 0; i < nof_tiles; i++) {
            memset(beam_data[i].data, 0, nof_channels * nof_samples * sizeof(T));
            beam_data[i].nof_packets = 0;
        }

        // Clear timestamp
        this->timestamp = DBL_MAX;
    }

    // Save data to disk
    void persist_container()
    {
        // If the container contains no data, do not persist
        if (beam_data[0].nof_packets < 2)
            return;

        // If a callback is defined, call it and return
        if (callback != nullptr)
        {
            for(unsigned i = 0; i < nof_tiles; i++) {
                callback(beam_data[i].data, timestamp, beam_data[i].tile, beam_data[i].nof_packets);
            }

            clear();
            return;
        }
        LOG(WARN, "No callback for beam data defined");
    }

private:
    // Parameters
    uint16_t nof_tiles;
    uint32_t nof_samples;
    uint16_t nof_channels;
    uint16_t total_nof_channels;
    uint16_t start_channel;

    // Tile map
    std::unordered_map<uint16_t, unsigned int> tile_map;

    // Timestamp
    double timestamp = DBL_MAX;

    // Beam data and info
    BeamStructure *beam_data;

    // Callback function
    DataCallback callback = nullptr;
};


// This class is responsible for consuming beam SPEAD packets coming out of TPMs
class PharosBeamformedData : public DataConsumer
{
public:

    // Override setDataCallback
    void setCallback(DataCallback callback) override;

    // Initialise consumer
    bool initialiseConsumer(json configuration) override;

protected:
    // Packet filtering function to be passed to network thread
    inline bool packetFilter(unsigned char* udp_packet) override;

    // Grab SPEAD packet from buffer and process
    bool processPacket() override;

    // Function called when a burst stream capture has finished
    void onStreamEnd() override;

private:

    // BeamInformation object
    PharosBurstBeamDataContainer<uint16_t> **containers{};
    unsigned int current_container = 0;
    uint32_t nof_containers = 2;

    // Number of received packets, used for integrate beam data mode
    double reference_time = 0;
    uint32_t rollover_counter = 0;
    unsigned long timestamp_rollover = 0;
    std::vector<uint32_t> reference_counter;

    // Data setup
    uint16_t nof_tiles = 0;           // Number of tiles
    uint16_t nof_channels = 0;        // Number of channels to store
    uint16_t total_nof_channels = 0;  // Total number of channels
    uint16_t start_channel = 0;       // Start channel
    uint32_t nof_samples = 0;         // Number of time samples

    unsigned long long counter = 0;
};


// This class is responsible for consuming integrated beam SPEAD packets coming out of TPMs
class PharosIntegratedBeamformedData : public DataConsumer
{
public:

    // Override setDataCallback
    void setCallback(DataCallback callback) override;

    // Initialise consumer
    bool initialiseConsumer(json configuration) override;

protected:
    // Packet filtering function to be passed to network thread
    inline bool packetFilter(unsigned char* udp_packet) override;

    // Grab SPEAD packet from buffer and process
    bool processPacket() override;

private:

    // AntennaInformation object
    PharosIntegratedBeamDataContainer<uint32_t> *container;

    // Number of received packets, used for integrate beam data mode
    uint16_t received_packets = 0;

    // Keep track of packet counter for buffering
    uint32_t saved_packet_counter = 0;

    // Data setup
    uint16_t nof_beams = 0;           // Number of beams
    uint16_t nof_tiles = 0;           // Number of tiles
    uint16_t nof_channels = 0;        // Number of channels
    uint32_t nof_samples = 0;         // Number of time samples
};

// Expose class factory for ChannelisedData
extern "C" DataConsumer *pharosburstbeam() { return new PharosBeamformedData; }
extern "C" DataConsumer *pharosintegratedbeam() { return new PharosIntegratedBeamformedData; }

#endif // BESTFRB_BEAMFORMEDDATA_H

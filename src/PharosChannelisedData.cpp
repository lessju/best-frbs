//
// Created by Alessio Magro on 22/07/2016.
//

#include "PharosChannelisedData.h"
#include "SPEAD.h"

#include <sys/mman.h>
#include <ctime>

using namespace std;
using namespace chrono;

// -------------------------------------------------------------------------------------------

bool PharosChannelisedData::initialiseConsumer(json configuration)
{
    // Check that all required keys are present
    if (!(key_in_json(configuration, "nof_channels")) &&
        (key_in_json(configuration, "nof_samples")) &&
        (key_in_json(configuration, "nof_antennas")) &&
        (key_in_json(configuration, "nof_antennas_per_tile")) &&
        (key_in_json(configuration, "max_packet_size"))) {
        LOG(FATAL, "Missing configuration item for ChannelisedData consumer. Requires "
                "nof_channels, nof_samples, nof_antennas and max_packet_size");
        return false;
    }

    // Set local values
    nof_channels = configuration["nof_channels"];
    nof_samples = configuration["nof_samples"];
    nof_antennas = configuration["nof_antennas"];
    nof_antennas_per_tpm_tile = configuration["nof_antennas_per_tile"];
    packet_size  = configuration["max_packet_size"];

    // Create ring buffer
    initialiseRingBuffer(packet_size, (size_t) nof_samples);

    // Create double buffer
    double_buffer= new PharosDoubleBuffer(nof_antennas, nof_channels, nof_samples);

    // Create pharos callback_manager and start
    callback_manager = new CallbackManager(double_buffer);

    // Start callback_manager
    callback_manager -> startThread();

    // All done
    return true;
}

// Packet filter
bool PharosChannelisedData::packetFilter(unsigned char *udp_packet)
{
    // Unpack SPEAD Header (or try to)
    uint64_t hdr = SPEAD_HEADER(udp_packet);

    // Check that this is in fact a SPEAD packet and that the correct
    // version is being used
    if ((SPEAD_GET_MAGIC(hdr) != SPEAD_MAGIC) ||
        (SPEAD_GET_VERSION(hdr) != SPEAD_VERSION) ||
        (SPEAD_GET_ITEMSIZE(hdr) != SPEAD_ITEM_PTR_WIDTH) ||
        (SPEAD_GET_ADDRSIZE(hdr) != SPEAD_HEAP_ADDR_WIDTH))
        return false;

    // Check whether the SPEAD packet contains burst channel data
    uint64_t mode = SPEAD_ITEM_ADDR(SPEAD_ITEM(udp_packet, 5));
    return mode == 0x7;
}

// Get and process packet
bool PharosChannelisedData::processPacket()
{
    // Get next packet to process
    size_t packet_size = ring_buffer -> pull_timeout(&packet, 1);

    // Check if the request timed out
    if (packet_size == SIZE_MAX) {
        // Request timed out
        return false;
    }

    // This packet is a SPEAD packet, since otherwise it would not have
    // passed through the filter
    uint64_t hdr = SPEAD_HEADER(packet);

    uint32_t packet_index   = 0;
    size_t packet_counter = 0;
    uint64_t payload_length = 0;
    uint64_t sync_time = 0;
    uint64_t timestamp = 0;
    uint16_t start_channel_id = 0;
    uint16_t start_antenna_id = 0;
    uint16_t nof_included_channels = 0;
    uint16_t nof_included_antennas = 0;
    uint16_t tile_id = 0;
    uint16_t station_id = 0;
    uint8_t  pol_id     = 0;
    uint32_t payload_offset = 0;

    // Get the number of items and get a pointer to the packet payload
    auto nofitems = (unsigned short) SPEAD_GET_NITEMS(hdr);
    uint8_t *payload = packet + SPEAD_HEADERLEN + nofitems * SPEAD_ITEMLEN;

    // Loop over items to extract values
    for(unsigned i = 1; i <= nofitems; i++)
    {
        uint64_t item = SPEAD_ITEM(packet, i);
        switch (SPEAD_ITEM_ID(item))
        {
            case 0x0001:  // Heap counter
            {
                packet_counter = (uint32_t) (SPEAD_ITEM_ADDR(item) & 0xFFFFFF);       // 24-bits
                packet_index   = (uint32_t) ((SPEAD_ITEM_ADDR(item) >> 24) & 0xFFFF); // 16-bits
                break;
            }
            case 0x0004: // Payload length
            {
                payload_length = SPEAD_ITEM_ADDR(item);
                break;
            }
            case 0x1027: // Sync time
            {
                sync_time = SPEAD_ITEM_ADDR(item);
                break;
            }
            case 0x1600: // Timestamp
            {
                timestamp = SPEAD_ITEM_ADDR(item);
                break;
            }
            case 0x2002: // Antenna and Channel information
            {
                uint64_t val = SPEAD_ITEM_ADDR(item);
                start_channel_id      = (uint16_t) ((val >> 24) & 0xFFFF);
                nof_included_channels = (uint16_t) ((val >> 16) & 0xFF);
                start_antenna_id      = (uint16_t) ((val >> 8) & 0xFF);
                nof_included_antennas = (uint16_t) (val & 0xFF);
                break;
            }
            case 0x2001: // Tile information
            {
                uint64_t val = SPEAD_ITEM_ADDR(item);
                station_id = (uint16_t) ((val >> 16) & 0xFFFF);
                tile_id    = (uint16_t) ((val >> 32) & 0xFF);
                pol_id     = (uint8_t)   (val & 0xFF);
                break;
            }
            case 0x3300: // Payload offset
            {
                payload_offset = (uint32_t) SPEAD_ITEM_ADDR(item);
                break;
            }
            case 0x2004:
                break;
            default:
                LOG(WARN, "Unknown item %#010x (%d of %d)", SPEAD_ITEM_ID(item), i, nofitems);
        }
    }

    // TEMPORARY: Timestamp_scale maybe will disappear, so it's hardcoded for now
    double packet_time = sync_time + timestamp * 1.08e-6;// timestamp_scale;

    // Calculate number of samples in packet
    uint32_t samples_in_packet;

    // Number of samples in packet
    samples_in_packet = (uint32_t) ((payload_length - payload_offset) /
                                    (nof_included_antennas * nof_included_channels * sizeof(uint16_t)));

    // Update packet index
    if (reference_counter == 0)
        reference_counter = packet_counter;

    // Check whether packet counter has rolled over
    if (packet_counter == 0 && pol_id == 0)
        rollover_counter += 1;

    // Multiply packet_counter by rollover counts
    packet_counter += rollover_counter << 24;

    // Assigned correct packet index
    packet_index = static_cast<uint32_t>((packet_counter - reference_counter) % (nof_samples / samples_in_packet));

    // We have processed the packet items, send data to packet counter
    double_buffer -> write_data(start_antenna_id + nof_antennas_per_tpm_tile * tile_id,
                                nof_included_antennas,
                                packet_index,
                                samples_in_packet,
                                start_channel_id,
                                (uint16_t *) (payload + payload_offset),
                                packet_time);

    // Ready from packet
    ring_buffer -> pull_ready();

    // All done, return
    return true;
}

// ------------------------------------------------------------------------------------------------------------------

// Default double buffer constructor
PharosDoubleBuffer::PharosDoubleBuffer(uint16_t nof_antennas, uint16_t nof_channels, uint32_t nof_samples,
                                       uint8_t nbuffers):
        nof_antennas(nof_antennas), nof_channels(nof_channels), nof_samples(nof_samples), nof_buffers(nbuffers)
{
    // Make sure that nof_buffers is a power of 2
    nbuffers = (uint8_t) pow(2, ceil(log2(nbuffers)));

    // Allocate the double buffer
    allocate_aligned((void **) &double_buffer, CACHE_ALIGNMENT, nbuffers * sizeof(PharosBuffer));

    // Allocate buffers
    for(unsigned i = 0; i < nbuffers; i++)
    {
        double_buffer[i].mutex = new std::mutex;
        allocate_aligned((void **) &(double_buffer[i].data), PAGE_ALIGNMENT,
                         nof_channels * nof_samples * nof_antennas * sizeof(uint16_t));

        // Lock memory
        if (mlock(double_buffer[i].data, nof_antennas * nof_channels * nof_samples * sizeof(uint16_t)) == -1)
            perror("Could not lock memory");
    }

    // Initialise buffers
    reset_all();

    // Initialise producer and consumer
    producer = 0;
    consumer = 0;

    // Set up timing variables
    tim.tv_sec  = 0;
    tim.tv_nsec = 1000;

    // Initialise current channel
    current_channel = -1;
}

// Class destructor
PharosDoubleBuffer::~PharosDoubleBuffer()
{
    for(unsigned i = 0; i < nof_buffers; i++)
    {
        delete double_buffer[i].mutex;
        free(double_buffer[i].data);
    }
    free(double_buffer);
}

// Write data to buffer
void PharosDoubleBuffer::write_data(uint16_t start_antenna, uint16_t nof_included_antennas,
                              uint32_t packet_index, uint32_t samples, uint32_t channel_id,
                              uint16_t *data_ptr, double timestamp)
{
    // Check if we're processing a new channel stream
    if (current_channel != channel_id)
    {
        auto now = std::chrono::system_clock::now();
        auto datetime = std::chrono::system_clock::to_time_t(now);
        auto date_text = strtok(ctime(&datetime), "\n");
        cout << date_text <<  ": Switching to new channel " << channel_id << endl;

        // We are processing a new channel, reset existing buffers
        reset_all();

        // Update current channel ID
        current_channel = channel_id;
    }

    // Check whether the current consumer buffer is empty
    if (double_buffer[producer].index == -1) {
        // Set channel and index of this buffer
        double_buffer[producer].index = static_cast<int>(packet_index);
        double_buffer[producer].channel_id = channel_id;
    }

    // Check if we are receiving a packet from a previous buffer, if so place in previous buffer
    else if (double_buffer[producer].ref_time > timestamp)
    {
        int local_producer = (producer == 0) ? nof_buffers - 1 : (producer - 1);
        copy_data(static_cast<uint32_t>(local_producer), start_antenna, nof_included_antennas,
                  packet_index * samples, samples, data_ptr, timestamp);
        double_buffer[local_producer].index = packet_index;
        return;
    }

    // Check if current buffer's index and timestamp are valid
    else if (packet_index == 0 && timestamp >=
            double_buffer[producer].ref_time + (nof_samples - 1) * 1.08e-6 &&
            double_buffer[producer].nof_packets > 2)
    {
        // We have skipped buffer borders, mark previous buffer as ready and switch to next one
        int local_producer = (producer == 0) ? nof_buffers - 1 : (producer - 1);
        if (double_buffer[local_producer].index != -1) {
            double_buffer[local_producer].ready = true;
        }

        // Update producer pointer
        producer = (producer + 1) % nof_buffers;

        // Wait for next buffer to become available
        unsigned int index = 0;
        while (index * tim.tv_nsec < 1e4)
        {
            if (double_buffer[producer].index != -1) {
                nanosleep(&tim, &tim2);
                index++;
            }
            else
                break;
        }

        if (index * tim.tv_nsec >= 1e4 )
            printf("Warning: Overwriting buffer [%d]!\n", producer);

        // Start using new buffer
        double_buffer[producer].index = packet_index;
        double_buffer[producer].read_samples = 0;
        double_buffer[producer].channel_id = channel_id;
    }

    // Copy data to buffer
    copy_data((uint32_t) producer, start_antenna, nof_included_antennas,
                    packet_index * samples, samples, data_ptr, timestamp);
    double_buffer[producer].index = packet_index;
}

inline void PharosDoubleBuffer::copy_data(uint32_t producer_index, uint16_t start_antenna, uint16_t nof_included_antennas,
                                    uint32_t start_sample_index, uint32_t samples,
                                    uint16_t *data_ptr, double timestamp)
{
    // Data will be stored in sample/antenna/channel
    uint16_t *ptr = double_buffer[producer_index].data + start_sample_index * nof_antennas * nof_channels + start_antenna;

    for(unsigned i = 0; i < samples * nof_channels; i++)
    {
        memcpy(ptr, data_ptr, nof_included_antennas * sizeof(uint16_t));
        ptr += nof_antennas;
        data_ptr += nof_included_antennas;
    }

    // Update number of samples in the current buffer
    if (start_antenna == 0)
        double_buffer[producer_index].read_samples += samples;

    double_buffer[producer_index].nof_packets++;

    // Update timings
    if (double_buffer[producer_index].ref_time > timestamp || double_buffer[producer_index].ref_time == 0)
        double_buffer[producer_index].ref_time = timestamp;
}


// In case where the data stream is terminated or has stopped, finish current buffer
void PharosDoubleBuffer::finish_write()
{
    // If a buffer is being written to, finalize it
    int local_producer = (producer == 0) ? nof_buffers - 1 : (producer - 1);
    if (double_buffer[local_producer].index != -1)
        double_buffer[local_producer].ready = true;

    if (double_buffer[producer].index != -1)
    {
        double_buffer[producer].ready = true;
        producer = (producer + 1) % nof_buffers;
    }
}

// Read buffer
PharosBuffer* PharosDoubleBuffer::read_buffer()
{
    // Wait for buffer to become available
    while(true) {
        // Lock global buffer
        global_mutex.lock();

        // Check if buffer is ready
        if (double_buffer[consumer].ready)
            break;

        // Unlock mutex
        global_mutex.unlock();

        // Wait for some time
        nanosleep(&tim, &tim2);
    }

    return &(double_buffer[consumer]);
}

// Ready from buffer, mark as processed
void PharosDoubleBuffer::release_buffer()
{
    // Set buffer as processed
    reset_buffer(consumer);

    // Move consumer pointer
    consumer = (consumer + 1) % nof_buffers;

    // Unlock global buffer
    global_mutex.unlock();
}

// Clear double buffer
void PharosDoubleBuffer::reset_buffer(unsigned index)
{
    // Reset buffer if index is valid
    if (index < nof_buffers) {
        double_buffer[index].mutex -> lock();
        double_buffer[index].ref_time = 0;
        double_buffer[index].ready = false;
        double_buffer[index].index = -1;
        double_buffer[index].read_samples = 0;
        double_buffer[index].nof_samples = nof_samples;
        double_buffer[index].nof_packets = 0;
        double_buffer[index].channel_id = 0;
        memset(double_buffer[index].data, 0, nof_samples * nof_channels * nof_antennas * sizeof(uint16_t));
        double_buffer[index].mutex -> unlock();
    }
}

// Clear double buffer
void PharosDoubleBuffer::reset_all()
{
    // Lock global buffer
    global_mutex.lock();

    // Initialise and allocate buffers in each struct instance
    for(unsigned i = 0; i < nof_buffers; i++)
        reset_buffer(i);

    // Reset producer and consumer indices
    producer = 0;
    consumer = 0;

    // Unlock global buffer
    global_mutex.unlock();
}

// ------------------------- CROSS CORRELATOR ----------------------------------

// Class constructor
CallbackManager::CallbackManager(PharosDoubleBuffer *double_buffer)
{
    // Get pointer to double buffer
    this->double_buffer = double_buffer;
}

// Main event loop
void CallbackManager::threadEntry()
{
    // Infinite loop: Process buffers
    unsigned counter = 0;
    while(!stop_thread)
    {
        // Get new buffer
        PharosBuffer *buffer = double_buffer -> read_buffer();

        // Call callback if set
        if (callback != nullptr)
            callback(buffer->data, buffer->ref_time, buffer->channel_id, buffer->nof_samples);

        // All done with data, release buffer
        double_buffer -> release_buffer();

        counter++;
    }
}

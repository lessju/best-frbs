//
// Created by Alessio Magro on 22/07/2016.
//

#ifndef BESTFRB_CHANNELDATA_H
#define BESTFRB_CHANNELDATA_H

#include <cstdlib>
#include <unistd.h>
#include <fcntl.h>
#include <cstring>
#include <cmath>
#include <string>
#include <mutex>

#include "DAQ.h"

// Represents a single buffer in the double (or more) buffering system
struct PharosBuffer
{
    double     ref_time;  // The reference time of the second contained in the buffer
    int        index;   // Index to be used to determine buffer boundaries
    bool       ready;        // Specifies whether the buffer is ready to be processed
    uint32_t   read_samples; //Number of actual samples in the buffer
    uint32_t   nof_packets; // Number of packets
    uint16_t   *data;         // Pointer to the buffer itself
    uint32_t   nof_samples;     // Number of samples in buffer
    uint32_t   channel_id;   // Channel ID in data
    std::mutex *mutex;    // Mutex lock for this buffer
};

class PharosDoubleBuffer
{

public:
    // Default constructor
    PharosDoubleBuffer(uint16_t nof_antennas, uint16_t nof_channels, uint32_t nof_samples,
                       uint8_t nbuffers = 3);

    // Class destructor
    ~PharosDoubleBuffer();

    // Write data to buffer
    void write_data(uint16_t start_antenna, uint16_t nof_included_antennas,
                    uint32_t packet_index, uint32_t samples, uint32_t channel_id,
                    uint16_t *data_ptr, double timestamp);

    // Finish write
    void finish_write();

    // Read buffer
    PharosBuffer* read_buffer();

    // Ready from buffer, mark as processed
    void release_buffer();

    // Clear double buffer
    void reset_buffer(unsigned index);

    void reset_all();

private:

    inline void copy_data(uint32_t producer_index, uint16_t start_antenna, uint16_t nof_included_antennas,
                          uint32_t start_sample_index, uint32_t samples,
                          uint16_t *data_ptr, double timestamp);

private:
    // The data structure which will hold the buffer elements
    PharosBuffer *double_buffer{};

    // Double buffer parameters
    uint16_t nof_antennas;   // Total number of antennas (or stations)
    uint32_t nof_samples;   // Total number of samples
    uint16_t nof_channels;  // Total number of channels
    uint8_t  nof_buffers; // Number of buffers in buffering system
    uint32_t current_channel; // Current channel

    // Producer and consumer pointers, specifying which buffer index to use
    // These are declared as volatile so tha they are not optimsed into registers
    volatile int producer;
    volatile int consumer;

    // Container wide semaphore
    std::mutex global_mutex;

    // Timing variables
    struct timespec tim{}, tim2{};
};

// -----------------------------------------------------------------------------

// Class which recieves data to be consumed by the Pharos callback_manager
class CallbackManager: public RealTimeThread
{

public:
     // Class constructor
     CallbackManager(PharosDoubleBuffer *double_buffer);

     // Set callback (provided by Correlator)
     void setCallback(DataCallback callback)
     {
       this -> callback = callback;
     }

protected:

    // Main thread event loop
    void threadEntry() override;

private:

    // Pointer to double buffer
    PharosDoubleBuffer *double_buffer;

    // Callback
    DataCallback callback = nullptr;
};


// -----------------------------------------------------------------------------

// Class which will hold the channelised data for correlation
class PharosChannelisedData: public DataConsumer
{
public:

    // Initialise consumer
    bool initialiseConsumer(json configuration) override;

    // Override setDataCallback
    void setCallback(DataCallback callback) override
    { callback_manager -> setCallback(callback); }

protected:
    // Packet filtering function to be passed to network thread
    inline bool packetFilter(unsigned char* udp_packet) override;

    // Grab SPEAD packet from buffer and process
    bool processPacket() override;

private:

   // Pointer to Double Buffer
    PharosDoubleBuffer *double_buffer;

    // Pointer to CallbackManager;
    CallbackManager *callback_manager;

    // Reference packet counter
    size_t reference_counter = 0;
    uint32_t rollover_counter = 0;

    // Data setup
    uint16_t nof_antennas = 0;        // Number of antennas
    uint16_t nof_antennas_per_tpm_tile = 32; // Number of antennas per tile
    uint16_t nof_channels = 0;        // Number of channels
    uint32_t nof_samples = 0;         // Number of time samples
};

extern "C" DataConsumer *pharoschannelised() { return new PharosChannelisedData; }

#endif // BESTFRB_CHANNELDATA_H

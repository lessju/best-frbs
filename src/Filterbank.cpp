#include "Filterbank.h"
#include <stdio.h>
#include <iostream>
#include <unistd.h>
#include <fcntl.h>


Filterbank::Filterbank(std::string filepath, uint32_t nbits) {

    // Open file for writing
    this->fd  = open(filepath.c_str(), O_WRONLY | O_CREAT | O_SYNC | O_TRUNC, (mode_t) 0666);

    // Tell the kernel how the file is going to be accessed (sequentially)
    posix_fadvise(this->fd, 0, 0, POSIX_FADV_SEQUENTIAL);

    // Preallocate first 500 MB
    // posix_fallocate(this->fd, 0, 500 * 1024 * 1024);

    // Store number of bits
    this->nbits = nbits;
}

int Filterbank::getFileDescriptor() {
    return this->fd;
}

void Filterbank::writeHeader(FILE_HEADER *header)
{
    /* Broadcast the header parameters to the output stream */
    if (header->machine_id != 0) {
        sendString("HEADER_START");
        sendInt("machine_id", header->machine_id);
        sendInt("telescope_id", header->telescope_id);
        sendCoords(header->src_raj,header->src_dej,header->az_start,header->za_start);

        /* filterbank data */
        /* N.B. for dedisperse to work, foff<0 so flip if necessary */
        sendInt("data_type",1);
        sendDouble("fch1",header->fch1);
        sendDouble("foff",header->foff);

        sendInt("nchans",header->nchans);

        /* beam info */
        sendInt("nbeams",header->nbeams);
        sendInt("ibeam",header->ibeam);

        /* number of bits per sample */
        sendInt("nbits", nbits);

        /* start time and sample interval */
        sendDouble("tstart", (header -> tstart));
        sendDouble("tsamp", header->tsamp);

        sendInt("nifs",1);

        sendString("HEADER_END");
    }
}

FILE_HEADER *Filterbank::readHeader() {
    return nullptr;
}

ssize_t Filterbank::writeBlock(void *block, size_t size) {
    return write(this->fd, block, size);
}

void Filterbank::syncFile() {
    fsync(this->fd);
}

void Filterbank::closeFile() {
    syncFile();
    close(this->fd);
 }

void Filterbank::sendString(const std::string &string) {
    auto len = string.length();
    if (write(this->fd, &len, sizeof(int)) < 0)
        return;
    if (write(this->fd, string.c_str(), len * sizeof(char)) < 0)
        return;
}

void Filterbank::sendFloat(const std::string &name, float value) {
    sendString(name);
    if (write(this->fd, &value, sizeof(float)) < 0)
        return;
}

void Filterbank::sendDouble(const std::string &name, double value) {
    sendString(name);
    if (write(this->fd, &value, sizeof(double)) < 0)
        return;
}

void Filterbank::sendInt(const std::string &name, int value) {
    sendString(name);
    if (write(this->fd, &value, sizeof(int)) < 0)
        return;
}

void Filterbank::sendChar(const std::string &name, char value) {
    sendString(name);
    if (write(this->fd, &value, sizeof(char)))
        return;
}

void Filterbank::sendLong(const std::string &name, long value) {
    sendString(name);
    if (write(this->fd, &value, sizeof(long)))
        return;
}

void Filterbank::sendCoords(double raj, double dej, double az, double za)
{
    if ((raj != 0.0) || (raj != -1.0)) sendDouble("src_raj",raj);
    if ((dej != 0.0) || (dej != -1.0)) sendDouble("src_dej",dej);
    if ((az != 0.0)  || (az != -1.0))  sendDouble("az_start",az);
    if ((za != 0.0)  || (za != -1.0))  sendDouble("za_start",za);
}

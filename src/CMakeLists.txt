cmake_minimum_required(VERSION 2.8)

project(PHAROS C CXX)

# Add include and lib directories
link_directories(/usr/local /usr/lib/x86_64-linux-gnu/lib ${DAQ_DIRECTORY}/lib)
include_directories(/usr/lib/x86_64-linux-gnu/include )
include_directories(${DAQ_DIRECTORY}/include)

# Source files
set(PHAROS_FILES
    PharosBeamformedData.h
    PharosBeamformedData.cpp
    PharosChannelisedData.h
    PharosChannelisedData.cpp
    Filterbank.h
    Filterbank.cpp
    SPEAD.h
        )

# CMAKE compilation flags
set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -std=c++14 -D_REENTRANT -fPIC -funroll-loops -O3 -march=native -fno-trapping-math -fno-math-errno -m64 -mavx2 -fno-rtti")

# Compile library
add_library(pharos SHARED ${PHAROS_FILES})

# Link required libraries
target_link_libraries(pharos daq ${CMAKE_THREAD_LIBS_INIT})

# Create executables
add_executable(pharos_beam pharos_beamformed_data.cpp)
target_link_libraries(pharos_beam pharos)

add_executable(pharos_channel pharos_channelised_data.cpp)
target_link_libraries(pharos_channel pharos)

# Install library
install(TARGETS "pharos" DESTINATION ${CMAKE_INSTALL_PREFIX}/lib)

# Install binaries
install(TARGETS "pharos_channel" DESTINATION ${CMAKE_INSTALL_PREFIX}/bin)
install(TARGETS "pharos_beam" DESTINATION ${CMAKE_INSTALL_PREFIX}/bin)

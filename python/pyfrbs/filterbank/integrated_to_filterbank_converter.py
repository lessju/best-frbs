from astropy.time import Time
from datetime import datetime
from math import floor
import numpy as np
import struct
import h5py
import pytz
import sys
import re
import os

from filterbank import create_filterbank_file


def process_coord(coord):
    """ Process coordinate """
    # Coordinates should be of the for NN:NN:NN.N
    if not re.match("[+|-]?\d\d:\d\d:\d\d\.\d+", coord):
        print("Coordinates should have the form NN:NN:NN.N")
        exit()

    return float(coord.replace(":", ""))


def generate_header():
    """ Generate a filterbank header """
    header = {'fch1': None,
              'foff': None,
              'nchans': None,
              'machine_id': 1,
              'telescope_id': 30,  # Norther cross telescope ID
              'src_raj': process_coord(conf.ra),
              'src_dej': process_coord(conf.dec),
              'az_start': 1,
              'za_start': 1,
              'nbeams': 1,
              'ibeam': 1,
              'nbits': 32,
              'nifs': 1,
              'data_type': 1,
              'tstart': None,
              'tsamp': None}

    return header


def dm_shift_per_freq(dm, chans, fch1, foff, tsamp):
    """ Calculate time shift per channel for provided DM
     :param dm: Dispersion Measure
     :param chans: Number of frequency channels
     :param fch1: Frequency of highest channel (highest frequency)
     :param foff: Channel bandwidth
     :param tsamp: Sampling time """
    dm_delay = lambda f1, f2: 4148.741601 * ((1.0 / f1 / f1) - (1.0 / f2 / f2))
    return [int(round(dm_delay(fch1 + (foff * i), fch1) * dm / tsamp)) for i in range(chans)]


def dedisperse_chunk(data, shifts, chunk_size):
    """ Dedisperse a chunk of integrated data"""
    # Construct dedispersed data holder
    shape = list(data.shape)
    shape[0] = chunk_size
    dedispersed = np.empty(shape)

    # Loop over all channels
    for i, shift in enumerate(shifts):
        dedispersed[:, i, :] = data[shifts:shifts + chunk_size, i, :]

    return dedispersed


# Script entry point
if __name__ == "__main__":
    from optparse import OptionParser
    from sys import argv

    parser = OptionParser(usage="usage: %hdf_to_filterbank_converter.py [options]")
    parser.add_option("-c", "--chunk-size", action="store", dest="chunk_size", type=int,
                      default=128, help="Chunk size [default: 128]")
    parser.add_option("-f", "--file", action="store", dest="file",
                      default="None", help="File to convert [default: None]")
    parser.add_option("-o", "--output-file", action="store", dest="output_file",
                      default="output", help="Output filename prefix [default: output]")
    parser.add_option("--source-name", action="store", dest="source_name",
                      default="", help="Source name (default: no name)")
    parser.add_option("--start-channel", action="store", dest="start_channel", type=int,
                      default=0, help="Start channel to store [default: 0]")
    parser.add_option("--stop-channel", action="store", dest="stop_channel", type=int,
                      default=0, help="Stop channel to store [default: 0 (last channel)]")
    parser.add_option("--start-frequency", action="store", dest="start_freq", type=float,
                      default=0, help="Frequency in MHz of channel 0 (bottom of the band) [default: 0 MHz]")
    parser.add_option("--channel-bandwidth", action="store", dest="bandwidth", type=float,
                      default=0.78125, help="Channel bandwidth in MHz [default: 0.78125 MHz ]")
    parser.add_option("--dm", action="store", dest="dm", type=float,
                      default=0, help="DM to dedisperse with [default: 0 (do not dedisperse)]")
    parser.add_option("--ra", action="store", dest="ra",
                      default="00:00:00.0", help="Stop channel to store [default: 00:00:00.0")
    parser.add_option("--dec", action="store", dest="dec",
                      default="00:00:00.0", help="Stop channel to store [default: 00:00:00.0")

    (conf, args) = parser.parse_args(argv[1:])

    # Sanity check on input file
    if not os.path.exists(conf.file) and not os.path.isfile(conf.file):
        print("Invalid input file specified: {}".format(conf.file))
        exit(-1)

    # Generate basic filterbank header
    header = generate_header()

    # Open HDF file
    with h5py.File(conf.file) as f:
        # Grab attributes from file
        nof_channels = f['root'].attrs['n_chans']
        nof_samples = f['root'].attrs['n_blocks'] * f['root'].attrs['n_samples']
        nof_beams = f['root'].attrs['n_beams']
        timestamp = f['root'].attrs['ts_start']
        tsamp = (f['sample_timestamps']['data'][1] - f['sample_timestamps']['data'][0])[0]

        # Calculate required nof_channels
        if conf.stop_channel == 0:
            conf.stop_channel = nof_channels
        nof_channels = conf.stop_channel - conf.start_channel

        # Calculate start frequency
        start_frequency = conf.start_freq + conf.start_channel * conf.bandwidth

        # Convert timestamp from UNIX timestamp to MJD
        unix_time = datetime.fromtimestamp(timestamp)
        unix_time = unix_time.replace(tzinfo=pytz.timezone('CET'))
        timestamp = Time(unix_time, format="datetime", scale="utc").mjd

        # Update filterbank header
        header['fch1'] = start_frequency + nof_channels * conf.bandwidth
        header['foff'] = conf.bandwidth
        header['nchans'] = nof_channels
        header['tstart'] = timestamp
        header['tsamp'] = tsamp

        # Add source name if given
        if conf.source_name != "":
            header['source_name'] = conf.source_name

        # Create and output file for each beams and insert header
        output_files = []
        for i in range(nof_beams):
            output_filename = "{}_beam_{}.fil".format(conf.output_file, i)
            create_filterbank_file(output_filename, header, 32)
            output_files.append(open(output_filename, 'ab+'))
            output_files[-1].seek(0, 2)

        # Grab shifts
        shifts = dm_shift_per_freq(conf.dm, nof_channels, header['fch1'], -conf.bandwidth, tsamp)
        max_shift = shifts[-1]

        # Read in input file in chunks
        for s in range(int(floor(nof_samples / float(conf.chunk_size)))):
            # Read current chunk (including data required for processing)
            if "beam_" in f.keys():
                data = f['beam_']['data'][0, conf.chunk_size * s: conf.chunk_size * (s + 1) + max_shift, conf.start_channel: conf.stop_channel, :]
            else:
                data = f['polarization_0']['data'][conf.chunk_size * s: conf.chunk_size * (s + 1) + max_shift, conf.start_channel: conf.stop_channel, :]

            # Dedisperse data
            if data.shape[0] != conf.chunk_size + max_shift:
                print("Reached end of processable file. Ignoring last chunk")
                break

            # Dedisperes if required
            if conf.dm != 0:
                dedispersed = dedisperse_chunk(data, shifts, conf.chunk_size)
            else:
                dedispersed = data

            # Flip data (so that highest freq is first)
            dedispersed = np.flip(dedispersed, axis=1)

            # Loop over beams
            for b in range(nof_beams):
                to_write = dedispersed[:, :, b].astype(np.float32).flatten()
                output_files[b].write(to_write.tostring())

            # Update status
            sys.stdout.write(
                "Processed %d of %d [%.2f%%]      \r" % (s, (nof_samples / conf.chunk_size) + 1,
                                                        (s / float(nof_samples / conf.chunk_size)) * 100))
            sys.stdout.flush()

        # Close files
        for f in output_files:
            f.close()

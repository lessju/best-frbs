"""
A module for reading filterbank files.
Patrick Lazarus, June 26, 2012
(Minor modification from file originally from June 6th, 2009)
"""

from pyfrbs.filterbank import sigproc

DEBUG = False


def create_filterbank_file(outfn, header, nbits=8, verbose=False, mode='append'):
    """Write filterbank header and spectra to file.
        Input:
            outfn: The outfile filterbank file's name.
            header: A dictionary of header paramters and values.
            nbits: The number of bits per sample of the filterbank file.
                This value always overrides the value in the header dictionary.
                (Default: 8 - i.e. each sample is an 8-bit integer)
            verbose: If True, be verbose (Default: be quiet)
            mode: Mode for writing (can be 'append' or 'write')
        Output:
            fbfile: The resulting FilterbankFile object opened
                in read-write mode.
    """
    dtype = get_dtype(nbits)  # Get dtype. This will check to ensure
    # 'nbits' is valid.
    header['nbits'] = nbits
    outfile = open(outfn, 'wb')
    outfile.write(sigproc.addto_hdr("HEADER_START", None))
    for paramname in header.keys():
        if paramname not in sigproc.header_params:
            # Only add recognized parameters
            print("Unrecognised parameter (%s)" % paramname)
            continue
        if verbose:
            print("Writing header param (%s)" % paramname)
        value = header[paramname]
        outfile.write(sigproc.addto_hdr(paramname, value))
    outfile.write(sigproc.addto_hdr("HEADER_END", None))

    outfile.close()


def is_float(nbits):
    """For a given number of bits per sample return
        true if it corresponds to floating-point samples
        in filterbank files.
        Input:
            nbits: Number of bits per sample, as recorded in the filterbank
                file's header.
        Output:
            isfloat: True, if 'nbits' indicates the data in the file
                are encoded as floats.
    """
    check_nbits(nbits)
    if nbits in [32, 64]:
        return True
    else:
        return False


def check_nbits(nbits):
    """Given a number of bits per sample check to make
        sure 'filterbank.py' can cope with it.
        An exception is raise if 'filterbank.py' cannot cope.
        Input:
            nbits: Number of bits per sample, as recorded in the filterbank
                file's header.
        Output:
            None
    """
    if nbits not in [64, 32, 16, 8]:
        raise ValueError("'filterbank.py' only supports " \
                         "files with 8- or 16-bit " \
                         "integers, or 32-bit floats " \
                         "(nbits provided: %g)!" % nbits)


def get_dtype(nbits):
    """For a given number of bits per sample return
        a numpy-recognized dtype.
        Input:
            nbits: Number of bits per sample, as recorded in the filterbank
                file's header.
        Output:
            dtype: A numpy-recognized dtype string.
    """
    check_nbits(nbits)
    if is_float(nbits):
        dtype = 'float%d' % nbits
    else:
        dtype = 'uint%d' % nbits
    return dtype


def read_header(filename, verbose=False):
    """Read the header of a filterbank file, and return
        a dictionary of header paramters and the header's
        size in bytes.
        Inputs:
            filename: Name of the filterbank file.
            verbose: If True, be verbose. (Default: be quiet)
        Outputs:
            header: A dictionary of header paramters.
            header_size: The size of the header in bytes.
    """
    header = {}
    filfile = open(filename, 'rb')
    filfile.seek(0)
    paramname = ""
    while paramname != 'HEADER_END':
        if verbose:
            print("File location: %d" % filfile.tell())
        paramname, val = sigproc.read_hdr_val(filfile, stdout=verbose)
        if verbose:
            print("Read param %s (value: %s)" % (paramname, val))
        if paramname not in ["HEADER_START", "HEADER_END"]:
            header[paramname] = val
    header_size = filfile.tell()
    filfile.close()
    return header, header_size

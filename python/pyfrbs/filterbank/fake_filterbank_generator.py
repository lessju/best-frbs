from astropy.time import Time
from datetime import datetime
from math import floor
import numpy as np
import time
import struct
import h5py
import pytz
import sys
import re
import os

from filterbank import create_filterbank_file, read_header


def process_coord(coord):
    """ Process coordinate """
    # Coordinates should be of the for NN:NN:NN.N
    if not re.match("[+|-]?\d\d:\d\d:\d\d\.\d+", coord):
        print("Coordinates should have the form NN:NN:NN.N")
        exit()

    return float(coord.replace(":", ""))


def generate_header(conf):
    """ Generate a filterbank header """
    header = {'fch1': None,
              'foff': None,
              'nchans': None,
              'machine_id': 1,
              'telescope_id': 30,  # Norther cross telescope ID
              'src_raj': process_coord(conf.ra),
              'src_dej': process_coord(conf.dec),
              'az_start': 1,
              'za_start': 1,
              'nbeams': 1,
              'ibeam': 1,
              'nbits': 32,
              'nifs': 1,
              'data_type': 1,
              'tstart': None,
              'tsamp': None}

    return header


# Script entry point
if __name__ == "__main__":
    from optparse import OptionParser
    from sys import argv

    parser = OptionParser(usage="usage: %hdf_to_filterbank_converter.py [options]")
    parser.add_option("-c", "--nof-channels", action="store", dest="channels", type=int,
                      default=22, help="Nof channels [default: 22]")
    parser.add_option("-o", "--output-file", action="store", dest="output_file",
                      default="output.fil", help="Output filename prefix [default: output]")
    parser.add_option("-s", "--source-name", action="store", dest="source",
                      default="source", help="Source Name [default: None]")
    parser.add_option("--start-frequency", action="store", dest="start_freq", type=float,
                      default=400, help="Frequency in MHz of channel 0 (bottom of the band) [default: 400 MHz]")
    parser.add_option("--channel-bandwidth", action="store", dest="bandwidth", type=float,
                      default=0.78125, help="Channel bandwidth in MHz [default: 0.78125 MHz ]")  
    parser.add_option("--ra", action="store", dest="ra",
                      default="00:00:00.0", help="Stop channel to store [default: 00:00:00.0")
    parser.add_option("--dec", action="store", dest="dec",
                      default="00:00:00.0", help="Stop channel to store [default: 00:00:00.0")
    parser.add_option("--complex", action="store_true", dest="complex",
                      help="Generate complex data [default: False]")

    (conf, args) = parser.parse_args(argv[1:])

    # Generate basic filterbank header
    header = generate_header(conf)

    # Convert timestamp from UNIX timestamp to MJD
    unix_time = datetime.utcfromtimestamp(time.time())
    unix_time = unix_time.replace(tzinfo=pytz.timezone('CET'))
    timestamp = Time(unix_time, format="datetime", scale="utc").mjd

    # Update filterbank header
    header['fch1'] = conf.start_freq + conf.channels * conf.bandwidth
    header['foff'] = -conf.bandwidth
    header['nchans'] = conf.channels
    header['tstart'] = timestamp
    header['tsamp'] = 1.08e-6
    header['source_name'] = "FRB180916"

    # Create and output file for each beams and insert header
    if conf.complex:
        create_filterbank_file(conf.output_file, header, nbits=64)
    else:
        create_filterbank_file(conf.output_file, header, nbits=32)

    # Generate random data
    with open(conf.output_file, 'ab+') as f:
        f.seek(0, 2)
        if conf.complex:
            f.write(np.arange(262144 * conf.channels * 32, dtype=np.int16).tostring())
        else:
            f.write(np.arange(262144 * conf.channels * 32, dtype=np.float32).tostring())
        f.flush()

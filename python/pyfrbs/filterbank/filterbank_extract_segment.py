from pathlib import Path
import sys
import os

from pyfrbs.filterbank.filterbank import read_header, create_filterbank_file

if __name__ == "__main__":
    from optparse import OptionParser
    from sys import argv

    parser = OptionParser(usage="usage: %dedisperse_filterbank_file.py [options]")
    parser.add_option("-f", "--file", action="store", dest="file", help="File to process")
    parser.add_option("-o", "--output-file", action="store", dest="output_file",
                      default="output_segment.fil", help="Output file name [default: output.fil]")
    parser.add_option("--time-offset", action="store", dest="time_offset", type=int,
                      default=0, help="Time offset from start of file in seconds [default: 0s]")
    parser.add_option("--seconds", action="store", dest="seconds", type=int,
                      default=0, help="Number of seconds from offset (if provided) to process [Required]")
    (conf, args) = parser.parse_args(argv[1:])

    # Sanity check: check input filepath
    if not os.path.exists(conf.file):
        raise FileNotFoundError("Input filepath is invalid")

    if Path(conf.file).suffix != ".fil":
        raise ValueError("Input file must be a filterbank file (with .fil suffix)")

    # Sanity check: check output filepath
    if conf.output_file and not os.path.exists(Path(conf.output_file).parent):
        raise NotADirectoryError("Output file must be in an existing directory")

    if conf.seconds == 0:
        print("Number of seconds to extract required")
        exit()

    # Read header information and get header size
    input_header, input_header_size = read_header(conf.file)

    # Calculate the number of spectra in the file
    input_total_samples = (os.path.getsize(conf.file) - input_header_size) / \
                          (input_header['nchans'] * input_header['nbits'] / 8)

    # Read required metadata
    time_resolution = input_header['tsamp']
    nof_channels = input_header['nchans']

    # Make a copy of the input header
    output_header = input_header.copy()
    output_header['tstart'] += conf.time_offset

    # If output file already exists, remove it
    if os.path.exists(conf.output_file):
        print("Removing pre-existing file")
        os.remove(conf.output_file)

    # Create output file
    create_filterbank_file(conf.output_file, output_header, input_header['nbits'])

    # Open output file
    nof_samples = 65536
    output_file = conf.output_file
    with open(output_file, 'ab+') as out:

        # Seek to end of file (after header)
        out.seek(0, 2)

        # Open input file
        with open(conf.file, 'rb') as input_file:

            nof_chunks = int((conf.seconds / time_resolution) / nof_samples)

            for index in range(nof_chunks):
                input_file.seek(input_header_size + (nof_samples * index) * nof_channels * input_header['nbits'] // 8)
                out.write(input_file.read(nof_samples * nof_channels * input_header['nbits'] // 8))

                # Update status
                sys.stdout.write(
                    "Processed %d of %d [%.2f%%]      \r" % (index, nof_chunks,
                                                             (index / nof_chunks) * 100))
                sys.stdout.flush()








from multiprocessing.pool import ThreadPool
import numpy as np
import threading
import time
import h5py
import sys
import os


class Correlator:

    # Custom numpy type for creating complex signed 8-bit data
    complex_8t = np.dtype([('real', np.int8), ('imag', np.int8)])

    def __init__(self, nof_samples: int = 1048576,
                 nof_antennas: int = 32, nof_channels: int = 2):
        """ Class initializer """

        self._nof_samples = nof_samples
        self._nof_antennas = nof_antennas
        self._nof_channels = nof_channels
        self._nof_threads = 8
        self._skip = 1

        # Processing variables
        self._correlated_data = None
        self._open_data_file = None
        self._nof_blocks = None

        # Numpy-based ring buffer for communication between consumer and producer
        self._consumer_lock = threading.Lock()
        self._ring_buffer = None
        self._producer = 0
        self._consumer = 0
        self._counter = 0

    def process_file(self, input_file):

        # Check that file exists
        if not os.path.exists(input_file):
            print("Specified data file [%s] does not exist" % input_file)
            exit()

        # Reset counters (as process_file can be called multiple times)
        self._producer = 0
        self._consumer = 0
        self._counter = 0

        output_directory = os.path.join(os.path.dirname(input_file), input_file + ".h5")

        # Get number of samples in file
        total_samples = os.path.getsize(input_file) // (self._nof_antennas * self._nof_channels * 2)
        self._nof_blocks = total_samples // self._nof_samples
        nof_baselines = int(0.5 * (self._nof_antennas ** 2 + self._nof_antennas))

        # Open file for processing
        self._open_data_file = open(input_file, 'rb')

        # Create output buffer
        self._correlated_data = np.zeros((self._nof_channels, nof_baselines, self._nof_blocks), dtype=np.complex64)

        # Create ring buffer
        self._ring_buffer = []
        for i in range(self._nof_threads * 2):
            self._ring_buffer.append(
                {'full': False, 'index': 0,
                 'data': np.zeros((self._nof_samples, self._nof_channels, self._nof_antennas), dtype=self.complex_8t)})

        # Initialise producer thread
        producer_thread = threading.Thread(target=self._reader)
        producer_thread.start()

        # Start up consumer thread pool
        start = time.time()
        pool = ThreadPool(self._nof_threads)
        pool.map(self._process_parallel, range(self._nof_blocks))
        end = time.time()
        print("Took {}s to correlate {} blocks of {} samples each".format(end - start, self._nof_blocks, self._nof_samples))

        # Join producer
        producer_thread.join()

        # All done, write correlations to file
        f = h5py.File(output_directory, "w")

        dataset = f.create_dataset("Vis", (self._nof_blocks, self._nof_channels, nof_baselines),
                                   maxshape=(self._nof_blocks, self._nof_channels, nof_baselines),
                                   dtype='c16')

        self._correlated_data = np.transpose(self._correlated_data, (0, 2, 1))
        dataset[:] = np.transpose(self._correlated_data, (1, 0, 2))[:]

        # Create baselines data set
        dataset_2 = f.create_dataset("Baselines", (nof_baselines, 3))

        antenna1, antenna2, baselines, index = [], [], [], 0
        for i in range(self._nof_antennas):
            for j in range(i, self._nof_antennas):
                antenna1.append(i)
                antenna2.append(j)
                baselines.append(index)
                index += 1

        dataset_2[:, :] = np.transpose([baselines, antenna1, antenna2])

        # Ready file
        f.flush()
        f.close()

    def _correlate(self, input_data, output):
        baseline = 0
        for antenna1 in range(self._nof_antennas):
            for antenna2 in range(antenna1, self._nof_antennas):
                for channel in range(self._nof_channels):
                    ans = np.correlate(input_data[channel, antenna1, :], input_data[channel, antenna2, :])
                    output[channel, baseline] = ans[0]
                baseline += 1

    def _process_parallel(self, _):

        # Read next data block (data is in sample/channel/antenna)
        try:
            # Wait for data to be available in
            self._consumer_lock.acquire()
            while not self._ring_buffer[self._consumer]['full']:
                time.sleep(0.1)

            # We have data, process it
            data = self._ring_buffer[self._consumer]['data']
            data = np.transpose(data, (1, 0, 2))
            data = np.transpose(data, (0, 2, 1)).copy()
            index = self._ring_buffer[self._consumer]['index']

            # All done, release lock
            self._ring_buffer[self._consumer]['full'] = False
            self._consumer = (self._consumer + 1) % self._nof_threads
            self._counter += 1
            self._consumer_lock.release()

            data = (data['real'] + 1j * data['imag']).astype(np.complex64)

            sys.stdout.write(
                "Processed %d of %d [%.2f%%]     \r" % (self._counter, self._nof_blocks,
                                                        (self._counter / float(self._nof_blocks)) * 100))
            sys.stdout.flush()

            # Perform correlation
            self._correlate(data, self._correlated_data[:, :, index])
        except Exception as e:
            import traceback
            print(e, traceback.format_exc())

    def _reader(self):

        for i in range(self._nof_blocks):

            # Copy to ring buffer
            while self._ring_buffer[self._producer]['full']:
                time.sleep(0.1)

            # Read data
            self._open_data_file.seek(i * self._nof_samples * self._nof_antennas * self._nof_channels * 2)
            data = np.fromfile(self._open_data_file, dtype=self.complex_8t,
                               count=self._nof_samples * self._nof_channels * self._nof_antennas)
            self._ring_buffer[self._producer]['data'][:] = np.reshape(data,
                                                                      (self._nof_samples, self._nof_channels,
                                                                       self._nof_antennas))
            self._ring_buffer[self._producer]['index'] = i
            self._ring_buffer[self._producer]['full'] = True

            # Update producer pointer
            self._producer = (self._producer + 1) % self._nof_threads


if __name__ == "__main__":
    from optparse import OptionParser

    # Command line options
    p = OptionParser()
    p.set_usage('offline_correlator.py [options] INPUT_FILE')
    p.set_description(__doc__)

    p.add_option('-f', '--file', dest='file', action='store', default="channel_output.dat",
                 help="Data file to process (default: 'channel_output.dat')")
    p.add_option('-s', '--samples', dest='nof_samples', action='store', default=1048576,
                 type='int', help='Number of samples (default: 1048576)')
    p.add_option('-a', '--antennas', dest='nof_antennas', action='store', default=24,
                 type='int', help='Number of antennas (default: 24)')
    p.add_option('-c', '--channels', dest='nof_channels', action='store', default=2,
                 type='int', help='Number of channels (default: 2)')
    p.add_option('-d', '--directory', dest='directory', action='store', default=None,
                 help='If specified, correlate all .dat files in directory (default: None)')
    opts, args = p.parse_args(sys.argv[1:])

    # Create correlator object
    correlator = Correlator(nof_samples=opts.nof_samples,
                            nof_channels=opts.nof_channels,
                            nof_antennas=opts.nof_antennas)

    if opts.directory is None:
        correlator.process_file(opts.file)
    else:
        for data_file in os.listdir(opts.directory):
            if data_file.endswith(".dat") and data_file.startswith("channel"):
                print("Processing {}".format(data_file))
                correlator.process_file(os.path.join(opts.directory, data_file))

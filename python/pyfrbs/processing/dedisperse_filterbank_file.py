from pyfrbs.processing.FilterbankCoherentDedisperser import FilterbankCoherentDedisperser

if __name__ == "__main__":
    from optparse import OptionParser
    from sys import argv

    parser = OptionParser(usage="usage: %dedisperse_filterbank_file.py [options]")
    parser.add_option("-c", "--chunk-size", action="store", dest="chunk_size", type=int,
                      default=262144, help="Chunk size [default: 262144]")
    parser.add_option("-f", "--file", action="store", dest="file", help="File to process")
    parser.add_option("-o", "--output-file", action="store", dest="output_file",
                      default="output.fil", help="Output file name [default: output.fil]")
    parser.add_option("--start-frequency", action="store", dest="start_freq", type=float,
                      default=399.484375, help="Frequency in MHz of channel 0 (bottom of the band) [default: "
                                               "399.484375 MHz")
    parser.add_option("--dm", action="store", dest="dm", type=float,
                      default=100, help="Dispersion Measure [default: 100]")
    parser.add_option("--time-offset", action="store", dest="time_offset", type=int,
                      default=0, help="Time offset from start of file in seconds [default: 0s]")
    parser.add_option("--seconds", action="store", dest="seconds", type=int,
                      default=-1, help="Number of seconds from offset (if provided) to process [default: -1 (all)]")
    parser.add_option("--output-filterbank", action="store_true", dest="output_filterbank", default=False,
                      help="Generate a standard filterbank file (16-bit power data) rather than complex value" 
                           " [default: False]")
    parser.add_option("--use-gpu", action="store_true", dest="use_gpu",
                      default=False, help="Use GPU [default: False")

    (conf, args) = parser.parse_args(argv[1:])

    if not conf.file:
        print("Input file required")
        exit()

    dedisperser = FilterbankCoherentDedisperser(input_filepath=conf.file,
                                                nof_samples=conf.chunk_size,
                                                dm=conf.dm,
                                                start_frequency=conf.start_freq,
                                                output_filepath=conf.output_file,
                                                time_offset=conf.time_offset,
                                                time_seconds=conf.seconds,
                                                use_gpu=conf.use_gpu)
    dedisperser.print_info()
    dedisperser.process(conf.output_filterbank)

from datetime import datetime
from astropy.time import Time
from pathlib import Path
from math import ceil
import numpy as np
import time
import sys
import os
import re

from pyfrbs.processing.coherent_dedispersion import CoherentDedispersion
from pyfrbs.filterbank.filterbank import create_filterbank_file, read_header

# Try importing CuPy. If an exception occurs, no GPU processing is possible
try:
    from cupyx.scipy.fft import ifft as cu_ifft
    from cupyx.scipy.fft import fft as cu_fft
    import cupy

    CUPY_AVAILABLE = True
except ImportError:
    CUPY_AVAILABLE = False
    pass  #  print("CuPy not installed (or cannot be imported). GPUs cannot be used.")


class PostProcessor:
    """ Class which post-processes an entire BEST FRB complex filterbank file"""

    # Custom complex format datatype stored in custom BEST FRB filterbank file
    complex_8t = np.dtype([('real', np.int8), ('imag', np.int8)])
    complex_16t = np.dtype([('real', np.int16), ('imag', np.int16)])

    def __init__(self, input_filepath: str,
                 output_filepath: str = None,
                 nof_samples: int = 8388608,
                 start_frequency: float = 399.875,
                 pfb_fft_length: int = 64,
                 time_average: int = 2,
                 dm: float = 0,
                 time_offset: int = 0,
                 time_seconds: int = -1,
                 source_name: str = "None",
                 ra: str = "00:00:00.0",
                 dec: str = "00:00:00.0",
                 telescope_id: int = 30,
                 use_gpu: bool = False):
        """ Class constructor
        :param input_filepath: Filepath for input data
        :param output_filepath: Filepath to save dedispersed output
        :param nof_samples: Chunk size to process
        :param start_frequency: Bottom frequency of first frequency channel
        :param pfb_fft_length: Number of fine channels to generate per coarse channel
        :param time_average: Time averaging factor
        :param dm: DM to use
        :param time_offset: Offset in seconds from beginning of file to process
        :param time_seconds: Number of seconds from offset to process
        :param source_name: Source name
        :param ra: Right Ascension of source
        :param dec: Number Declination of source
        :param telescope_id: Telescope identifier
        :param use_gpu: Use GPU for processing
        :raises: FileNotFoundError if input_filepath is incorrect
        :raises: NotADirectoryError if output_filepath is not within an existing directory
        :raises: ValueError if any parameter is invalid """

        # Print warning message if GPUs requested but CuPy is not installed
        if use_gpu and not CUPY_AVAILABLE:
            print("CuPy is not installed, cannot use GPUs.")

        # Local reference to either cupy or numpy depending on whether GPUs are required and avialable
        if use_gpu and CUPY_AVAILABLE:
            self._cp = cupy
        else:
            self._cp = np

        # Sanity check: check input filepath
        if not os.path.exists(input_filepath):
            raise FileNotFoundError("Input filepath is invalid")

        if Path(input_filepath).suffix != ".fil":
            raise ValueError("Input file must be a filterbank file (with .fil suffix)")

        # Sanity check: check output filepath
        if output_filepath and not os.path.exists(Path(output_filepath).parent):
            raise NotADirectoryError("Output file must be in an existing directory")
        elif not output_filepath:
            p = Path(input_filepath)
            output_filepath = f"{p.parent}/{p.stem}_processed{p.suffix}"

        # Local copy of parameters
        self._input_filepath = input_filepath
        self._output_filepath = output_filepath
        self._original_start_frequency = start_frequency
        self._time_offset = time_offset
        self._time_seconds = time_seconds
        self._nof_samples = nof_samples
        self._pfb_fft_length = pfb_fft_length
        self._time_average = time_average
        self._source_name = source_name
        self._telescope_id = telescope_id
        self._use_gpu = use_gpu
        self._dm = dm

        # Convert RA and DEC coordinates to float (as required by sigproc)
        self._ra = self._process_source_coordinate(ra)
        self._dec = self._process_source_coordinate(dec)

        # Initialise header parameter placeholders
        self._input_header = None
        self._input_header_size = 0
        self._input_total_samples = 0
        self._original_frequency_resolution = 0
        self._original_time_resolution = 0
        self._original_nof_channels = 0

        # Load input file header
        self._load_filterbank_header()

        # Perform sanity checks
        self._check_parameters()

        # Define and update processing parameters
        self._nof_pfb_edge_points = 5 * (self._pfb_fft_length // 64)
        self._total_fine_channels = 16 * self._pfb_fft_length
        self._channel_skip = 2
        self._coarse_channels_to_process = self._original_nof_channels - self._channel_skip
        self._frequency_resolution = self._original_frequency_resolution * (32. / 27.) / self._pfb_fft_length
        self._start_frequency = self._original_start_frequency + \
                                (self._original_nof_channels - 1) * self._original_frequency_resolution + \
                                ((self._pfb_fft_length - self._nof_pfb_edge_points) / 2 - self._nof_pfb_edge_points) * \
                                self._frequency_resolution
        self._time_resolution = self._original_time_resolution * self._pfb_fft_length * self._time_average

        self._rescale_sigma = 6.0
        self._rescale_mean = 32768.0
        self._rescale_scale = self._rescale_mean / self._rescale_sigma

        # Window on time domain data
        window = np.hamming(self._pfb_fft_length)
        self._fft_filter = np.repeat(window[:, np.newaxis], self._original_nof_channels - self._channel_skip, axis=1)

        # Calculate channel frequencies
        self._channel_frequencies = self._calculate_channel_frequencies()

        # Calculate overlap required for over-and-save
        self._overlap_length = self._calculate_dispersion_overlap()

        # Create coherent dedisperser
        self._dedisperser = None
        if self._dm != 0:
            self._dedisperser = CoherentDedispersion(nof_channels=self._total_fine_channels,
                                                     nof_samples=self._nof_samples // self._pfb_fft_length,
                                                     dm=self._dm,
                                                     channel_frequencies=self._channel_frequencies,
                                                     time_resolution=self._time_resolution,
                                                     frequency_resolution=self._frequency_resolution,
                                                     invert_sign=False,
                                                     align_channels=False,
                                                     use_gpu=self._use_gpu)

        # If using GPU and GPU available, copy FFT window to GPU
        if self._use_gpu and CUPY_AVAILABLE:
            self._fft_filter = self._cp.asarray(self._fft_filter)

    def process(self):
        """ Start processing """

        # Create output file
        self._create_output_file()

        # Start timer
        t0 = time.time()

        # Select datatype to use based on number of bits in filterbank header
        datatype = self.complex_8t
        if self._input_header['nbits'] == 32:
            print("Interpreting as complex 16-bit")
            datatype = self.complex_16t

        # Open output file
        with open(self._output_filepath, 'ab+') as output_file:

            # Seek to end of file (after header)
            output_file.seek(0, 2)

            # Open input file
            with open(self._input_filepath, 'rb') as input_file:

                # Calculate number of chunks to process
                nof_chunks = int((self._time_seconds / self._original_time_resolution) / (self._nof_samples - self._overlap_length))

                for index in range(nof_chunks):
                    # Seek to required position
                    # Due to overlap and save, each buffer starts off at i * (nof_samples - overlap) * nof_channels
                    t_offset = int((self._time_offset / self._original_time_resolution) +
                                   index * (self._nof_samples - self._overlap_length))
                    input_file.seek(
                        self._input_header_size + t_offset * self._original_nof_channels * datatype.itemsize)

                    # Load the data
                    data = np.fromfile(input_file,
                                       count=self._nof_samples * self._original_nof_channels,
                                       dtype=datatype)

                    # Convert data to complex and reshape
                    data = (data['real'] + 1j * data['imag']).astype(np.complex64)
                    data = np.reshape(data, (self._nof_samples, self._original_nof_channels))

                    # Skip the first two coarse channels
                    data = data[:, self._channel_skip:]

                    # Reshape for channelisation
                    data = np.reshape(data, (int(self._nof_samples // self._pfb_fft_length),
                                             self._pfb_fft_length,
                                             self._coarse_channels_to_process))

                    # Copy input data to GPU, if using
                    if self._use_gpu and CUPY_AVAILABLE:
                        data = self._cp.asarray(data)

                    # Channelise data
                    data = self._channelise(data)

                    # Dedisperse data
                    data = self._dedisperse(data)

                    # Remove overlap if required
                    if self._overlap_length != 0:
                        shift = self._overlap_length // self._pfb_fft_length
                        data = data[shift // 2:-shift // 2, :]

                    # Compute power and time average
                    data = self.average_and_power(data)

                    # Normalise
                    data = self._normalise(data)

                    # Reverse spectra
                    data = self._cp.flip(data, axis=1)

                    # Cast and flatten data
                    data = data.flatten().astype(np.uint16)

                    # Copy data to host if using GPU
                    if self._use_gpu and CUPY_AVAILABLE:
                        data = self._cp.asnumpy(data)

                    # Write converted data chunk to file
                    output_file.write(data.flatten().tobytes())

                    # Update status
                    sys.stdout.write(
                        "Processed %d of %d [%.2f%%] in %.2fs (estimated total: ~%.2fs)   \r" %
                        ((index + 1), nof_chunks,
                         ((index + 1) / nof_chunks) * 100,
                         time.time() - t0,
                         ((time.time() - t0) / (index + 1)) * nof_chunks))
                    sys.stdout.flush()

    def _channelise(self, data):
        """ Perform fine channelisation """

        # Apply FFT windowing filter
        data *= self._fft_filter

        # Transpose data
        data = self._cp.transpose(data, (0, 2, 1))

        # Perform FFT
        data = self._cp.fft.fftshift(self._cp.fft.fft(data, axis=-1), axes=-1)

        # Reshape data to required shape. The first and last pfb_edge_points channels from each coarse
        # channels are discarded. They represent channels which lies on the overlapped regions in the
        # TPM PFB
        new_shape = (int(self._nof_samples // self._pfb_fft_length),
                     self._coarse_channels_to_process * (self._pfb_fft_length - 2 * self._nof_pfb_edge_points))
        data = self._cp.reshape(data[:, :, self._nof_pfb_edge_points:-self._nof_pfb_edge_points], new_shape)

        # Last two fine channels are discarded to end up with 1024 channel (hardcoded for now)
        return data[:, :-2]

    def _dedisperse(self, data):
        """ Dedisperse data """
        if self._dedisperser:
            return self._dedisperser.dedisperse(data)
        else:
            return data

    def average_and_power(self, data):
        """ Perform time averaging and compute power """

        # Reshape data so the time average is performed in its own dimensions
        new_shape = (data.shape[0] // self._time_average,
                     self._time_average,
                     self._total_fine_channels)
        data = self._cp.reshape(data, new_shape)

        # Compute absolute
        # NOTE: Computing the absolute this way is much faster than using np.abs(). This should be
        #       equivalent to np.power(np.abs(data), 2)
        data = data.real ** 2 + data.imag ** 2

        # Average in time
        return self._cp.mean(data, axis=1)

    def _normalise(self, data):
        """ Normalise data """
        # Calculate mean and standard deviation over time axis
        data_mean = self._cp.mean(data, axis=0)
        data_std = self._cp.std(data, axis=0)

        # Subtract the mean and divide by the standard deviation to produce a 32-bit floating point time-series
        # with zero mean and unit variance, then multiply the normalised time-series by 5461.33 (32768/6) and
        # add 32768 to produce a floating point time-series with mean of 32768 and standard deviation of 5461.33
        data = (((data - data_mean) / data_std) * self._rescale_scale) + self._rescale_mean

        # Round floating power values and clip to within uint16 range
        return self._cp.clip(self._cp.rint(data), 0, 2 ** 16 - 1)

    def _create_output_file(self):
        """ Create output file """

        # If file already exists, remove it
        if os.path.exists(self._output_filepath):
            print("Removing pre-existing file")
            os.remove(self._output_filepath)

        # Make a copy of the input header
        output_header = self._input_header.copy()

        # Update start time (due to offset and wing of first block)
        output_header['tstart'] += self._time_offset + (self._overlap_length // 2) * self._time_resolution

        # Convert time to MJD
        unix_time = datetime.fromtimestamp(output_header['tstart'])
        output_header['tstart'] = Time(unix_time, format="datetime", scale="utc").mjd

        # Update frequency and time resolution
        output_header['fch1'] = self._start_frequency
        output_header['foff'] = -self._frequency_resolution
        output_header['tsamp'] = self._time_resolution
        output_header['nchans'] = self._total_fine_channels

        # Set default params
        output_header['telescope_id'] = self._telescope_id
        output_header['src_raj'] = self._ra
        output_header['src_dej'] = self._dec
        output_header['source_name'] = self._source_name

        # Create output file
        create_filterbank_file(self._output_filepath, output_header, 16)

    def _load_filterbank_header(self):
        """ Load filterbank header """

        # Read header information and get header size
        self._input_header, self._input_header_size = read_header(self._input_filepath)

        # Calculate the number of spectra in the file
        self._input_total_samples = (os.path.getsize(self._input_filepath) - self._input_header_size) / \
                                    (self._input_header['nchans'] * self._input_header['nbits'] // 8)

        # Read required metadata
        self._original_frequency_resolution = abs(self._input_header['foff'])
        self._original_time_resolution = self._input_header['tsamp']
        self._original_nof_channels = self._input_header['nchans']

    def _check_parameters(self):
        # Sanity check on provided time offset
        if self._time_offset / self._original_time_resolution > self._input_total_samples:
            raise ValueError(f"Provided time offset ({self._time_offset}) is longer than "
                             f"file observation length ({self._input_total_samples * self._original_time_resolution})")

        # Sanity check on provided time seconds
        if self._time_seconds != -1 and \
                (self._time_seconds + self._time_offset) / self._original_time_resolution > self._input_total_samples:
            self._time_seconds = self._input_total_samples * self._original_time_resolution - self._time_offset
            print("Warning! Provided time seconds (after offset) beyond remaining observation length. "
                  f"Setting seconds to {self._time_seconds}")
        elif self._time_seconds == -1:
            self._time_seconds = self._input_total_samples * self._original_time_resolution - self._time_offset

    def _calculate_channel_frequencies(self):
        """ Calculate the center frequency of each frequency channel """
        df = self._frequency_resolution
        freqs = np.array([self._start_frequency - df * i for i in range(self._total_fine_channels)])
        return np.flip(freqs)

    def _calculate_dispersion_overlap(self):
        """ Calculate overlap required for the over-and-save method """

        # If DM is 0, then overlap is 0
        if self._dm == 0:
            return 0

        # Otherwise, compute overlap
        tdm = 8.3e6 * self._dm * self._frequency_resolution * np.min(self._channel_frequencies) ** -3
        overlap = int(ceil((tdm * 1e-3) / self._time_resolution))

        # Adjust overlap to be a multiple of factor (samples required for channelisation and averaging)
        if overlap % 2 != 0:
            overlap += 1
        overlap *= self._pfb_fft_length * self._time_average

        return overlap

    @staticmethod
    def _process_source_coordinate(coordinate: str):
        """ Process coordinate """
        # Coordinates should be of the form NN:NN:NN.N
        if not re.match(r'[+|-]?\d\d:\d\d:\d\d\.\d+', coordinate):
            raise ValueError(f"Provided coordinate {coordinate} is invalid")
        return float(coordinate.replace(":", ""))


# Script entry point
if __name__ == "__main__":
    from optparse import OptionParser
    from sys import argv

    parser = OptionParser(usage="usage: %filterbank_converter.py [options]")
    parser.add_option("-c", "--chunk-size", action="store", dest="chunk_size", type=int,
                      default=4194304, help="Chunk size [default: 4194304]")
    parser.add_option("-f", "--file", action="store", dest="file",
                      default="None", help="File to convert [default: None]")
    parser.add_option("-o", "--output-file", action="store", dest="output_file",
                      default="output.fil", help="Converted file name [default: output.fil]")
    parser.add_option("--fft-points", action="store", dest="fft_len", type=int,
                      default=64, help="N. points of the fft [default: 64]")
    parser.add_option("--start-frequency", action="store", dest="start_freq", type=float,
                      default=399.875, help="Frequency in MHz of channel 0 (bottom of the band) [default: 399.875 MHz]")
    parser.add_option("--channel-bandwidth", action="store", dest="bandwidth", type=float,
                      default=0.78125, help="Channel bandwidth in MHz [default: 0.78125 MHz ]")
    parser.add_option("--time-average", action="store", dest="time_average", type=int,
                      default=2, help="Time averaging factor [default: 2 ]")
    parser.add_option("--source-name", action="store", dest="source_name",
                      default="", help="Source name (default: no name)")
    parser.add_option("--ra", action="store", dest="ra",
                      default="00:00:00.0", help="Right Ascension of the source [default: 00:00:00.0")
    parser.add_option("--dec", action="store", dest="dec",
                      default="00:00:00.0", help="Declination of the source [default: 00:00:00.0")
    parser.add_option("--dm", action="store", dest="dm", type=float,
                      default=0, help="DM to coherently dedisperse [default: 0 (no dedispersion)")
    parser.add_option("--telescope-id", action="store", dest="tel_id", type=int,
                      default=30, help="ID of the Telescope [default: 30=Medicina Northern Cross")
    parser.add_option("--time-offset", action="store", dest="time_offset", type=int,
                      default=0, help="Time offset from start of file in seconds [default: 0s]")
    parser.add_option("--seconds", action="store", dest="seconds", type=int,
                      default=-1, help="Number of seconds from offset (if provided) to process [default: -1 (all)]")
    parser.add_option("--use-gpu", action="store_true", dest="use_gpu",
                      default=False, help="Use GPU [default: False")
    (conf, args) = parser.parse_args(argv[1:])

    print("Initialising BEST-FRB post-processing script")

    pp = PostProcessor(input_filepath=conf.file,
                       output_filepath=conf.output_file,
                       nof_samples=conf.chunk_size,
                       start_frequency=conf.start_freq,
                       pfb_fft_length=conf.fft_len,
                       time_average=conf.time_average,
                       dm=conf.dm,
                       time_offset=conf.time_offset,
                       time_seconds=conf.seconds,
                       source_name=conf.source_name,
                       ra=conf.ra,
                       dec=conf.dec,
                       telescope_id=conf.tel_id,
                       use_gpu=conf.use_gpu)

    print("Initialisation ready, starting processing")

    pp.process()

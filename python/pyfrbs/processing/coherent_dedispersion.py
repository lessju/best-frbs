import numpy as np

# Try importing CuPy. If an exception occurs, no GPU processing is possible
cp = None
try:
    from cupyx.scipy.fft import ifft as cu_ifft
    from cupyx.scipy.fft import fft as cu_fft
    import cupy as cp
except ImportError:
    pass  # print("CuPy not installed (or cannot be imported). GPUs cannot be used.")


class CoherentDedispersion(object):
    """ Class containing multiple coherent processing algorithms which
        can run on CPU and GPU (different implementations)"""

    def __init__(self, nof_channels: int, nof_samples: int, dm: float,
                 channel_frequencies: np.ndarray,
                 time_resolution: float,
                 frequency_resolution: float,
                 use_gpu: bool = False,
                 invert_sign: bool = False,
                 align_channels: bool = True):
        """ Class constructor
        :param nof_channels: Number of frequency channels in data block
        :param nof_samples: Number of time samples in data block
        :param dm: Dispersion Measure to dedisperse at
        :param channel_frequencies: The center frequency of each channel [MHz]
        :param time_resolution: Sampling time resolution [us]
        :param frequency_resolution: Frequency resolution [MHz]
        :param invert_sign: Invert transfer function sign
        :param align_channels: Align frequency channels together
        :param use_gpu: Use GPU acceleration if possible """

        # Local copy of parameters
        self._channel_frequencies = channel_frequencies.copy()
        self._frequency_resolution = frequency_resolution
        self._time_resolution = time_resolution * 1e6
        self._nof_channels = nof_channels
        self._nof_samples = nof_samples
        self._use_gpu = True if use_gpu and cp else False
        self._invert_sign = invert_sign
        self._align_channels = align_channels
        self._dm = dm

        # Pre-compute inverse transfer function
        self._transfer_function = self._compute_inverse_transfer_function()

        # If using GPU, initialise
        if self._use_gpu:
            self._transfer_function_gpu = self._initialise_gpu()

    def _compute_inverse_transfer_function(self):
        """ Compute inverse transfer function which will de-disperse the data. Optionally
        include shifts to align the frequency channels
        :returns: Transfer function to be used for processing
        :raises: NotImplementedError if include_channel_alignment is set to True """

        # Definition from Manchester and Taylor (1972)
        k_dm = 1 / 2.41e-4

        # Calculate Fourier frequencies
        f = np.fft.fftfreq(self._nof_samples, d=self._time_resolution)
        f0 = self._channel_frequencies[:, np.newaxis]

        # Intra-channel de-dispersion
        shift = + 2j * np.pi * 1e6 * k_dm * self._dm * f ** 2 / (f + f0) / f0 ** 2

        # Invert transfer function sign if required
        if self._invert_sign:
            shift *= -1

        # Include channel alignment in transfer function if required
        if self._align_channels:
            shift += - 2j * np.pi * 1e6 * (f + f0) * (0 - k_dm * self._dm * (1 / f0 ** 2 - 1 / f0.max() ** 2))

        # Return transfer function
        return np.transpose(np.exp(shift))

    def _initialise_gpu(self):
        """ Initialise data structures for GPU processing"""

        # Copy transfer function to GPU
        return cp.asarray(self._transfer_function)

    def dedisperse(self, input_data: np.ndarray):
        """ Dedisperse data blob
        :param input_data: Numpy array containing input data of shape (nof_samples, nof_channels)
        :returns: Dedispersed complex data with same shape as input"""

        # Sanity checks
        if input_data.shape != (self._nof_samples, self._nof_channels) and input_data.dtype != np.complex64:
            print(f"Invalid input data, must be of shape ({self._nof_channels}, {self._nof_samples}) and of type "
                  f"complex64")
            return None

        if self._use_gpu:
            # Check whether input data is already on GPU and if so do not perform memory transfers
            on_gpu = type(input_data) == cp.ndarray

            # Copy input data to GPU if required
            if on_gpu:
                input_gpu = input_data
            else:
                input_gpu = cp.array(input_data)

            # Perform fourier transforms
            output_gpu = cu_ifft(cu_fft(input_gpu, axis=0) * self._transfer_function_gpu, axis=0)

            # Copy result to host
            if on_gpu:
                return output_gpu
            else:
                output_data = cp.asnumpy(output_gpu)

                # Delete GPU arrays (GPU allocated memory will be removed by garbage collector)
                del input_gpu
                del output_gpu

                return output_data
        else:
            # Perform processing on CPU
            return np.fft.ifft(np.fft.fft(input_data, axis=0) * self._transfer_function, axis=0)


if __name__ == "__main__":
    data = np.zeros((24, 256 * 1024), dtype=np.complex64)
    c = CoherentDedispersion(24, 256 * 1024, 56.7, np.ones(24) * 410, 1.08, 0.78125, False)
    c.dedisperse(data)

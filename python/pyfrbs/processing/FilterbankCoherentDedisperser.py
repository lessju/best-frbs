from astropy.time import Time
from datetime import datetime

from pyfrbs.filterbank.filterbank import read_header, create_filterbank_file
from pathlib import Path
from math import ceil
import numpy as np
import sys
import os

from pyfrbs.processing.coherent_dedispersion import CoherentDedispersion


class FilterbankCoherentDedisperser:
    """ Class which coherently dedisperses an entire BEST FRB complex filterbank file"""

    # Custom complex format datatype stored in custom BEST FRB filterbank file
    complex_8t = np.dtype([('real', np.int8), ('imag', np.int8)])
    complex_16t = np.dtype([('real', np.int16), ('imag', np.int16)])

    def __init__(self, input_filepath: str, nof_samples: int, dm: float,
                 start_frequency: float,
                 output_filepath: str = None,
                 time_offset: int = 0,
                 time_seconds: int = -1,
                 use_gpu: bool = False):
        """ Class constructor
        :param input_filepath: Filepath for input data
        :param nof_samples: Dedispersion buffer size
        :param dm: DM to use
        :param start_frequency: Bottom frequency of first frequency channel
        :param output_filepath: Filepath to save dedispersed output
        :param time_offset: Offset in seconds from beginning of file to process
        :param time_seconds: Number of seconds from offset to process
        :param use_gpu: Use GPU for processing
        :raises: FileNotFoundError if input_filepath is incorrect
        :raises: NotADirectoryError if output_filepath is not within an existing directory
        :raises: ValueError if input file is not a filterbank file
        :raises: ValueError if time_offset is longer than file observation time"""

        # Sanity check: check input filepath
        if not os.path.exists(input_filepath):
            raise FileNotFoundError("Input filepath is invalid")

        if Path(input_filepath).suffix != ".fil":
            raise ValueError("Input file must be a filterbank file (with .fil suffix)")

        # Sanity check: check output filepath
        if output_filepath and not os.path.exists(Path(output_filepath).parent):
            raise NotADirectoryError("Output file must be in an existing directory")
        elif not output_filepath:
            p = Path(input_filepath)
            output_filepath = f"{p.parent}/{p.stem}_dedispersed.{p.suffix}"

        # Local copy of parameters
        self._input_filepath = input_filepath
        self._output_filepath = output_filepath
        self._start_frequency = start_frequency
        self._time_offset = time_offset
        self._time_seconds = time_seconds
        self._nof_samples = nof_samples
        self._use_gpu = use_gpu
        self._dm = dm

        # Input filter metadata placeholders
        self._channel_frequencies = None
        self._frequency_resolution = 0
        self._input_total_samples = 0
        self._input_header_size = 0
        self._time_resolution = 0
        self._nof_channels = 0
        self._input_header = None

        # Load input file header
        self._load_filterbank_header()

        # Sanity check on provided time offset
        if self._time_offset / self._time_resolution > self._input_total_samples:
            raise ValueError(f"Provided time offset ({self._time_offset }) is longer than "
                             f"file observation length ({self._input_total_samples * self._time_resolution})")

        # Sanity check on provided time seconds
        if self._time_seconds != -1 and \
                (self._time_seconds + self._time_offset) / self._time_resolution > self._input_total_samples:
            self._time_seconds = self._input_total_samples - (self._time_offset / self._time_resolution)
            print("Warning! Provided time seconds (after offset) beyond remaining observation length. "
                  f"Setting seconds to {self._time_seconds}")
        elif self._time_seconds == -1:
            self._time_seconds = self._input_total_samples * self._time_resolution - self._time_offset

        # Calculate channel frequencies
        self._channel_frequencies = self._calculate_channel_frequencies()

        # Calculate overlap required for over-and-save
        self._overlap_length = self._calculate_overlap()

        # Create coherent processing instance
        self._dedisperser = CoherentDedispersion(nof_channels=self._nof_channels,
                                                 nof_samples=self._nof_samples,
                                                 dm=self._dm,
                                                 channel_frequencies=self._channel_frequencies,
                                                 time_resolution=self._time_resolution * 1e6,  # Convert to us
                                                 frequency_resolution=self._frequency_resolution,
                                                 invert_sign=False,
                                                 align_channels=True,
                                                 use_gpu=self._use_gpu)

    def process(self, output_filterbank=False):
        """ Process file
         :param output_filterbank: Generate filterbank with power values. This will also
         perform the required conversions for the file to be processable by third party software"""

        # Define data type being used based on nof bits in filterbank file
        datatype = self.complex_8t
        if self._input_header['nbits'] == 32:
            datatype = self.complex_16t

        # Create output file
        self._create_output_file(output_filterbank)

        # Open output file
        with open(self._output_filepath, 'ab+') as output_file:

            # Seek to end of file (after header)
            output_file.seek(0, 2)

            # Open input file
            with open(self._input_filepath, 'rb') as input_file:

                # Calculate number of chunks to process
                if self._time_seconds == -1:
                    nof_chunks = int(self._input_total_samples // (self._nof_samples - self._overlap_length) - (
                            self._time_offset / self._time_resolution))
                else:
                    nof_chunks = int((self._time_seconds / self._time_resolution) / self._nof_samples)

                for index in range(nof_chunks):
                    # Seek to required position
                    # Due to overlap and save, each buffer starts off at i * (nof_samples - overlap) * nof_channels
                    t_offset = int((self._time_offset / self._time_resolution) +
                                   index * (self._nof_samples - self._overlap_length))
                    input_file.seek(self._input_header_size + t_offset * self._nof_channels * datatype.itemsize)

                    # Load the data
                    data = np.fromfile(input_file, count=self._nof_samples * self._nof_channels, dtype=datatype)

                    # Convert data to complex and reshape
                    data = (data['real'] + 1j * data['imag']).astype(np.complex64)
                    data = np.reshape(data, (self._nof_samples, self._nof_channels))

                    # Dedisperse data
                    data = self._dedisperser.dedisperse(data)

                    # Get required segment from data
                    data = data[self._overlap_length // 2:-self._overlap_length // 2, :]

                    # Data conversion before writing to file
                    if output_filterbank:
                        # Convert to power and scale to uint16
                        data = np.abs(data).astype(np.uint16)
                    else:
                        # Stack real and imaginary components and cast to type float32
                        data = np.dstack((data.real.astype(np.float32), data.imag.astype(np.float32)))

                    # Write data to file
                    output_file.write(data.tobytes())

                    # Update status
                    sys.stdout.write(
                        "Processed %d of %d [%.2f%%]      \r" % (index, nof_chunks,
                                                                 (index / nof_chunks) * 100))
                    sys.stdout.flush()

    def _load_filterbank_header(self):
        """ Load filterbank header """

        # Read header information and get header size
        self._input_header, self._input_header_size = read_header(self._input_filepath)

        # Calculate the number of spectra in the file
        self._input_total_samples = (os.path.getsize(self._input_filepath) - self._input_header_size) / \
                                    (self._input_header['nchans'] * self._input_header['nbits'] // 8)

        # Read required metadata
        self._frequency_resolution = abs(self._input_header['foff'])
        self._time_resolution = self._input_header['tsamp']
        self._nof_channels = self._input_header['nchans']

    def _create_output_file(self, generate_filterbank=False):
        """ Create output file
         :param generate_filterbank: Generate correctly formed filterbank file"""

        # If file already exists, remove it
        if os.path.exists(self._output_filepath):
            print("Removing pre-existing file")
            os.remove(self._output_filepath)

        # Make a copy of the input header
        output_header = self._input_header.copy()

        # Update start time (due to offset and wing of first block)
        output_header['tstart'] += self._time_offset + self._overlap_length * self._time_resolution

        # If generating a filterbank file, we need to perform additional operations]
        if generate_filterbank:
            # Convert time to MJD
            unix_time = datetime.fromtimestamp(output_header['tstart'])
            output_header['tstart'] = Time(unix_time, format="datetime", scale="utc").mjd

            # Update channels
            output_header['fch1'] = self._channel_frequencies[-1]
            output_header['foff'] = -self._frequency_resolution

            # Set default params
            output_header['telescope_id'] = 30  # Note hardcoded Medicina identifier
            output_header['src_raj'] = 0
            output_header['src_dej'] = 0
            output_header['source_name'] = ""

            # Create output file
            create_filterbank_file(self._output_filepath, output_header, 16)
        else:
            create_filterbank_file(self._output_filepath, output_header, 64)

    def _calculate_channel_frequencies(self):
        """ Calculate the center frequency of each frequency channel """
        df = self._frequency_resolution
        return np.array([self._start_frequency + i * df + df / 2 for i in range(self._nof_channels)])

    def _calculate_overlap(self):
        """ Calculate overlap required for the over-and-save method """
        tdm = 8.3e6 * self._dm * self._frequency_resolution * np.max(self._channel_frequencies) ** -3
        overlap = int(ceil((tdm * 1e-3) / self._time_resolution))

        # If calculated overlap is not a multiple of two, add one
        if overlap % 2 != 0:
            overlap += 1

        return overlap

    def print_info(self):
        """ Display information on setup """
        print("------------- Coherent Dedispersion Setup -------------")
        print("-------------------------------------------------------")
        print(f"Input file:            {self._input_filepath}")
        print(f"Output file:           {self._output_filepath}")
        print(f"Start offset:          {self._time_offset}s ({int(self._time_offset / self._time_resolution)}) samples")
        print(f"Seconds to process:    {self._time_seconds}s ({int(self._time_seconds / self._time_resolution)}) samples")
        print(f"Frequency resolution:  {self._frequency_resolution}MHz")
        print(f"Time resolution:       {self._time_resolution}s")
        print(f"Center start channel:  {self._channel_frequencies[0]:.2f}MHz")
        print(f"Center stop channel:   {self._channel_frequencies[-1]:.2f}Mhz")
        print(f"DM:                    {self._dm}")
        print(f"Overlap:               {self._overlap_length * self._time_resolution * 1e3:.2f}ms ({self._overlap_length} samples)")
        print("-------------------------------------------------------\n")

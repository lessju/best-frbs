import ast
import datetime
import logging as log
import logging.config as log_config
import time
import os
import re
from logging.handlers import TimedRotatingFileHandler

import configparser

from pyfrbs import settings


class FRBConfig:
    def __init__(self, config_file_name=None, config_options=None, config_name=None):
        """
        Initialise the FRB configuration
        :param config_file_name: The path to the FRB configuration file
        :param config_options: Configuration options to override the default config settings
        :param config_name: Configuration name to use
        """

        # Set configuration files root
        self._config_root = os.path.expanduser(os.environ['FRB__CONFIG_DIRECTORY'])

        # Check if path is valid
        if not os.path.exists(self._config_root) or not os.path.isdir(self._config_root):
            log.error("FRB__CONFIG_DIRECTORY is invalid ({}), path does not exist".format(self._config_root))
            return

        # The configuration parser of the FRB system
        self._parser = configparser.RawConfigParser()
        self._loaded = False
        now = datetime.datetime.utcnow().isoformat('T')

        if config_name:
            self.log_filepath = self._set_logging_config(config_name)
        else:
            # Load the logging configuration
            try:
                self.log_filepath = self._set_logging_config(config_options['observation']['name'])
            except KeyError:
                self.log_filepath = self._set_logging_config('FRB_observation_' + now)
            except TypeError:
                self.log_filepath = self._set_logging_config('FRB_observation_' + now)

        if config_file_name:
            # Set the configurations from file
            full_path = os.path.join(self._config_root, config_file_name)
            self._load_from_file(full_path)
        else:
            # load the frb default configuration
            self._load_from_file(os.path.join(self._config_root, 'default.ini'))

        # Load configuration
        self.load()

        if config_options:
            # Override the configuration with settings passed on in the config_options dictionary
            self.update_config(config_options)

    def _load_from_file(self, config_filepath):
        """
        Load the configuration of the FRB application into the settings.py file
        :param config_filepath: The path to the configuration file
        :return: None
        """

        # Load the configuration file requested by the user
        try:
            with open(os.path.expanduser(config_filepath)) as f:
                self._parser.read_file(f)

                if os.path.islink(config_filepath):
                    config_filepath = os.path.realpath(config_filepath)
                log.info('Loaded the {} configuration file.'.format(config_filepath))

        except IOError:
            log.warning('Config file at {} was not found'.format(config_filepath))
            exit()
        except configparser.Error:
            log.exception('An error has occurred whilst parsing configuration file at: %s', config_filepath)
            exit()

    def update_config(self, config_options):
        """
        Override the configuration settings using an external dictionary
        :param config_options:
        """
        if not config_options:
            return None

        for section in config_options:
            if isinstance(config_options[section], dict):
                # If settings is a dictionary, add it as a section
                for (key, value) in config_options[section].items():
                    self._parser.set(section, key, value)
            else:
                # Else, put the configuration in the observations settings
                self._parser.set('observation', section, config_options[section])

        # Re-load the system configuration upon initialisation
        self.load()

    def _set_logging_config(self, observation_name):
        """
        Load the logging configuration
        """

        # Load the logging configuration file
        config_filepath = os.path.join(self._config_root, 'logging.ini')

        self._load_from_file(config_filepath)
        log_config.fileConfig(config_filepath, disable_existing_loggers=False)

        # Override the logger's debug level
        log.getLogger().setLevel('INFO')

        # Create directory for file log
        directory = os.path.join('/var/log/frb', '{:%Y_%m_%d}'.format(datetime.datetime.utcnow()))
        if not os.path.exists(directory):
            os.makedirs(directory)

        log_path = os.path.join(directory, observation_name + '.log')

        handler = TimedRotatingFileHandler(log_path, when="h", interval=1, backupCount=5, utc=True)
        formatter = log.Formatter(self._parser.get('formatter_formatter', 'format'))
        formatter.converter = time.gmtime
        handler.setFormatter(formatter)
        log.getLogger().addHandler(handler)

        return log_path

    def get(self, section, key):
        return self._parser.get(section, key)

    def load(self):
        """
        Use a config parser to build the settings module. This module is accessible through
        the application
        """

        # Pre-compile regular expression for speed-up
        literal_re = re.compile("^True|False|[0-9]+(\.[0-9]*)?$")

        # Temporary class to create section object in settings file
        class Section(object):
            def settings(self):
                return self.__dict__.keys()

        # Loop over all sections in config file
        for section in self._parser.sections():
            # Create instance to inject into settings file
            instance = Section()

            for (k, v) in self._parser.items(section):
                # If value is a string, interpret it
                if isinstance(v, str) or isinstance(v, unicode):
                    # Check if value is a number or boolean
                    if re.match(literal_re, v) is not None:
                        setattr(instance, k, ast.literal_eval(v))

                    # Check if value is a list
                    elif re.match("^\[.*\]$", re.sub('\s+', '', v)):
                        setattr(instance, k, ast.literal_eval(v))

                    # Otherwise it is a string
                    else:
                        setattr(instance, k, v)
                else:
                    setattr(instance, k, v)

            # Add object instance to settings
            setattr(settings, section, instance)

        settings.loaded = True

        log.info('Configurations successfully loaded.')

    def is_loaded(self):
        return self._loaded

    @staticmethod
    def to_dict():
        """
        :return: The dictionary representation of the Frb configuration
        """
        return {section: settings.__dict__[section].__dict__ for section in settings.__dict__.keys() if
                not section.startswith('__')}

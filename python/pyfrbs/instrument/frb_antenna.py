#!/usr/bin/env python
"""
 FRB Antenna Interface
  - Read position
  - Point Antenna
"""

__author__ = "Andrea Mattana"
__copyright__ = "Copyright 2021, Istituto di RadioAstronomia, INAF, Italy"
__credits__ = ["Andrea Mattana"]
__license__ = "GPL"
__version__ = "1.0"
__maintainer__ = "Andrea Mattana"

import socket
import time
import os
import glob
import datetime
import calendar

log_path = "/var/log/frb/POINTING"


def dt_to_timestamp(d):
    return calendar.timegm(d.timetuple())


def get_date(f, offset_start=-14):
    return dt_to_timestamp(datetime.datetime.strptime(f[offset_start:offset_start + 10], "%Y-%m-%d"))


def check_valid_week(f):
    tstamp_today = get_date(datetime.datetime.strftime(datetime.datetime.utcnow(), "%Y-%m-%d"), offset_start=0)
    if tstamp_today - get_date(f) > (6 * 24 * 3600):
        return False
    else:
        return True


def get_current_timestring(format="%Y-%m-%d  %H:%M:%S  "):
    now = datetime.datetime.utcnow()
    return datetime.datetime.strftime(now, format)


def get_current_logtimestring(format="%Y-%m-%d  %H:%M:%S  "):
    now = datetime.datetime.utcnow()
    return str(dt_to_timestamp(now)) + "  " + datetime.datetime.strftime(now, format)


def log_pointing(message):
    """
    Checks if a log not older than a week exists.
    If True, append server messages,
    otherwise creates a new one and log server messages

    @param message: The string that will logged
    """
    if not os.path.exists(log_path):
        os.mkdir(log_path)
    lista = sorted(glob.glob(log_path + "/FRB_POINTING_*.txt"))
    if len(lista) and check_valid_week(lista[-1]):
        logfile = lista[-1]
        attr = "a"
    else:
        logfile = log_path + "/FRB_POINTING_" + datetime.datetime.strftime(datetime.datetime.utcnow(), "%Y-%m-%d.txt")
        attr = "w"
    with open(logfile, attr) as f:
        f.write(message + "\n")
        f.flush()


def read_frb_pos(s, addr="192.167.189.74", port=7200, log=True):
    """
    Send command to point the FRB Antennas

    @param addr: (Pointing Server TCP/IP address, string)
    @param port: (Pointing Server TCP/Port, int)
    @param log:  (Enable Log, bool)

    @rtype: Server reply String

        - Example:

            ans = read_frb_pos()
            print ans
                FRB(2N) ONSOURCE 45.8,45.9,45.8,45.9,45.8,45.9,45.8,45.9

    """
    #s = 0
    data = "ERROR - Connection (%s:%d)" % (addr, port)
    try:
        #s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        #s.settimeout(2)
        #s.connect((addr, port))
        s.sendall("frb")
        time.sleep(0.3)
        data = s.recv(1024)
        #s.close()
    except:
        pass
    #del s
    return data


def write_frb_pos(s, newpos, addr="192.167.189.74", port=7200, log=True):
    """
    Send command to point the FRB Antennas

    @param newpos: Target declination (float)
    @param addr: (Pointing Server TCP/IP address, string)
    @param port: (Pointing Server TCP/Port, int)
    @param log:  (Enable Log, bool)
    @return: Server reply String

        - Example:

            ans = write_frb_pos(50.5)
            print ans
                FRB(2N) MOVING 45.8,45.9,45.8,45.9,45.8,45.9,45.8,45.9

    """
    #s = 0
    data = "ERROR - Connection (%s:%d)" % (addr, port)
    try:
        #s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        #s.connect((addr, port))
        #s.settimeout(3)
        s.sendall("frb " + str(newpos))
        time.sleep(1)
        data = s.recv(1024)
        #s.close()
    except:
        print("ERROR - Sending new Position " + str(pos))
        pass
    #del s
    return data


if __name__=="__main__":
    from optparse import OptionParser
    from sys import argv
    op = OptionParser(usage="python frb_antenna.py [--point=declination(float)]")
    op.add_option("--addr", dest="addr", default="192.167.189.74", help="TCP Server IP address")
    op.add_option("--port", type="int", dest="port", default=7200, help="TCP Server Port number")
    op.add_option("--point", dest="point", default="", help="Target declination")
    op.add_option("--retries", dest="retries", default=3, help="Number of retries (default: 3)")
    opts, args = op.parse_args(argv[:])

    ip = opts.addr
    port = int(opts.port)
    pos = None

    sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    sock.connect((ip, port))
    sock.settimeout(3)

    try:
        data = read_frb_pos(sock, ip, port)
        print(get_current_timestring(), data, "\n")
        log_pointing(get_current_logtimestring() + data)
        if not opts.point == "" and not data.split(" ")[0] == "ERROR":
            try:
                pos = float(opts.point)
            except:
                print("Point Argument ERROR: A number is required!\n")
                exit()
            print("Sending new position (%f) to FRB Antenna (2N)\n" % pos)
            data = write_frb_pos(sock, pos, ip, port)
            time.sleep(0.3)
            print(get_current_timestring(), data)
            retries = 1
            while data.split(" ")[0] == "ERROR" and retries < opts.retries:
                retries = retries + 1
                data = write_frb_pos(sock, pos, ip, port)
                print(get_current_timestring(), data)
                log_pointing(get_current_logtimestring() + data)
            if not data.split(" ")[0] == "ERROR":
                try:
                    while not data.split(" ")[1] == "ONSOURCE":
                        time.sleep(1)
                        data = read_frb_pos(sock, ip, port)
                        print(get_current_timestring(), data)
                        log_pointing(get_current_logtimestring() + data)
                    print("\n\nFRB Antenna OnSource!!\n")
                except KeyboardInterrupt:
                    print("\nProgram terminated by user...\n")
            else:
                print("\nUnable to point FRB Antenna for Server communication error!\n")
        sock.close()
    except KeyboardInterrupt:
        sock.close()
    del sock



from datetime import datetime
from enum import Enum
import subprocess
import threading
import logging
import signal
import glob
import time
import pytz
import sys
import os

from pyfrbs.processing.best_frb_post_processor import PostProcessor
from pyfrbs.digital_backend.digital_backend import Station
from pyfrbs.frb_config import FRBConfig
from pyfrbs import settings

# Global values
from pyfrbs.processing.correlator import Correlator

PHAROS_CHANNEL_PATH = "/opt/frbs/bin/pharos_channel"
PHAROS_BEAM_PATH = "/opt/frbs/bin/pharos_beam"


class SchedulerParameterException(Exception):
    """ Exception representing an invalid or erroneous scheduler parameter """
    pass


class DigitalBackendException(Exception):
    """ Exception representing an exception in the digital backend """
    pass


class DAQMODE(Enum):
    BEAM = 1
    SINGLE_CHANNEL = 2
    MULTI_CHANNEL = 3


class BESTFRBScheduler:

    def __init__(self, daq_mode: DAQMODE, directory: str, start_time: str,
                 duration: int, calibration_file: str = None, correlate: bool = False,
                 ra: str = None, dec: str = None, source_name: str = None,
                 channel_to_transmit: int = -1, combine_beams=False, configuration_file: str = None):
        """ Class initializer """

        # Load default configuration
        FRBConfig(config_file_name=configuration_file)

        # Set up parameters
        self._channel_to_transmit = channel_to_transmit
        self._calibration_file = calibration_file
        self._combine_beams = combine_beams
        self._source_name = source_name
        self._start_time = start_time
        self._directory = directory
        self._duration = duration
        self._daq_mode = daq_mode
        self._ra = ra
        self._dec = dec

        # Flags determining whether to post-process the data
        self._post_process_beam_data = False  # Determine during check_parameters
        self._correlate_channel_data = correlate

        # Observation stopping event and timer
        self._timer = threading.Timer(self._duration, self._stop_observation)
        self._stop_event = threading.Event()
        self._stop = False

        # Check parameters
        self._check_parameters()

        # Base arguments for all processing modes
        self._base_arguments = ["-e", settings.instrument.lmc_interface,
                                "-i", settings.instrument.lmc_ip,
                                "-d", self._directory]

        # Connect to TPM (program and initialise if required)
        self._connect_to_backend()

        # Placeholder for original signal handlers
        self._python_signal_handlers = {}

    def start_schedule(self):
        """ Start the schedule """

        # Assign signal handlers
        self._assign_signal_handlers()

        # Determine observation start time
        if self._start_time != "now":
            logging.info("Setting observation to run at {}".format(self._start_time))

            # Set start time. Shave
            start_time = pytz.utc.localize(datetime.strptime(self._start_time, "%d/%m/%Y_%H:%M"))

            # Wait for start time (busy waiting for now)
            while pytz.utc.localize(datetime.utcnow()) < start_time:
                time.sleep(1)

                if self._stop_event.is_set():
                    logging.info("Service interrupted, exiting")
                    exit()
        else:
            logging.info("Starting observation now")

        # Set up timer to finish to wait for observation finish (for beam and single channel modes)
        if self._daq_mode != DAQMODE.MULTI_CHANNEL:
            logging.info(f"Setting timer to stop observation in {self._duration}s")
            self._timer.start()

        # Call appropriate observing function
        if self._daq_mode == DAQMODE.BEAM:
            self._beam_observation()
        elif self._daq_mode == DAQMODE.SINGLE_CHANNEL:
            self._single_channel_observation()
        else:
            self._multi_channel_observation()

    def stop_schedule(self):
        """ Sets the stopping event """
        self._stop_event.set()

    def _check_parameters(self):
        """ Check scheduling parameters """

        # Check start time
        if self._start_time != "now":
            try:
                start_time = pytz.utc.localize(datetime.strptime(self._start_time, "%d/%m/%Y_%H:%M"))
                curr_time = pytz.utc.localize(datetime.utcnow())

                wait_seconds = (start_time - curr_time).total_seconds()
                if wait_seconds < 10:
                    logging.error("Scheduled start time must be at least 10s in the future")
                    exit()
            except ValueError:
                raise SchedulerParameterException(f"Start time {self._start_time} "
                                                  f"is not a valid format (\"%d/%m/%Y_%H:%M\")")

        # Check duration
        if self._duration < 10:
            raise SchedulerParameterException(f"Duration should be more than 10 seconds")

        # Check output directory
        if not os.path.exists(self._directory):
            raise SchedulerParameterException(f"Invalid output directory ({self._directory})")
        elif not self._directory.endswith("/"):
            self._directory += "/"

        # Check if output directory is writable
        if not os.access(self._directory, os.W_OK):
            raise SchedulerParameterException(f"Permission error on output directory ({self._directory})")

        # Check calibration file (if required)
        if self._daq_mode == DAQMODE.BEAM:
            if self._calibration_file is None:
                raise SchedulerParameterException("Calibration file must be provided for beam observation")
            elif not os.path.exists(self._calibration_file):
                raise SchedulerParameterException(f"Provided calibration file ({self._calibration_file}) does not exist")

        # Check whether channel to transmit is specified
        if self._daq_mode == DAQMODE.SINGLE_CHANNEL:
            if not (0 <= self._channel_to_transmit <= settings.observation.beam_channels):
                raise SchedulerParameterException(f"A valid channel to transmit (0 - {settings.observation.beam_channels}) "
                                                    "is required for single channel mode")

        # Check post-processing parameters
        checks = [self._ra != "00:00:00.0", self._dec != "00:00:00.0", self._source_name != ""]
        if all(checks) and self._daq_mode == DAQMODE.BEAM:
            logging.info("Automatically post-processing data after beam observation")
            self._post_process_beam_data = True
        elif any(checks) and not self._daq_mode == DAQMODE.BEAM:
            logging.warning("All post-processing parameters (ra, dec, source-name) required to enable post-processing.")

    def _beam_observation(self):
        """ Schedule a beam observation """
        # Set up observation arguments
        arguments = self._base_arguments + ["-t", str(len(settings.instrument.tpm_ips)),
                                            "-c", str(settings.observation.beam_channels),
                                            "-f", str(settings.data_acquisition.start_channel),
                                            "-l", str(settings.data_acquisition.stop_channel)]

        if self._combine_beams:
            arguments += ["-b"]

        # Start data acquisition
        daq_process = subprocess.Popen([PHAROS_BEAM_PATH] + arguments)

        # Wait for a while
        time.sleep(2)

        # Wait for observation to finish
        self._wait_for_stop()

        # All done, clear everything
        logging.info("Observation ended")
        self._timer.cancel()
        daq_process.kill()

        # Reset signal handlers
        self._assign_signal_handlers(original=True)

        # Post-process data if required
        if self._post_process_beam_data:
            self._post_process_beam()

    def _single_channel_observation(self):
        """ Schedule single channel observation """
        # Set up observation arguments
        arguments = self._base_arguments + ["-a", str(settings.observation.nof_antennas),
                                            "-c", str(settings.data_acquisition.nof_continuous_channels)]

        # Transmit required channel
        self._station.stop_data_transmission()
        self._station.send_channelised_data_continuous(self._channel_to_transmit)

        # Start data acquisition
        daq_process = subprocess.Popen([PHAROS_CHANNEL_PATH] + arguments)

        # Wait for a while
        time.sleep(2)

        # Wait for observation to finish
        self._wait_for_stop()

        # All done, clear everything
        logging.info("Observation ended")
        self._timer.cancel()
        daq_process.kill()

        # Reset signal handlers
        self._assign_signal_handlers(original=True)

        # Correlate generated files if required
        if self._correlate_channel_data:
            self._correlate_observation()

    def _multi_channel_observation(self):
        """ Schedule multi-channel observation """
        # Set up observation arguments
        arguments = self._base_arguments + ["-a", str(settings.observation.nof_antennas),
                                            "-c", str(settings.data_acquisition.nof_continuous_channels),
                                            "-s", str(109 * 6000)]

        # Stop any data transmission from the TPM
        self._station.stop_data_transmission()

        # Start data acquisition
        daq_process = subprocess.Popen([PHAROS_CHANNEL_PATH] + arguments)

        # Wait for a while
        time.sleep(2)

        # Go over all the channels
        logging.info("Observation started")
        for channel in range(settings.data_acquisition.start_channel,
                             settings.data_acquisition.stop_channel + settings.data_acquisition.delta_channel,
                             settings.data_acquisition.delta_channel):
            self._station.stop_data_transmission()
            self._station.send_channelised_data_continuous(channel)
            self._stop_event.wait(settings.data_acquisition.channel_time)

            if self._stop_event.is_set():
                logging.info("Service interrupted, exiting")
                break

        # All done, clear everything
        logging.info("Observation ended")
        self._timer.cancel()
        daq_process.kill()

        # Reset signal handlers
        self._assign_signal_handlers(original=True)

        # Correlate generated files if required
        if self._correlate_channel_data:
            self._correlate_observation()

    def _correlate_observation(self):
        """ Correlate channel data """

        print(settings.data_acquisition.nof_continuous_channels, settings.observation.nof_antennas)
        correlator = Correlator(nof_channels=settings.data_acquisition.nof_continuous_channels,
                                nof_antennas=settings.observation.nof_antennas)

        for data_file in os.listdir(self._directory):
            if data_file.endswith(".dat") and data_file.startswith("channel"):
                print(f"Processing {data_file}")
                correlator.process_file(os.path.join(self._directory, data_file))

    def _post_process_beam(self):
        """ Post process beam data"""
        # Find filterbank files in directory
        files = glob.glob(os.path.join(os.path.abspath(os.path.expanduser(self._directory)), "*.fil"))

        if len(files) == 0:
            logging.warning("No filterbank files detected, cannot post-process")

        # Process first file found
        logging.info(f"Post-processing observation {files[0]}")
        pp = PostProcessor(input_filepath=files[0],
                           source_name=self._source_name,
                           ra=self._ra,
                           dec=self._dec,
                           use_gpu=True)
        pp.process()

        logging.info("Finished post-processing")

    def _connect_to_backend(self):
        """ Connect to the digital backend. """

        # Connect to the digital backend
        self._station = Station()
        self._station.connect()

        # Check if backend is properly formed, and if not, program and initialise
        if not self._station.properly_formed_station:
            try:
                # Program
                self._station.connect(program=True, initialise=True)
            except Exception as e:
                raise DigitalBackendException(f"Failure during programming or initialising station: {e}")

        # Enable or disable beam data transmission depending on what DAQ mode is required
        if self._daq_mode == DAQMODE.BEAM:
            self._station['fpga1.dsp_regfile.beamf_add_ctrl.enable'] = 0x1
            self._station['fpga2.dsp_regfile.beamf_add_ctrl.enable'] = 0x1
        else:
            self._station['fpga1.dsp_regfile.beamf_add_ctrl.enable'] = 0x0
            self._station['fpga2.dsp_regfile.beamf_add_ctrl.enable'] = 0x0

        # Apply calibration file if performing a beamforming observation
        if self._daq_mode == DAQMODE.BEAM:
            try:
                self._station.apply_coefficients(self._calibration_file, 0, -1)
            except Exception as e:
                raise DigitalBackendException(f"Failure during downloading coefficients: {e}")

    def _wait_for_stop(self):
        """ Wait for observation to finish """

        logging.info("Observation started")
        while not self._stop:
            self._stop_event.wait(5)

            if self._stop_event.is_set():
                logging.info("Service interrupted, exiting")
                self._stop = True

    def _stop_observation(self):
        """ Signal a stop obsevation """
        self._stop = True

    def _signal_handler(self, signum, frame):
        """ Signal handler which set stopping event """
        logging.info("Service interrupted")
        self._stop_event.set()

    def _assign_signal_handlers(self, original=False):
        """ Assign signal handlers """
        if original:
            signal.signal(signal.SIGTERM, self._python_signal_handlers[signal.SIGTERM])
            signal.signal(signal.SIGHUP, self._python_signal_handlers[signal.SIGHUP])
            signal.signal(signal.SIGINT, self._python_signal_handlers[signal.SIGINT])
        else:
            self._python_signal_handlers[signal.SIGTERM] = signal.signal(signal.SIGTERM, self._signal_handler)
            self._python_signal_handlers[signal.SIGHUP] = signal.signal(signal.SIGHUP, self._signal_handler)
            self._python_signal_handlers[signal.SIGINT] = signal.signal(signal.SIGINT, self._signal_handler)


if __name__ == "__main__":

    # Command line options
    from optparse import OptionParser

    p = OptionParser()
    p.set_usage('bestfrb_scheduler.py [options]')
    p.set_description(__doc__)

    p.add_option("-s", "--starttime", action="store", dest="starttime",
                 default="now",
                 help="Time at which to start observation.Format: dd/mm/yyyy_hh:mm. Default: now")
    p.add_option("-l", "--duration", action="store", dest="duration",
                 type="int", default="120", help="Observation length [default: 120]")
    p.add_option("--multi-channel", action="store_true", dest="multi_channel",
                 default=False, help="Multi-channel mode (-l will be ignored) [default: False]")
    p.add_option("--channelised", action="store_true", dest="channelised", default=False,
                 help="Acquire channelised data. If False acquired beamformed data [default: False]")
    p.add_option("--channel-to-transmit", action="store", dest="channel_to_transmit", default=-1, type=int,
                 help="Channel to transmit (required for single-channel observation) [default: -1]")
    p.add_option("--directory", action="store", dest="directory", default='.',
                 help="Directory to save data in [default: '.']")
    p.add_option("--configuration-file", action="store", dest="configuration_file",
                 default=None, help="Configuration file to use (overrides default)")
    p.add_option("--calibration-file", action="store", dest="calibration_file", default=None,
                 help="Calibration file, required for beamformed observation  [default: None]")
    p.add_option("--combine-beams", action="store_true", dest="combine_beams", default=False,
                 help="Combine beams from multiple TPMs [default: False]")
    p.add_option("--correlate", action="store_true", dest="correlate", default=False,
                 help="Correlate data after channel observation [default: False]")
    p.add_option("--source-name", action="store", dest="source_name",
                 default="", help="Source name (default: no name)")
    p.add_option("--ra", action="store", dest="ra",
                 default="00:00:00.0", help="Right Ascension of the source [default: 00:00:00.0")
    p.add_option("--dec", action="store", dest="dec",
                 default="00:00:00.0", help="Declination of the source [default: 00:00:00.0")

    opts, args = p.parse_args(sys.argv[1:])

    # Sanity checks
    if opts.configuration_file is not None:
        opts.configuration_file += '.ini' if not opts.configuration_file.endswith('.ini') else ''

    # Create daq mode
    daq_mode = DAQMODE.BEAM
    if opts.multi_channel:
        daq_mode = DAQMODE.MULTI_CHANNEL
    elif opts.channelised:
        daq_mode = DAQMODE.SINGLE_CHANNEL

    obs_scheduler = BESTFRBScheduler(daq_mode=daq_mode,
                                     directory=opts.directory,
                                     start_time=opts.starttime,
                                     duration=opts.duration,
                                     calibration_file=opts.calibration_file,
                                     configuration_file=opts.configuration_file,
                                     channel_to_transmit=opts.channel_to_transmit,
                                     ra=opts.ra,
                                     dec=opts.dec,
                                     combine_beams=opts.combine_beams,
                                     source_name=opts.source_name,
                                     correlate=opts.correlate)
    obs_scheduler.start_schedule()

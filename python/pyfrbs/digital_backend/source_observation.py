from datetime import datetime, timedelta
from time import sleep
from sys import stdout
import subprocess
import threading
import logging
import signal
import sched
import glob
import time
import sys
import os
import pytz

from pyfrbs.digital_backend.digital_backend import Station
from pyfrbs.frb_config import FRBConfig
from pyfrbs import settings

# python  tpm_frb.py -IP

# Global variables
from pyfrbs.processing.best_frb_post_processor import PostProcessor

stop = False

multi_channel_mode = False
acquire_channel = False
directory = "."

# The below will be populated later
start_channel = 0
stop_channel = 0
delta_channel = 0
channel_time = 0

# Stopping clauses
stop_event = threading.Event()


# Signal handler
def signal_handler(signum, frame):
    global stop_event
    logging.info("Service interrupted")
    stop_event.set()


def assign_signal_handlers():
    """ Assign signal handlers """
    signal.signal(signal.SIGTERM, signal_handler)
    signal.signal(signal.SIGHUP, signal_handler)
    signal.signal(signal.SIGINT, signal_handler)


def stop_observation():
    global stop
    stop = True


def run_observation(duration, combine_beams=False):
    global stop

    # Create station
    station = Station()
    station.connect()

    logging.info("Setting timer to stop observation in %ds" % duration)
    timer = threading.Timer(duration, stop_observation)
    timer.start()

    base_args = ["-e", settings.instrument.lmc_interface,
                 "-i", settings.instrument.lmc_ip,
                 "-d", directory]
    channel_args = base_args + ["-a", str(settings.observation.nof_antennas),
                                "-c", str(settings.data_acquisition.nof_continuous_channels)]
    beam_args = base_args + ["-t", str(len(settings.instrument.tpm_ips)),
                             "-c", str(settings.observation.beam_channels),
                             "-f", str(start_channel),
                             "-l", str(stop_channel)]

    # If the combine beams options is set, add to beam_args argument
    if combine_beams:
        beam_args += ["-b"]

    # If in multi-channel mode, stop any data transmission before creating receiver
    # and add a reduced number of samples for acquisition
    if multi_channel_mode:
        station.stop_data_transmission()
        channel_args = channel_args + ["-s", str(109 * 6000)]

    # Start DAQ
    if acquire_channel:
        logging.info(f"Running channel acquisition with multi_channel={multi_channel_mode}")
        daq_process = subprocess.Popen(["/opt/frbs/bin/pharos_channel"] + channel_args)
    else:
        logging.info(f"Running beam acquisition with combine_beams={combine_beams}")
        daq_process = subprocess.Popen(["/opt/frbs/bin/pharos_beam"] + beam_args)

    # Wait for a while
    logging.info("Waiting for acquisition to initialise")
    sleep(5)

    # Start sending data
    if multi_channel_mode:
        for channel in range(start_channel, stop_channel + delta_channel, delta_channel):
            station.stop_data_transmission()
            station.send_channelised_data_continuous(channel)
            stop_event.wait(channel_time)

            if stop_event.is_set():
                logging.info("Service interrupted, exiting")
                break

    else:
        # Wait for observation to finish
        logging.info("Observation started")
        while not stop:
            stop_event.wait(5)

            if stop_event.is_set():
                logging.info("Service interrupted, exiting")
                stop = True

    # All done, clear everything
    logging.info("Observation ended")
    timer.cancel()

    # Stop DAQ
    daq_process.kill()


if __name__ == "__main__":

    # Command line options
    from optparse import OptionParser

    p = OptionParser()
    p.set_usage('source_observation.py [options]')
    p.set_description(__doc__)

    p.add_option("-s", "--starttime", action="store", dest="start_time",
                 default="now",
                 help="Time at which to start observation.Format: dd/mm/yyyy_hh:mm. Default: now")
    p.add_option("-l", "--duration", action="store", dest="duration",
                 type="int", default="120", help="Observation length [default: 120]")
    p.add_option("--multi-channel", action="store_true", dest="multi_channel",
                 default=False, help="Multi-channel mode (-l will be ignored) [default: False]")
    p.add_option("--combine-beams", action="store_true", dest="combine_beams",
                 default=False, help="Combine beams from multiple tile (for beamformed observations only) "
                                     "[default: False]")
    p.add_option("--channelised", action="store_true", dest="channelised", default=False,
                 help="Acquire channelised data. If False acquired beamformed data [default: False]")
    p.add_option("--directory", action="store", dest="directory", default='./',
                 help="Directory to save data in [default: '.']")
    p.add_option("--source-name", action="store", dest="source_name",
                 default="", help="Source name (default: no name)")
    p.add_option("--ra", action="store", dest="ra",
                 default="00:00:00.0", help="Right Ascension of the source [default: 00:00:00.0")
    p.add_option("--dec", action="store", dest="dec",
                 default="00:00:00.0", help="Declination of the source [default: 00:00:00.0")
    p.add_option("--configuration-file", action="store", dest="configuration_file",
                 default=None, help="Configuration file to use (overrides default)")

    opts, args = p.parse_args(sys.argv[1:])
    
    # check if configuration file is correctly passed with the file extension
    if opts.configuration_file is not None:
        opts.configuration_file += '.ini' if not opts.configuration_file.endswith('.ini') else ''
    
    # check if directory where output data are stored exists
    if not os.path.exists(opts.directory):
        logging.error("Not a valid directory")
        exit(-1)
        
    # Check if output directory is writable
    if not os.access(opts.directory, os.W_OK):
        logging.error(f"Permission error on output directory ({opts.directory})")
        exit(-1)
    
    # Load FRB configuration
    FRBConfig(config_file_name=opts.configuration_file)
    
    # Apply parameters
    directory = opts.directory
    multi_channel_mode = opts.multi_channel
    acquire_channel = opts.channelised
    start_channel = settings.data_acquisition.start_channel
    stop_channel = settings.data_acquisition.stop_channel
    delta_channel = settings.data_acquisition.delta_channel
    channel_time = settings.data_acquisition.channel_time

    # Check post-processing parameters
    post_process = False
    checks = [opts.ra != "00:00:00.0", opts.dec != "00:00:00.0", opts.source_name != ""]
    if all(checks) and not (multi_channel_mode or acquire_channel):
        logging.info("Automatically launching post-processing script after observation")
        post_process = True
    elif any(checks) and not (multi_channel_mode or acquire_channel):
        logging.error("All post-processing parameters (ra, dec, source-name) required to enable post-processing")
        exit()

    # Assign signal handlers
    assign_signal_handlers()

    # Check if directory was specified
    if opts.start_time is None:
        logging.error("Start time must be specified")
        exit(0)

    # Check that start time is valid
    start_time = 0
    curr_time = 0
    if opts.start_time != "now":

        # start time in UTC
        start_time = pytz.utc.localize(datetime.strptime(opts.start_time, "%d/%m/%Y_%H:%M"))

        # current time in UTC
        curr_time = pytz.utc.localize(datetime.utcnow())

        wait_seconds = (start_time - curr_time).total_seconds()
        if wait_seconds < 10:
            logging.error("Scheduled start time must be at least 10s in the future")
            exit()

    # Schedule observation
    if opts.start_time == "now":
        run_observation(opts.duration, opts.combine_beams)
    else:
        logging.info("Setting scheduler to run at {}".format(start_time))
        s = sched.scheduler(time.time, time.sleep)
        s.enter((start_time - curr_time).total_seconds(), 0, run_observation, [opts.duration, opts.combine_beams])

        if stop_event.is_set():
            logging.info("Service interrupted, exiting")
            exit()

        s.run()

    # Observation finished
    logging.info("Observation finished")

    # Check if we need to post-process the data
    if post_process:
        # Find filterbank files in directory
        files = glob.glob(os.path.join(os.path.abspath(os.path.expanduser(directory)), "*.fil"))

        if len(files) == 0:
            logging.warning("No filterbank files detected, cannot post-process")

        # Process first file found
        logging.info(f"Post-processing observation {files[0]}")
        pp = PostProcessor(input_filepath=files[0],
                           source_name=opts.source_name,
                           ra=opts.ra,
                           dec=opts.dec,
                           use_gpu=True)
        pp.process()

        logging.info("Finished post-processing")

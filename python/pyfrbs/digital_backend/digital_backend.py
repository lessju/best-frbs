#! /usr/bin/env python
from pyfrbs.digital_backend.tile_frb import Tile
from pyfrbs.frb_config import FRBConfig
from pyfrbs import settings
from pyfabil import Device

from multiprocessing import Pool
import numpy as np
import threading
import logging
import time
import math
import os


def create_tile_instance(tile_ip):
    """ Add a new tile to the station
    :param tile_ip: TPM IP """

    return Tile(ip=tile_ip,
                port=10000,
                lmc_ip=settings.instrument.lmc_ip,
                lmc_port=settings.instrument.lmc_port,
                sampling_rate=float(settings.instrument.sampling_rate),
                nof_signals_per_fpga=settings.instrument.signals_per_fpga)


def program_cpld(tile_ip):
    """ Update tile CPLD.
     :param tile_ip: TPM IP """

    try:
        threading.currentThread().name = tile_ip
        logging.info("Initialising Tile {}".format(tile_ip))

        # Create station instance and program CPLD
        station_tile = create_tile_instance(tile_ip)
        station_tile.program_cpld(settings.instrument.firmware)
        return True
    except Exception as e:
        logging.error("Could not program CPLD of {}: {}".format(tile_ip, e))
        return False


def program_fpgas(tile_ip):
    """ Program FPGAs
     :param tile_ip: TPM IP """

    try:
        threading.currentThread().name = tile_ip
        logging.info("Initialising Tile {}".format(tile_ip))

        # Create station instance and program FPGAs
        station_tile = create_tile_instance(tile_ip)
        station_tile.program_fpgas(settings.instrument.firmware)
        return True
    except Exception as e:
        logging.error("Could not program FPGAs of {}: {}".format(tile_ip, e))
        return False


def initialise_tile(tile_ip):
    """ Internal connect method to thread connection
     :param tile_ip: TPM IP """

    try:
        threading.currentThread().name = tile_ip
        logging.info("Initialising Tile {}".format(tile_ip))

        # Create station instance and initialise
        station_tile = create_tile_instance(tile_ip)
        station_tile.initialise(enable_test=settings.instrument.enable_pattern_generator)

        # Set ADA gain if enabled
        if settings.instrument.enable_ada:
            station_tile.tpm.tpm_ada.set_ada_gain(settings.instrument.ada_gain)

        # Set channeliser truncation
        logging.info("Setting channeliser truncation to {}".format(settings.instrument.channel_truncation_scale))
        station_tile.set_channeliser_truncation(settings.instrument.channel_truncation_scale)

        # Enable test pattern generator if required
        if settings.instrument.enable_pattern_generator:
            station_tile.tpm.tpm_pattern_generator[0].initialise()
            station_tile.tpm.tpm_pattern_generator[1].initialise()
            station_tile.tpm.tpm_pattern_generator[0].set_pattern(range(1024), "channel")
            station_tile.tpm.tpm_pattern_generator[1].set_pattern(range(1024), "channel")
            station_tile.tpm.tpm_pattern_generator[0].set_signal_adder([0] * 64, "channel")
            station_tile.tpm.tpm_pattern_generator[1].set_signal_adder([0] * 64, "channel")
            station_tile['fpga1.pattern_gen.channel_left_shift'] = 4
            station_tile['fpga2.pattern_gen.channel_left_shift'] = 4
            station_tile.tpm.tpm_pattern_generator[0].start_pattern("channel")
            station_tile.tpm.tpm_pattern_generator[1].start_pattern("channel")
            station_tile.tpm.tpm_pattern_generator[0].start_pattern("jesd")
            station_tile.tpm.tpm_pattern_generator[1].start_pattern("jesd")

            station_tile.tpm.test_generator[0].set_tone(0, 100 * 800e6 / 1024, 0.0)
            station_tile.tpm.test_generator[0].set_tone(1, 201 * 800e6 / 1024, 0.1)
            station_tile.tpm.test_generator[1].set_tone(0, 100 * 800e6 / 1024, 0.0)
            station_tile.tpm.test_generator[1].set_tone(1, 201 * 800e6 / 1024, 0.1)
            station_tile.tpm.test_generator[0].enable_prdg(0.1)
            station_tile.tpm.test_generator[1].enable_prdg(0.1)
            station_tile.tpm.test_generator[0].set_pulse_frequency(0, 0.0)
            station_tile.tpm.test_generator[1].set_pulse_frequency(0, 0.0)
            station_tile.tpm.test_generator[0].channel_select(0xFFFF)
            station_tile.tpm.test_generator[1].channel_select(0xFFFF)

        station_tile['fpga1.dsp_regfile.debug_module_ctrl'] = 0
        station_tile['fpga2.dsp_regfile.debug_module_ctrl'] = 0

        disable_mask = 0xFFFF
        for n in range(settings.instrument.signals_per_fpga):
            disable_mask <<= 1
        disable_mask &= 0xFFFF
        station_tile['fpga1.jesd204_if.regfile_channel_disable'] = disable_mask
        station_tile['fpga2.jesd204_if.regfile_channel_disable'] = disable_mask

        # Set beam truncation
        for fpga in range(1, 3):
            for i in range(4):
                station_tile['fpga{}.beamf{}.bit_round'.format(fpga, i)] = 5

        return True
    except Exception as e:
        logging.warning("Could not initialise Tile {}: {}".format(tile_ip, e))
        return False


class Station(object):
    """ Class representing an AAVS station """

    def __init__(self):
        """ Class constructor """

        # Save configuration locally
        self._station_id = 0

        # Add tiles to station
        self.tiles = []
        for tile in settings.instrument.tpm_ips:
            self.add_tile(tile)

        # Default duration of sleeps
        self._seconds = 1.0

        # Set if the station is properly configured
        self.properly_formed_station = None

    def connect(self, update_cpld=False, program=False, initialise=False, send_beam=False):
        """ Initialise all tiles """

        # Start with the assumption that the station will be properly formed
        self.properly_formed_station = True

        # Create a pool of nof_tiles processes
        pool = None
        if any([update_cpld, program, initialise]):
            pool = Pool(len(self.tiles))

        # Check if we are programming the CPLD, and if so program
        if update_cpld:
            logging.info("Programming CPLD")
            res = pool.map(program_cpld, settings.instrument.tpm_ips)

            if not all(res):
                logging.error("Could not program TPM CPLD!")
                self.properly_formed_station = False

        # Check if programming is required, and if so program
        if program and self.properly_formed_station:
            logging.info("Programming tiles")
            res = pool.map(program_fpgas, settings.instrument.tpm_ips)

            if not all(res):
                logging.error("Could not program tiles!")
                self.properly_formed_station = False

        # Check if initialisation is required, and if so initialise
        if initialise and self.properly_formed_station:
            logging.info("Initialising tiles")
            res = pool.map(initialise_tile, settings.instrument.tpm_ips)

            if not all(res):
                logging.error("Could not initialise tiles!")
                self.properly_formed_station = False

        # Ready from pool
        if pool is not None:
            pool.terminate()

        # Connect all tiles
        for tile in self.tiles:
            tile.connect()
            if not tile.tpm.is_programmed():
                self.properly_formed_station = False

        # Initialise if required
        if initialise and self.properly_formed_station:
            logging.info("Forming station")
            self._form_station()

            # If initialising, synchronise all tiles in station
            logging.info("Synchronising station")
            self._station_post_synchronisation()
            self._synchronise_tiles(send_beam)

        elif not self.properly_formed_station:
            logging.warning("Some tiles were not initialised or programmed. Not forming station")

        # If not initialising, check that station is formed properly
        else:
            self.check_station_status()

    def reset(self):
        """ Reset station """
        logging.info("Resetting station")
        for tile in self.tiles:
            tile.reset_board()

    def add_tile(self, tile_ip):
        """ Add a new tile to the station
        :param tile_ip: Tile IP to be added to station """
        self.tiles.append(Tile(ip=tile_ip,
                               port=10000,
                               lmc_ip=settings.instrument.lmc_ip,
                               lmc_port=settings.instrument.lmc_port,
                               sampling_rate=float(settings.instrument.sampling_rate),
                               nof_signals_per_fpga=settings.instrument.signals_per_fpga))

    def check_station_status(self):
        """ Check that the station is still valid """
        tile_ids = []
        for tile in self.tiles:
            if tile.tpm is None:
                self.properly_formed_station = False
                break

            tile_id = tile.get_tile_id()
            if tile.get_tile_id() < len(self.tiles) and tile_id not in tile_ids:
                tile_ids.append(tile_id)
            else:
                self.properly_formed_station = False
                break

        if not self.properly_formed_station:
            logging.warning("Station configuration is incorrect (unreachable TPMs or incorrect tile ids)!")

    def _form_station(self):
        """ Forms the station """

        # Assign station and tile id, and tweak transceivers
        for i, tile in enumerate(self.tiles):
            tile.set_station_id(self._station_id, i)
            tile.tweak_transceivers()

    def _synchronise_tiles(self, send_beam):
        """ Synchronise time on all tiles """

        pps_detect = self['fpga1.pps_manager.pps_detected']
        logging.debug("FPGA1 PPS detection register is ({})".format(pps_detect))
        pps_detect = self['fpga2.pps_manager.pps_detected']
        logging.debug("FPGA2 PPS detection register is ({})".format(pps_detect))

        # Repeat operation until Tiles are synchronised
        while True:
            # Read the current time on first tile
            self.tiles[0].wait_pps_event()

            # PPS edge detected, write time to all tiles
            curr_time = self.tiles[0].get_fpga_time(Device.FPGA_1)
            logging.info("Synchronising tiles in station with time %d" % curr_time)

            for tile in self.tiles:
                tile.set_fpga_time(Device.FPGA_1, curr_time)
                tile.set_fpga_time(Device.FPGA_2, curr_time)

            # All done, check that PPS on all boards are the same
            self.tiles[0].wait_pps_event()

            times = set()
            for tile in self.tiles:
                times.add(tile.get_fpga_time(Device.FPGA_1))
                times.add(tile.get_fpga_time(Device.FPGA_2))

            if len(times) == 1:
                break

        # Tiles synchronised
        curr_time = self.tiles[0].get_fpga_time(Device.FPGA_1)
        logging.info("Tiles in station synchronised, time is %d" % curr_time)

        # Set LMC data lanes
        for i, tile in enumerate(self.tiles):
            # Configure integrated data streams
            tile.set_beam_download()

            logging.info("Using 40G for LMC traffic")
            tile.set_lmc_download("10g",
                                  payload_length=8192)

            # Configure integrated data streams
            logging.info("Using 40G for integrated LMC traffic")
            tile.set_lmc_integrated_download("10g",
                                             channel_payload_length=2048,
                                             beam_payload_length=2048)

        # Start beamformer
        if send_beam:
            logging.info("Starting beamformer")
            for tile in self.tiles:
                tile.initialise_beamformer()

        for tile in self.tiles:
            tile.check_arp_table()

        # Start data acquisition on all boards
        logging.info("Setting data acquisition")
        delay = 1
        tile.wait_pps_event()
        t0 = self.tiles[0].get_fpga_time(Device.FPGA_1)
        for tile in self.tiles:
            tile.start_acquisition(start_time=t0,
                                   delay=delay,
                                   integration_time=float(settings.observation.beam_integration_time),
                                   first_channel=settings.observation.first_channel,
                                   nof_channels=settings.observation.beam_channels,
                                   enable_beam_tx=send_beam)

        t1 = self.tiles[-1].get_fpga_time(Device.FPGA_1)
        t2 = self.tiles[-1].get_fpga_time(Device.FPGA_2)
        if t0 == t1 == t2:
            logging.info("Waiting for start acquisition")
            while self.tiles[0]['fpga1.dsp_regfile.stream_status.channelizer_vld'] == 0:
                time.sleep(0.1)
        else:
            logging.error("Start data acquisition not synchronised! Rerun initialisation")
            exit()

    def _station_post_synchronisation(self):
        """ Post tile configuration synchronization """

        pps_delays = [0] * len(self.tiles)

        for tile in self.tiles:
            tile['fpga1.pps_manager.sync_cnt_enable'] = 0x7
            tile['fpga2.pps_manager.sync_cnt_enable'] = 0x7
        time.sleep(0.2)
        for tile in self.tiles:
            tile['fpga1.pps_manager.sync_cnt_enable'] = 0x0
            tile['fpga2.pps_manager.sync_cnt_enable'] = 0x0

        # Station synchronisation loop
        sync_loop = 0
        max_sync_loop = 5
        while sync_loop < max_sync_loop:
            self.tiles[0].wait_pps_event()

            current_tc = [tile.get_phase_terminal_count() for tile in self.tiles]
            delay = [tile.get_pps_delay() for tile in self.tiles]
            from operator import add
            delay = list(map(add, delay, pps_delays))

            for n in range(len(self.tiles)):
                self.tiles[n].set_phase_terminal_count(self.tiles[n].calculate_delay(delay[n], current_tc[n], 16, 24))

            self.tiles[0].wait_pps_event()

            current_tc = [tile.get_phase_terminal_count() for tile in self.tiles]
            delay = [tile.get_pps_delay() for tile in self.tiles]

            for n in range(len(self.tiles)):
                self.tiles[n].set_phase_terminal_count(self.tiles[n].calculate_delay(delay[n], current_tc[n],
                                                                                     delay[0] - 4, delay[0] + 4))

            self.tiles[0].wait_pps_event()

            delay = [tile.get_pps_delay() for tile in self.tiles]
            delay = list(map(add, delay, pps_delays))

            synced = 1
            for n in range(len(self.tiles) - 1):
                if abs(delay[0] - delay[n + 1]) > 4:
                    logging.warning("Resynchronizing station ({})".format(delay))
                    sync_loop += 1
                    synced = 0

            if synced == 1:
                phase1 = [hex(tile['fpga1.pps_manager.sync_phase']) for tile in self.tiles]
                phase2 = [hex(tile['fpga2.pps_manager.sync_phase']) for tile in self.tiles]
                logging.debug("Final FPGA1 clock phase ({})".format(phase1))
                logging.debug("Final FPGA2 clock phase ({})".format(phase2))

                logging.info("Finished station post synchronisation ({})".format(delay))
                return delay

        logging.error("Station post synchronisation failed!")

    # ------------------------------------------------------------------------------------------------
    def test_generator_set_tone(self, dds, frequency=100e6, ampl=0.0, phase=0.0, delay=512):
        t0 = self.tiles[0]["fpga1.pps_manager.timestamp_read_val"] + delay
        for tile in self.tiles:
            for gen in tile.tpm.test_generator:
                gen.set_tone(dds, frequency, ampl, phase, t0)
        t1 = self.tiles[0]["fpga1.pps_manager.timestamp_read_val"]
        if t1 > t0:
            logging.info("Set tone test pattern generators synchronisation failed.")

    def test_generator_disable_tone(self, dds, delay=512):
        t0 = self.tiles[0]["fpga1.pps_manager.timestamp_read_val"] + delay
        for tile in self.tiles:
            for gen in tile.tpm.test_generator:
                gen.set_tone(dds, 0, 0, 0, t0)
        t1 = self.tiles[0]["fpga1.pps_manager.timestamp_read_val"]
        if t1 > t0:
            logging.info("Set tone test pattern generators synchronisation failed.")

    def test_generator_set_noise(self, ampl=0.0, delay=512):
        t0 = self.tiles[0]["fpga1.pps_manager.timestamp_read_val"] + delay
        for tile in self.tiles:
            for gen in tile.tpm.test_generator:
                gen.enable_prdg(ampl, t0)
        t1 = self.tiles[0]["fpga1.pps_manager.timestamp_read_val"]
        if t1 > t0:
            logging.info("Set tone test pattern generators synchronisation failed.")

    def test_generator_input_select(self, inputs):
        for tile in self.tiles:
            tile.test_generator[0].channel_select(inputs & 0xFFFF)
            tile.test_generator[1].channel_select((inputs >> 16) & 0xFFFF)

    def enable_beamformer_test_pattern(self, nof_tiles=16):
        """ Enable beamformer test pattern """
        for tile in self.tiles:
            log2_tiles = int(math.log(nof_tiles, 2))
            tile.tpm.tpm_pattern_generator[0].start_pattern("beamf")
            tile.tpm.tpm_pattern_generator[1].start_pattern("beamf")
            tile['fpga1.pattern_gen.beamf_left_shift'] = 4 - log2_tiles
            tile['fpga2.pattern_gen.beamf_left_shift'] = 4 - log2_tiles
            tile['fpga1.beamf_ring.csp_scaling'] = 4
            tile['fpga2.beamf_ring.csp_scaling'] = 4
            tile.tpm.tpm_pattern_generator[0].set_signal_adder([0] * 64, "beamf")
            tile.tpm.tpm_pattern_generator[1].set_signal_adder([1] * 64, "beamf")

    def disable_beamformer_test_pattern(self):
        """ Enable beamformer test pattern """
        for tile in self.tiles:
            tile.tpm.tpm_pattern_generator[0].stop_pattern("beamf")
            tile.tpm.tpm_pattern_generator[1].stop_pattern("beamf")

    def enable_channeliser_test_pattern(self):
        """ Enable beamformer test pattern """
        for tile in self.tiles:
            tile.tpm.tpm_pattern_generator[0].start_pattern("channel")
            tile.tpm.tpm_pattern_generator[1].start_pattern("channel")

    def stop_channeliser_test_pattern(self):
        """ Enable beamformer test pattern """
        for tile in self.tiles:
            tile.tpm.tpm_pattern_generator[0].stop_pattern("channel")
            tile.tpm.tpm_pattern_generator[1].stop_pattern("channel")

    # ------------------------------------------------------------------------------------------------

    def apply_coefficients(self, filepath, calibration_beam, calibration_tile):
        """ Apply coefficients file """
        if not os.path.exists(filepath):
            logging.error("Calibration coefficients file {} does not exist".format(filepath))

        # Determine number of tiles being used
        nof_tiles = 1
        if calibration_tile == -1:
            nof_tiles = len(self.tiles)

        # Array to hold coefficients
        coeffs = np.ones((settings.observation.nof_channels,
                          settings.instrument.signals_per_fpga * 2 * nof_tiles), dtype=np.complex64)

        try:
            # Load coefficients from file
            with open(filepath, 'r') as f:
                data = f.read()

                # Split into multiple channels
                for entry in data.split('#'):
                    channel = entry.strip()
                    if channel == "":
                        continue

                    # Split into lines
                    lines = channel.split("\n")

                    # First line will contain channel number
                    channel_number = [int(s) for s in lines[0].split() if s.isdigit()][0]

                    # Parse the rest as complex numbers
                    values = [complex(x) for x in lines[1:] if x != '']

                    # Check if number of antennas is correct (only required in first iteration)
                    if len(values) != coeffs.shape[1]:
                        logging.error(f"Number of coefficients in file ({len(values)}) does not match "
                                      f"number of antennas for {nof_tiles} tiles")
                        return
                    else:
                        # Place coefficients array
                        coeffs[channel_number, :] = np.array(values)

            # We have the coefficients, normalise per channel and apply to firmware
            for i in range(coeffs.shape[1]):
                # Extract real and imaginary parts
                real = coeffs[:, i].real
                imag = coeffs[:, i].imag

                # Find maximum absolute real and imaginary components to normalise with
                max_val = max(np.max(np.abs(real)), np.max(np.abs(imag)))
                real = (real / max_val) * 127
                imag = (imag / max_val) * 127

                real = np.round(real).astype(np.int32)
                imag = np.round(imag).astype(np.int32)

                # Generate combined coefficient
                values = ((imag & 0xFF) << 8) | (real & 0xFF)

                # Determine to which tile the antenna belongs to
                if nof_tiles == 1:
                    self.tiles[calibration_tile].set_beamformer_coefficients(values, calibration_beam, i)
                else:
                    antenna_in_tile = i % (settings.instrument.signals_per_fpga * 2)
                    tile = self.tiles[i // (settings.instrument.signals_per_fpga * 2)]
                    tile.set_beamformer_coefficients(values, calibration_beam, antenna_in_tile)

            # Apply coefficients on TPMs
            for tile in self.tiles:
                tile.synchronise_beamformer_coefficients(calibration_beam)

            if calibration_tile == -1:
                logging.info("Downloaded calibration coefficients to beam {} for entire station".format(calibration_beam))
            else:
                logging.info("Downloaded calibration coefficients to beam {}, tile {}".format(calibration_beam, calibration_tile))

        except Exception as e:
            raise Exception("Exception occurred when trying to apply coefficients: {}".format(e))

    # ------------------------------------------------------------------------------------------------

    def mii_test(self, pkt_num, show_result=True):
        """ Perform mii test """

        for i, tile in enumerate(self.tiles):
            logging.debug("MII test setting Tile " + str(i))
            tile.mii_prepare_test(i + 1)

        for i, tile in enumerate(self.tiles):
            logging.debug("MII test starting Tile " + str(i))
            tile.mii_exec_test(pkt_num, wait_result=False)

        if not show_result:
            return

        while True:
            for i, tile in enumerate(self.tiles):
                logging.debug("Tile " + str(i) + " MII test result:")
                tile.mii_show_result()
                k = input("Enter quit to exit. Any other key to continue.")
                if k == "quit":
                    return

    # ------------------------------------ DATA OPERATIONS -------------------------------------------

    def send_raw_data(self, sync=False, period=0, timeout=0):
        """ Send raw data from all Tiles """
        self._wait_available()
        t0 = self.tiles[0].get_fpga_timestamp(Device.FPGA_1)
        for tile in self.tiles:
            tile.send_raw_data(sync=sync, period=period, timeout=timeout, timestamp=t0, seconds=self._seconds)
        return self._check_data_sync(t0)

    def send_raw_data_synchronised(self, period=0, timeout=0):
        """ Send synchronised raw data from all Tiles """
        self._wait_available()
        t0 = self.tiles[0].get_fpga_timestamp(Device.FPGA_1)
        for tile in self.tiles:
            tile.send_raw_data_synchronised(period=period, timeout=timeout, timestamp=t0, seconds=self._seconds)
        return self._check_data_sync(t0)

    def send_channelised_data(self, number_of_samples=1024, first_channel=0, last_channel=511, period=0, timeout=0):
        """ Send channelised data from all Tiles """
        self._wait_available()
        t0 = self.tiles[0].get_fpga_timestamp(Device.FPGA_1)
        for tile in self.tiles:
            tile.send_channelised_data(number_of_samples=number_of_samples, first_channel=first_channel,
                                       last_channel=last_channel, period=period,
                                       timeout=timeout, timestamp=t0, seconds=self._seconds)
        return self._check_data_sync(t0)

    def send_beam_data(self, period=0, timeout=0):
        """ Send beam data from all Tiles """
        self._wait_available()
        t0 = self.tiles[0].get_fpga_timestamp(Device.FPGA_1)
        for tile in self.tiles:
            tile.send_beam_data(period=period, timeout=timeout, timestamp=t0, seconds=self._seconds)
        return self._check_data_sync(t0)

    def send_channelised_data_continuous(self, channel_id, timeout=0):
        """ Send continuous channelised data from all Tiles """
        self.stop_data_transmission()
        self._wait_available()
        t0 = self.tiles[0].get_fpga_timestamp(Device.FPGA_1)
        for tile in self.tiles:
            tile.send_channelised_data_continuous(channel_id=channel_id, timeout=timeout,
                                                  timestamp=t0, seconds=self._seconds)
        return self._check_data_sync(t0)

    def stop_data_transmission(self):
        """ Stop data transmission """
        logging.info("Stopping data transmission")
        for tile in self.tiles:
            tile.stop_data_transmission()

    def stop_integrated_data(self):
        """ Stop integrated data transmission """
        for tile in self.tiles:
            tile.stop_integrated_data()

    def get_adc_rms(self):
        """ Get ADC RMS from all TPMs """
        rms = []
        for tile in self.tiles:
            rms.extend(tile.get_adc_rms())
        return rms

    def _wait_available(self):
        """ Make sure all boards can send data """
        while any([tile.check_pending_data_requests() for tile in self.tiles]):
            logging.info("Waiting for pending data requests to finish")
            time.sleep(0.1)

    def _check_data_sync(self, t0):
        """ Check whether data synchronisation worked """
        delay = self._seconds * (1 / (1080 * 1e-9) / 256)
        timestamps = [tile.get_fpga_timestamp(Device.FPGA_1) for tile in self.tiles]
        logging.debug("Data sync check: timestamp={}, delay={}".format(str(timestamps), delay))
        return all([(t0 + delay) > t1 for t1 in timestamps])

    def test_wr_exec(self):
        import time
        start = time.time()
        ba = self.tiles[0].tpm.register_list['%s.pattern_gen.%s_data' % ('fpga1', "beamf")]['address']

        for n in range(1024):
            self.tiles[0][ba] = list(range(256))

        end = time.time()
        logging.debug("test_wr_exec: {}".format((end - start)))

    # ------------------------------------------- TEST FUNCTIONS ---------------------------------------

    def test_rd_exec(self):
        import time
        start = time.time()
        ba = self.tiles[0].tpm.register_list['%s.pattern_gen.%s_data' % ('fpga1', "beamf")]['address']

        for n in range(1024):
            self.tiles[0].tpm.read_register(ba, n=256)

        end = time.time()
        logging.debug("test_rd_exec: {}".format((end - start)))

    def err_reset(self):
        self['fpga1.dsp_regfile.error_clear'] = 0xFFFFFFFF
        self['fpga1.dsp_regfile.error_clear'] = 0x0
        self['fpga2.dsp_regfile.error_clear'] = 0xFFFFFFFF
        self['fpga2.dsp_regfile.error_clear'] = 0x0
        self['fpga1.beamf_ring.control.error_rst'] = 1
        self['fpga1.beamf_ring.control.error_rst'] = 0
        self['fpga2.beamf_ring.control.error_rst'] = 1
        self['fpga2.beamf_ring.control.error_rst'] = 0
        self['fpga1.regfile.eth10g_error'] = 0
        self['fpga2.regfile.eth10g_error'] = 0

    def error_check(self):
        logging.debug(self['fpga1.dsp_regfile.error_detected'])
        logging.debug(self['fpga2.dsp_regfile.error_detected'])
        logging.debug(self['fpga1.beamf_ring.error'])
        logging.debug(self['fpga2.beamf_ring.error'])
        logging.debug(self['fpga1.regfile.eth10g_error'])
        logging.debug(self['fpga2.regfile.eth10g_error'])

    def ddr3_check(self):
        try:
            while True:
                for n in range(len(self.tiles)):
                    if (self.tiles[n]['fpga1.ddr3_if.status'] & 0x100) != 256:
                        logging.info("Tile" + str(n) + " FPGA1 DDR Error Detected!")
                        logging.info(hex(self.tiles[n]['fpga1.ddr3_if.status']))
                        logging.info(time.asctime(time.localtime(time.time())))
                        time.sleep(5)

                    if (self.tiles[n]['fpga2.ddr3_if.status'] & 0x100) != 256:
                        logging.info("Tile" + str(n) + " FPGA2 DDR Error Detected!")
                        logging.info(hex(self.tiles[n]['fpga2.ddr3_if.status']))
                        logging.info(localtime=time.asctime(time.localtime(time.time())))
                        time.sleep(5)
        except KeyboardInterrupt:
            pass

    @staticmethod
    def check_adc_sysref():
        for adc in range(16):
            error = 0
            values = station['adc' + str(adc), 0x128]
            for i in range(len(values)):
                msb = (values[i] & 0xF0) >> 4
                lsb = (values[i] & 0x0F) >> 0
                if msb == 0 and lsb <= 7:
                    logging.warning('Possible setup error in tile %d adc %d' % (i, adc))
                    error = 1
                if msb >= 9 and lsb == 0:
                    logging.warning('Possible hold error in tile %d adc %d' % (i, adc))
                    error = 1
                if msb == 0 and lsb == 0:
                    logging.warning('Possible setup and hold error in tile %d adc %d' % (i, adc))
                    error = 1
            if error == 0:
                logging.debug('ADC %d sysref OK!' % adc)

    # ------------------------------------------- OVERLOADED FUNCTIONS ---------------------------------------

    def __getitem__(self, key):
        """ Read register across all tiles """
        return [tile.tpm[key] for tile in self.tiles]

    def __setitem__(self, key, value):
        """ Write register across all tiles """
        for tile in self.tiles:
            tile.tpm[key] = value


if __name__ == "__main__":
    from optparse import OptionParser
    from sys import argv

    parser = OptionParser(usage="usage: %station [options]")
    parser.add_option("-f", "--bitfile", action="store", dest="bitfile",
                      default=None, help="Bitfile to use (-P still required) [default: None]")
    parser.add_option("-P", "--program", action="store_true", dest="program",
                      default=False, help="Program FPGAs [default: False]")
    parser.add_option("-I", "--initialise", action="store_true", dest="initialise",
                      default=False, help="Initialise TPM [default: False]")
    parser.add_option("-C", "--program_cpld", action="store_true", dest="program_cpld",
                      default=False, help="Update CPLD firmware (requires -f option) [default: False]")
    parser.add_option("-B", "--send_beam", action="store_true", dest="send_beam",
                      default=False, help="Enable beam transmission")
    parser.add_option("-R", "--reset", action="store_true", dest="reset",
                      default=False, help="Reset TPMs in station")
    parser.add_option("--configuration-file", action="store", dest="configuration_file",
                      default=None, help="Configuration file to use (overrides default)")
    parser.add_option("--calibration-file", action="store", dest="coefficients_file",
                      default=None, help="Calibration coefficient file to apply [default: None]")
    parser.add_option("--calibration-beam", action="store", dest="calibration_beam",
                      default=0, type="int", help="Beam to apply calibration coefficients to [default: 0]")
    parser.add_option("--calibration-tile", action="store", dest="calibration_tile",
                      default=-1, type="int", help="Determine which tile this calibration is for [default: -1, all tiles]")

    (conf, args) = parser.parse_args(argv[1:])

    # Load default configuration
    if conf.configuration_file is not None:
        conf.configuration_file += '.ini' if not conf.configuration_file.endswith('.ini') else ''
    FRBConfig(config_file_name=conf.configuration_file)

    # Overwrite required settings
    if conf.bitfile is not None:
        settings.instrument.firmware = conf.bitfile

    # Set current thread name
    threading.currentThread().name = "Digital Backend"

    # Create station
    station = Station()

    # Connect station (program, initialise and configure if required)
    station.connect(update_cpld=conf.program_cpld,
                    program=conf.program,
                    initialise=conf.initialise,
                    send_beam=conf.send_beam)

    # Reset TPMs if required
    if conf.reset:
        station.reset()
        exit()

    # Apply coefficients if required
    if conf.coefficients_file is not None:
        station.apply_coefficients(conf.coefficients_file, conf.calibration_beam, conf.calibration_tile)

# -*- coding: utf-8 -*-
"""
Created on March, 7th 18:11:43 2022

@author: Giovanni Naldi
"""


from datetime import datetime
import time
import os
import logging
import pytz
import re


from pyfrbs.digital_backend.digital_backend import Station
from pyfrbs.frb_config import FRBConfig
from pyfrbs import settings


if __name__ == "__main__":
    from optparse import OptionParser
    from sys import argv
    
    parser = OptionParser()
    
    parser.set_usage('beam_tracking_scheduler.py [options]')
    parser.set_description(__doc__)
    
    parser.add_option("--calib-name", action="store", dest="calibratorname",
                      default="CasA", help="Source used as calibrator. You can choose among: CasA, TauA, VirA, CygA, HerA. [Default: CasA]")
    parser.add_option("--directory", action="store", dest="directory", 
                      default="None", help="Directory to save data in [default: None]")
    parser.add_option("--configuration-file", action="store", dest="configuration_file",
                      default=None, help="Configuration file to use (overrides default)")
    
    (conf, args) = parser.parse_args(argv[1:])
    
    # Load FRB configuration
    FRBConfig(config_file_name=conf.configuration_file)
    
    # Connect to the digital backend
    station = Station()
    station.connect()
    
    # Input data copied to local constants
    calib_name = conf.calibratorname
    directory = conf.directory
    
    # Check for integrity of input data
    if "cas" in calib_name.lower():
        calibrator = "Cas-A"
    elif "tau" in calib_name.lower():
        calibrator = "Tau-A"
    elif "vir" in calib_name.lower():
        calibrator = "Vir-A"
    elif "cyg" in calib_name.lower():
        calibrator = "Cyg-A"
    elif "her" in calib_name.lower():
        calibrator = "Her-A"
    else:
        logging.error("Calibrator Name is not among options. Possible calibrators are CasA, TauA, VirA, CygA, HerA")
        exit(1)
    
    if conf.directory is None:
        logging.error("Please, specify the directory where to store data")
        exit(2)
        
    if not os.path.isdir(conf.directory):
        logging.error("Not a valid directory")
        exit(3)
    
    fname_keyword = "_TimeIntervals"
    time_intervals_file = False
    for fname in os.listdir(directory):
        if fname_keyword in fname:
            file_time_intervals_full = directory + fname
            file_time_intervals_splitted = fname.split("_")
            target = file_time_intervals_splitted[1]
            time_intervals_file = True
    
    if not time_intervals_file:
        logging.error("There is no time intervals file for beam tracking in the directory")
        exit(4)
    
    
    
    with open(file_time_intervals_full,"rt") as input_file:
        text = input_file.read()
        matches = re.findall(r'\d{4}/\d{2}/\d{2}\s\d{2}:\d{2}:\d{2}.\d{4}', text)
        datetime_matches = [datetime.strptime(date, "%Y/%m/%d %H:%M:%S.%f") for date in matches]
    
    
    # current time in UTC
    curr_time = pytz.utc.localize(datetime.utcnow())
    
    # start time in UTC
    start_time = pytz.utc.localize(datetime_matches[0])
    
    wait_seconds = (start_time - curr_time).total_seconds()
    if wait_seconds < 300:
        logging.error("Scheduled start time must be at least 5 minutes beforehand")
        exit(5)
    
    
    
    for i in range(len(datetime_matches)):
        if (i % 2) == 0:
            if os.path.isfile(f"{directory}Beam_{str(i//2)}_Total_Coeff_from_{calibrator}_To_{target}.txt"):
                print('Loading Coefficients For Beam ' + str(i//2) + ' Scheduled At ' + datetime.strftime(pytz.utc.localize(datetime_matches[i]),"%Y-%m-%d_%H:%M:%S.%f"))
            else:
                logging.error('Coefficient File For Beam ' + str(i//2) + ' Does Not Exist')
                exit(6)
    
    print('-----------------------------------------------------------')
    
    
    for i in range(len(datetime_matches)):
        if (i % 2) == 0:
            while pytz.utc.localize(datetime.utcnow()) < pytz.utc.localize(datetime_matches[i]):
                time.sleep(0.1)
            station.apply_coefficients(f"{directory}Beam_{str(i//2)}_Total_Coeff_from_{calibrator}_To_{target}.txt", 0, -1)
    
    
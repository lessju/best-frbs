__author__ = 'Alessio Magro'

import logging
import time

from pyfabil.plugins.firmwareblock import FirmwareBlock
from pyfabil.base.definitions import *
from time import sleep
from math import log, ceil


class TpmFrbFirmware(FirmwareBlock):
    """ FirmwareBlock tests class """

    @firmware({'design': 'tpm_pharos2', 'major': '1', 'minor': '>1'})
    @compatibleboards(BoardMake.TpmBoard)
    @friendlyname('tpm_frb_firmware')
    @maxinstances(2)
    def __init__(self, board, **kwargs):
        """ TpmFrbFirmware initializer
        :param board: Pointer to board instance
        """
        super(TpmFrbFirmware, self).__init__(board)

        # Device must be specified in kwargs
        if kwargs.get('device', None) is None:
            raise PluginError("TpmFrbFirmware requires device argument")
        self._device = kwargs['device']

        if kwargs.get('fsample', None) is None:
            logging.info("TpmFrbFirmware: Setting default sampling frequency 800 MHz.")
            self._fsample = 800e6
        else:
            self._fsample = float(kwargs['fsample'])

        try:
            if self.board['fpga1.regfile.feature.xg_eth_implemented'] == 1:
                self.xg_eth = True
            else:
                self.xg_eth = False
            if self.board['fpga1.regfile.feature.xg_eth_40g_implemented'] == 1:
                self.xg_40g_eth = True
            else:
                self.xg_40g_eth = False
        except:
            self.xg_eth = False
            self.xg_40g_eth = False
            
        self._nof_integrators = self.board['fpga1.dsp_regfile.feature.beam_integrators']

        # Load required plugins
        self._jesd1 = self.board.load_plugin("TpmJesd", device=self._device, core=0)
        self._jesd2 = self.board.load_plugin("TpmJesd", device=self._device, core=1)
        self._fpga = self.board.load_plugin('TpmFpga', device=self._device)

        self._fortyg = None
        self._teng = None

        if self._device == Device.FPGA_1:

            if self.xg_eth and not self.xg_40g_eth:
                self._teng = [self.board.load_plugin("TpmTenGCoreXg", device=self._device, core=0),
                              self.board.load_plugin("TpmTenGCoreXg", device=self._device, core=1),
                              self.board.load_plugin("TpmTenGCoreXg", device=self._device, core=2),
                              self.board.load_plugin("TpmTenGCoreXg", device=self._device, core=3)]
            elif self.xg_eth and self.xg_40g_eth:
                self._fortyg = self.board.load_plugin("TpmFortyGCoreXg", device=self._device, core=0)
            else:
                self._teng = [self.board.load_plugin("TpmTenGCore", device=self._device, core=0),
                              self.board.load_plugin("TpmTenGCore", device=self._device, core=1),
                              self.board.load_plugin("TpmTenGCore", device=self._device, core=2),
                              self.board.load_plugin("TpmTenGCore", device=self._device, core=3)]

        self._f2f = [self.board.load_plugin("TpmFpga2Fpga", core=0),
                     self.board.load_plugin("TpmFpga2Fpga", core=1)]
        self._beamf = [self.board.load_plugin("BeamfSimple", device=self._device, core=0),
                       self.board.load_plugin("BeamfSimple", device=self._device, core=1),
                       self.board.load_plugin("BeamfSimple", device=self._device, core=2),
                       self.board.load_plugin("BeamfSimple", device=self._device, core=3)]
        self._testgen = self.board.load_plugin("TpmTestGenerator", device=self._device, fsample=self._fsample)
        self._sysmon = self.board.load_plugin("TpmSysmon", device=self._device)
        self._patterngen = self.board.load_plugin("TpmPatternGenerator", device=self._device, fsample=self._fsample)
        self._power_meter = self.board.load_plugin("AdcPowerMeter", device=self._device, fsample=self._fsample)
        self._integrator = [self.board.load_plugin("TpmIntegrator", device=self._device, fsample=self._fsample, core=i) for i in range(self._nof_integrators)]


        self._device_name = "fpga1" if self._device is Device.FPGA_1 else "fpga2"

    def fpga_clk_sync(self):
        """ FPGA synchronise clock"""

        if self._device_name == 'fpga1':

            fpga0_phase = self.board['fpga1.pps_manager.sync_status.cnt_hf_pps']

            # restore previous counters status using PPS phase
            self.board['fpga1.pps_manager.sync_tc.cnt_1_pulse'] = 0
            time.sleep(1.1)
            for n in range(5):
                fpga0_cnt_hf_pps = self.board['fpga1.pps_manager.sync_phase.cnt_hf_pps']
                if abs(fpga0_cnt_hf_pps - fpga0_phase) <= 3:
                    logging.debug("FPGA1 clock synced to PPS phase!")
                    break
                else:
                    rd = self.board['fpga1.pps_manager.sync_tc.cnt_1_pulse']
                    self.board['fpga1.pps_manager.sync_tc.cnt_1_pulse'] = rd + 1
                    time.sleep(1.1)

        if self._device_name == 'fpga2':

            # Synchronize FPGA2 to FPGA1 using sysref phase
            fpga0_phase = self.board['fpga1.pps_manager.sync_phase.cnt_1_sysref']

            self.board['fpga2.pps_manager.sync_tc.cnt_1_pulse'] = 0x0
            sleep(0.1)
            for n in range(5):
                fpga1_phase = self.board['fpga2.pps_manager.sync_phase.cnt_1_sysref']
                if fpga0_phase == fpga1_phase:
                    logging.debug("FPGA2 clock synced to SYSREF phase!")
                    break
                else:
                    rd = self.board['fpga2.pps_manager.sync_tc.cnt_1_pulse']
                    self.board['fpga2.pps_manager.sync_tc.cnt_1_pulse'] = rd + 1
                    sleep(0.1)

            logging.debug("FPGA1 clock phase before adc_clk alignment: " + hex(self.board['fpga1.pps_manager.sync_phase']))
            logging.debug("FPGA2 clock phase before adc_clk alignment: " + hex(self.board['fpga2.pps_manager.sync_phase']))

    def start_ddr_initialisation(self):
        """ Start DDR initialisation """
        if self.board['board.regfile.ctrl.en_ddr_vdd'] == 0:
            self.board['board.regfile.ctrl.en_ddr_vdd'] = 1
            time.sleep(0.5)
        logging.debug("%s DDR3 reset" % self._device_name)
        self.board["%s.regfile.reset.ddr_rst" % self._device_name] = 0x1
        self.board["%s.regfile.reset.ddr_rst" % self._device_name] = 0x0

    def check_ddr_initialisation(self):
        """ Check whether DDR has initialised """
        if self.board.memory_map.has_register("%s.regfile.stream_status.ddr_init_done" % self._device_name):
            status = self.board["%s.regfile.stream_status.ddr_init_done" % self._device_name]
        else:
            status = self.board["%s.regfile.status.ddr_init_done" % self._device_name]

        if status == 0x0:
            logging.debug("DDR3 %s is not initialised" % self._device_name)
            self.initialise_ddr()
        else:
            logging.debug("DDR3 %s initialised!" % self._device_name)
            return

    def initialise_ddr(self):
        """ Initialise DDR """

        if self.board['board.regfile.ctrl.en_ddr_vdd'] == 0:
            self.board['board.regfile.ctrl.en_ddr_vdd'] = 1
            time.sleep(0.5)

        for n in range(3):
            logging.debug("%s DDR3 reset" % self._device_name)
            self.board["%s.regfile.reset.ddr_rst" % self._device_name] = 0x1
            self.board["%s.regfile.reset.ddr_rst" % self._device_name] = 0x0

            for m in range(5):
                if self.board.memory_map.has_register("%s.regfile.stream_status.ddr_init_done" % self._device_name):
                    status = self.board["%s.regfile.stream_status.ddr_init_done" % self._device_name]
                else:
                    status = self.board["%s.regfile.status.ddr_init_done" % self._device_name]

                if status == 0x0:
                    logging.debug("Wait DDR3 %s init" % self._device_name)
                    time.sleep(0.2)
                else:
                    logging.debug("DDR3 %s initialised!" % self._device_name)
                    return

        logging.error("Cannot initilaise DDR3 %s" % self._device_name)

    def initialise_firmware(self):
        """ Initialise firmware components """
        max_retries = 4
        retries = 0

        while self.board['%s.jesd204_if.regfile_status' % self._device_name] & 0x1F != 0x1E and retries < max_retries:
            # Reset FPGA
            self._fpga.fpga_global_reset()

            self._fpga.fpga_mmcm_config(self._fsample)
            self._fpga.fpga_jesd_gth_config(self._fsample)

            self._fpga.fpga_reset()

            # Start JESD cores
            self._jesd1.jesd_core_start()
            self._jesd2.jesd_core_start()

            # Initialise FPGAs
            # I have no idea what these ranges are
            self._fpga.fpga_start(range(16), range(16))

            retries += 1
            sleep(0.2)

        if retries == max_retries:
            raise BoardError("TpmFrbFirmware: Could not configure JESD cores")

        # Initialise DDR
        self.start_ddr_initialisation()

        # Initialise power meter
        self._power_meter.initialise()

        # Initialise 10G/40G cores
        if self._device == Device.FPGA_1:
            if self.xg_40g_eth:
                self._fortyg.initialise_core()
            else:
                for teng in self._teng:
                    teng.initialise_core()

        self._patterngen.initialise()

    #######################################################################################

    def initialize_spead(self):
        """ Initialize SPEAD  """
        self.board["%s.lmc_spead_tx.control" % self._device_name] = 0x0400100C

    def send_raw_data(self):
        """ Send raw data from the TPM """
        self.board["%s.lmc_gen.payload_length" % self._device_name] = 1024
        self.board["%s.lmc_gen.raw_all_channel_mode_enable" % self._device_name] = 0x0
        self.board["%s.lmc_gen.request.raw_data" % self._device_name] = 0x1

    def send_raw_data_synchronised(self):
        """ Send raw data from the TPM """
        self.board["%s.lmc_gen.payload_length" % self._device_name] = 1024
        self.board["%s.lmc_gen.raw_all_channel_mode_enable" % self._device_name] = 0x1
        self.board["%s.lmc_gen.request.raw_data" % self._device_name] = 0x1

    def send_channelised_data(self, number_of_samples=128, first_channel=0, last_channel=511):
        """ Send channelized data from the TPM """
        logging.warning("Not supported!")

    def send_channelised_data_continuous(self, channel_id, nof_antenna=12, nof_channels=2, single_shot=False):
        """ Continuously send channelised data from a single channel
        :param channel_id: Channel ID
        :param single_shot: Send a snapshot
        """
        if self.board["%s.lmc_gen.tx_demux" % self._device_name] != 0x2:
            logging.info("Data download through 10G is not enabled! Aborting...")
            return -1

        max_spead_payload_byte_size = 7000
        single_shot_nof_packets = 8

        nof_samples = max_spead_payload_byte_size // (nof_antenna * nof_channels * 2)
        nof_beats = nof_samples * (nof_channels // 2)  # divided by mux factor
        spead_payload_byte_size = nof_samples * (nof_antenna * nof_channels * 2)

        logging.info("Transmitting frequency channels %i to %i from %s" % (
            channel_id // 2 * 2, channel_id // 2 * 2 + nof_channels - 1, self._device_name))

        self.board["%s.lmc_gen.channelized_nof_included_antenna" % self._device_name] = nof_antenna
        self.board[
            "%s.lmc_gen.channelized_multiple_channel_mode.single_shot" % self._device_name] = 1 if single_shot else 0
        self.board["%s.lmc_gen.channelized_multiple_channel_mode.enable" % self._device_name] = 1
        self.board["%s.lmc_gen.channelized_multiple_channel_mode.id" % self._device_name] = channel_id // 2 * 2
        self.board["%s.lmc_gen.channelized_multiple_channel_mode.num" % self._device_name] = nof_channels
        self.board["%s.lmc_gen.payload_length" % self._device_name] = spead_payload_byte_size
        self.board["%s.lmc_gen.channelized_pkt_length" % self._device_name] = single_shot_nof_packets * nof_beats - 1
        self.board["%s.lmc_gen.request.channelized_data" % self._device_name] = 0x1

    def stop_channelised_data_continuous(self):
        """ Stop transmission of continuous channel data """
        self.board["%s.lmc_gen.channelized_multiple_channel_mode.enable" % self._device_name] = 0x0

    def stop_channelised_data(self):
        """ Stop sending channelised data """
        self.board["%s.lmc_gen.channelized_single_channel_mode.enable" % self._device_name] = 0x0

    def send_beam_data(self):
        """ Send beam data from the TPM """
        self.board["%s.lmc_gen.request.beamformed_data" % self._device_name] = 0x1

    def configure_integrated_beam_data(self, integration_time=0.5, start_channel=0, last_channel=512):
        """ Configure transmission of integrated beam data from the TPM
        :param integration_time: Integration time in seconds
        """
        freq = self._fsample

        # Calculate integration samples (including oversampling factor)
        spectra_per_second = (freq * 32 / 27.0) / (512 * 2.0)
        integration_length = spectra_per_second * integration_time
        integration_length = int(integration_length)
        actual_integration_time = integration_length / spectra_per_second
        min_integration_time = 0.00004

        # Sanity check
        if float(actual_integration_time) < min_integration_time:
            logging.warning("Integration time %.4fs is less than the minimum integration time (%.4f). Settings "
                            "integration time to minimum" %
                            (actual_integration_time, min_integration_time))
            integration_length = int(min_integration_time * spectra_per_second)

        logging.info("Setting beam integration time on %s to %.8fs" % (self._device_name, actual_integration_time))

        download_bits = 32
        data_bits = 8

        max_bit_width = data_bits * 2 + 1 + int(ceil(log(integration_length, 2)))
        scaling_factor = 0
        if max_bit_width > download_bits:
            scaling_factor = max_bit_width - download_bits

        # Send request to TPM
        for n in range(self._nof_integrators):
            self.board["%s.lmc_integrated_gen%d.beamf_start_read_channel" % (self._device_name, n)] = 0
            self.board["%s.lmc_integrated_gen%d.beamf_last_read_channel" % (self._device_name, n)] = (last_channel - start_channel) // 2 - 1
            self.board["%s.lmc_integrated_gen%d.beamf_enable" % (self._device_name, n)] = 0x0
            self.board["%s.lmc_integrated_gen%d.beamf_scaling_factor" % (self._device_name, n)] = scaling_factor
            self.board["%s.lmc_integrated_gen%d.beamf_integration_length" % (self._device_name, n)] = integration_length
            self.board["%s.lmc_integrated_gen%d.beamf_carousel_enable" % (self._device_name, n)] = 0x0
            self.board["%s.lmc_integrated_gen%d.beamf_enable" % (self._device_name, n)] = 0x1

    def stop_integrated_beam_data(self):
        """ Stop receiving integrated beam data from the board """
        for n in range(self._nof_integrators):
            self._integrator[n].stop_integrated_beam_data()

    def stop_integrated_data(self):
        """ Stop transmission of integrated data"""
        for n in range(self._nof_integrators):
            self._integrator[n].stop_integrated_beam_data()
            
    #######################################################################################

    def download_beamforming_weights(self, weights, antenna):
        """ Apply beamforming weights
        :param weights: Weights array
        :param antenna: Antenna ID
        """
        self.board["%s.beamf.ch%02dcoeff" % (self._device_name, antenna)] = weights

    ##################### Superclass method implementations #################################

    def initialise(self):
        """ Initialise TpmFrbFirmware """
        logging.info("TpmFrbFirmware has been initialised")
        return True

    def status_check(self):
        """ Perform status check
        :return: Status
        """
        logging.info("TpmFrbFirmware : Checking status")
        return Status.OK

    def clean_up(self):
        """ Perform cleanup
        :return: Success
        """
        logging.info("TpmFrbFirmware : Cleaning up")
        return True

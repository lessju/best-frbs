import os
import sys
import threading
import time
from multiprocessing.pool import ThreadPool
from optparse import OptionParser
from threading import Thread

import h5py
import numpy as np

# Custom numpy type for creating complex signed 8-bit data
complex_8t = np.dtype([('real', np.int8), ('imag', np.int8)])

# Pharos parameters
nof_samples = None  # Populated at runtime
nof_antennas = 24   # Can be changed at runtime
nof_channels = 2    # Can be changed at runtime

# Global variables
open_data_file = None
nof_blocks = None
output = None
threads = 10
skip = 1

# Numpy-based ring buffer for communication between consumer and producer
consumer_lock = threading.Lock()
ring_buffer = None
producer = 0
consumer = 0
counter = 0


def correlate(input_data, output_data, nants):
    baseline = 0
    for antenna1 in range(nants):
        for antenna2 in range(antenna1, nants):
            for channel in range(nof_channels):
                ans = np.correlate(input_data[channel, antenna1, :], input_data[channel, antenna2, :])
                output_data[channel, baseline] = ans[0]
            baseline += 1


def process_parallel(iteration):
    global ring_buffer
    global output
    global counter
    global consumer

    # Read next data block (data is in sample/channel/antenna)
    try:
        # Wait for data to be available in
        consumer_lock.acquire()
        while not ring_buffer[consumer]['full']:
            time.sleep(0.1)

        # We have data, process it
        data = ring_buffer[consumer]['data']
        data = np.transpose(data, (1, 0, 2))
        data = np.transpose(data, (0, 2, 1)).copy()
        index = ring_buffer[consumer]['index']

        # All done, release lock
        ring_buffer[consumer]['full'] = False
        consumer = (consumer + 1) % threads
        counter += 1
        consumer_lock.release()

        data = (data['real'] + 1j * data['imag']).astype(np.complex64)

        sys.stdout.write(
            "Processed %d of %d [%.2f%%]     \r" % (counter, nof_blocks,
                                                    (counter / float(nof_blocks)) * 100))
        sys.stdout.flush()

        # Perform correlation
        correlate(data, output[:, :, index], nof_antennas)
    except Exception as e:
        import traceback
        print(e, traceback.format_exc())


def reader():
    global open_data_file
    global ring_buffer
    global nof_blocks
    global producer

    for i in range(nof_blocks):

        # Copy to ring buffer
        while ring_buffer[producer]['full']:
            time.sleep(0.1)

        # Read data
        open_data_file.seek(i * nof_samples * nof_antennas * nof_channels * 2)
        data = np.fromfile(open_data_file, dtype=complex_8t, count=nof_samples * nof_channels * nof_antennas)
        ring_buffer[producer]['data'][:] = np.reshape(data, (nof_samples, nof_channels, nof_antennas))

        ring_buffer[producer]['index'] = i
        ring_buffer[producer]['full'] = True

        # Update producer pointer
        producer = (producer + 1) % threads


def correlator(data_file):
    global open_data_file
    global ring_buffer
    global nof_blocks
    global output

    # Check that directory exists
    if not os.path.exists(data_file):
        print("Specified data file [%s] does not exist" % data_file)
        exit()

    output_directory = os.path.join(os.path.dirname(data_file), data_file + ".h5")

    # Get number of samples in file
    total_samples = os.path.getsize(data_file) // (nof_antennas * nof_channels * 2)
    nof_blocks = total_samples // nof_samples
    nof_baselines = int(0.5 * (nof_antennas ** 2 + nof_antennas))

    # Open file for processing
    open_data_file = open(data_file, 'rb')

    # Create output buffer
    output = np.zeros((nof_channels, nof_baselines, nof_blocks), dtype=np.complex64)

    # Create ring buffer
    ring_buffer = []
    for i in range(threads * 2):
        ring_buffer.append(
            {'full': False, 'index': 0, 'data': np.zeros((nof_samples, nof_channels, nof_antennas), dtype=complex_8t)})

    # Initialise producer thread
    producer_thread = Thread(target=reader)
    producer_thread.start()

    # Start up consumer thread pool
    start = time.time()
    pool = ThreadPool(threads)
    pool.map(process_parallel, range(nof_blocks))
    end = time.time()
    print("Took {}s to correlate {} blocks of {} samples each".format(end-start, nof_blocks, nof_samples))

    # Join producer
    producer_thread.join()

    # All done, write correlations to file
    f = h5py.File(output_directory, "w")

    dset = f.create_dataset("Vis", (nof_blocks, nof_channels, nof_baselines),
                            maxshape=(nof_blocks, nof_channels, nof_baselines),
                            dtype='c16')

    output = np.transpose(output, (0, 2, 1))
    dset[:] = np.transpose(output, (1, 0, 2))[:]

    # Create baselines data set
    dset2 = f.create_dataset("Baselines", (nof_baselines, 3))

    antenna1, antenna2, baselines, index = [], [], [], 0
    for i in range(nof_antennas):
        for j in range(i, nof_antennas):
            antenna1.append(i)
            antenna2.append(j)
            baselines.append(index)
            index += 1

    dset2[:, :] = np.transpose([baselines, antenna1, antenna2])

    # Ready file
    f.flush()
    f.close()


if __name__ == "__main__":

    # Command line options
    p = OptionParser()
    p.set_usage('best2_process_corr.py [options] INPUT_FILE')
    p.set_description(__doc__)

    p.add_option('-f', '--file', dest='file', action='store', default="channel_output.dat",
                 help="Data file to process (default: 'channel_output.dat')")
    p.add_option('-s', '--samples', dest='nof_samples', action='store', default=1048576,
                 type='int', help='Number of samples (default: 1048576)')
    p.add_option('-a', '--antennas', dest='nof_antennas', action='store', default=24,
                 type='int', help='Number of antennas (default: 24)')
    p.add_option('-c', '--channels', dest='nof_channels', action='store', default=2,
                 type='int', help='Number of channels (default: 2)')
    p.add_option('-d', '--directory', dest='directory', action='store', default=None,
                 help='If specified, correlate all .dat files in directory (default: None)')
    opts, args = p.parse_args(sys.argv[1:])

    # Update global config
    nof_antennas = opts.nof_antennas
    nof_channels = opts.nof_channels
    nof_samples = opts.nof_samples

    if opts.directory is None:
        correlator(opts.file)
    else:
        for f in os.listdir(opts.directory):
            if f.endswith(".dat") and f.startswith("channel"):
                print("Processing {}".format(f))
                correlator(os.path.join(opts.directory, f))

#!/usr/bin/env python
import glob

import matplotlib.pyplot as plt
import numpy as np

from pyfrbs.persisters import *


def extract_values(values):
    """ Extract values from string representation of list
    :param values: String representation of values
    :return: List of values
    """

    # Return list
    converted = []

    # Loop over all comma separated values
    for item in values.split(","):
        # Check if item contains a semi-colon
        if item.find(":") > 0:
            index = item.find(":")
            lower = item[:index]
            upper = item[index + 1:]
            converted.extend(range(int(lower), int(upper) + 1))
        else:
            converted.append(int(item))

    return converted


# Script entry point
if __name__ == "__main__":
    from optparse import OptionParser
    from sys import argv

    parser = OptionParser(usage="usage: %spectrum_plotter.py [options]")
    parser.add_option("-l", "--log", action="store_true", dest="log",
                      default=False, help="Log the data (20log(X)) [default: False]")
    parser.add_option("-t", "--timestamp", action="store", dest="timestamp",
                      default=None, help="Timestamp to plot [default: Latest file in directory]")
    parser.add_option("-d", "--data_directory", action="store", dest="directory",
                      default="/data", help="Data directory [default: /data]")
    parser.add_option("-b", "--beams", action="store", dest="beams",
                      default="0", help="Beams to plot [default: 0]")
    parser.add_option("-c", "--channels", action="store", dest="channels",
                      default="0:351", help="Channels to plot [default: All]")
    parser.add_option("-s", "--nof_samples", action="store", dest="nof_samples",
                      type='int', default=128, help="Number of samples to average over [default: 128]")
    parser.add_option("-o", "--offset", action="store", dest="offset",
                      type='int', default=0, help="Sample offset [default: 0]")
    parser.add_option("--waterfall", action="store_true", dest="waterfall",
                      default=False, help="Generate waterfall plots rather than spectrum [default: False]")
    parser.add_option("--power", action="store_true", dest="power",
                      default=False, help="Generate the power plot per channel [default: False]")

    (conf, args) = parser.parse_args(argv[1:])

    # Check if directory exists
    if not (os.path.exists(conf.directory) and os.path.isdir(conf.directory)):
        logging.error("Specified directory (%s) does not exist or is not a directory" % conf.directory)
        exit(0)

    # Set logging
    log = logging.getLogger('')
    log.setLevel(logging.DEBUG)
    str_format = logging.Formatter("%(asctime)s - %(levelname)s - %(message)s")

    # Remove any pending lock file
    for f in glob.glob(os.path.join(conf.directory, "*.lock")):
        os.remove(f)

    # Ge channel file handler
    beam_file_mgr = BeamFormatFileManager(root_path=conf.directory, daq_mode=FileDAQModes.Integrated)
    if conf.nof_samples == -1:
        beam_file_mgr.read_data(timestamp=conf.timestamp, tile_id=0, n_samples=1)
        conf.nof_samples = beam_file_mgr.n_blocks * beam_file_mgr.n_samples

    # Parse all string-representation parameters
    conf.channels = extract_values(conf.channels)
    conf.beams = extract_values(conf.beams)

    # Process beams
    for j, b in enumerate(conf.beams):
        f = plt.figure(j)
        # f.tight_layout()
        plt.subplot(111)

        data, timestamps = beam_file_mgr.read_data(timestamp=conf.timestamp,
                                                    tile_id=0,
                                                    polarizations=[0],
                                                    channels=conf.channels,
                                                    beams=[b],
                                                    n_samples=conf.nof_samples,
                                                    sample_offset=conf.offset)
        if len(data) == 0:
            print("Nothing to plot")
            exit(0)

        # Remove beam and pol dimension (samp/chans remain) and spikes 
        data = data[0, :, :, 0]
        #data[np.where(data > 1e8)] = 0

        if conf.log:
            data[np.where(data == 0)] = 1

            if conf.waterfall:
                plt.imshow(10 * np.log10(data[:, :]), aspect='auto')
            elif conf.power:
                for i, c in enumerate(conf.channels):
                    plt.plot(10 * np.log10(data[:, i]), label="Channel {}".format(c))
            else:
                plt.plot(np.array(conf.channels),
                            10 * np.log10(np.sum(data[:, :], axis=0) / conf.nof_samples))

        else:
            if conf.waterfall:
                plt.imshow(data[:, :], aspect='auto')
            elif conf.power:
                for i, c in enumerate(conf.channels):
                    plt.plot(data[:, i], label="Channel {}".format(c))
            else:
                plt.plot(np.array(conf.channels),
                            np.sum(data[:, :], axis=0) / conf.nof_samples)

        if conf.waterfall:
            plt.title("Beam {} Waterfall".format(b))
            plt.xlabel("Sample)")
            plt.ylabel("Channel (log)" if conf.log else "Channel")
            plt.colorbar()
        elif conf.power:
            plt.title("Beam {}".format(b))
            plt.xlabel("Sample)")
            plt.ylabel("Arbitrary Power (log)" if conf.log else "Arbitrary Power")
            plt.legend()
        else:
            plt.title("Beam {} Spectrum".format(b))
            plt.xlabel("Channel)")
            plt.ylabel("Arbitrary Power (log)" if conf.log else "Arbitrary Power")
            plt.grid()

    plt.show()

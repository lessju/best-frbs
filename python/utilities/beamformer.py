#! /usr/bin/python

import datetime
import logging
import os
import warnings

import numpy as np
from astropy import constants
from astropy import units as u
from astropy.coordinates import Angle, AltAz, SkyCoord, EarthLocation
from astropy.time import TimeDelta
from astropy.time.core import Time
from astropy.utils.exceptions import AstropyWarning

warnings.simplefilter('ignore', category=AstropyWarning)

__author__ = 'Alessio Magro'


# --------------------------------------------- Beamformer Class ---------------------------------------
class AntennaArray(object):
    """ Class representing antenna array """

    def __init__(self, filepath=None):
        self._ref_lat = None
        self._ref_lon = None
        self._ref_height = None

        self._x = None
        self._y = None
        self._z = None
        self._height = None

        self._n_antennas = None
        self._baseline_vectors_ecef = None
        self._baseline_vectors_enu = None

        # If filepath is defined, initialise array
        if filepath:
            self.load_from_file(filepath)

    def positions(self):
        """
        :return: Antenna positions
        """
        return self._x, self._y, self._z, self._height

    @property
    def size(self):
        return self._n_antennas

    @property
    def lat(self):
        """ Return reference latitude
        :return: reference latitude
        """
        return self._ref_lat

    @property
    def lon(self):
        """ Return reference longitude
        :return: reference longitude
        """
        return self._ref_lon

    @property
    def height(self):
        """
        Retern reference height
        :return: reference height
        """
        return self._ref_height

    def load_from_file(self, filepath):
        """ Load antenna positions from file
        :param filepath: Path to file
        """

        # Check that file exists
        if not os.path.exists(filepath):
            logging.error("Could not load antenna positions from %s" % filepath)
            return

        # Load file
        array = np.loadtxt(filepath, delimiter=',')

        # Keep track of how many antennas are in the array
        self._n_antennas = len(array)

        # Convert file data to astropy format, and store lat, lon and height for each antenna
        if self._x is None:
            self._x = []

        if self._y is None:
            self._y = []

        if self._z is None:
            self._z = []

        if self._height is None:
            self._height = []

        for i in range(len(array)):
            latitude = Angle(str(array[i, 0]) + 'd')
            longitude = Angle(str(array[i, 1]) + 'd')
            height = u.Quantity(array[i, 2], u.m)

            loc = EarthLocation.from_geodetic(lon=longitude, lat=latitude, height=height)
            (x, y, z) = loc.to_geocentric()

            self._x.append(x)
            self._y.append(y)
            self._z.append(z)
            self._height.append(height)

        # Use first antenna as reference antenna
        self._ref_lat = Angle(str(array[0, 0]) + 'd')
        self._ref_lon = Angle(str(array[0, 1]) + 'd')
        self._ref_height = u.Quantity(array[0, 2], u.m)


class Beamformer(object):
    """ Helper class for generating beamforming coefficients """

    def __init__(self, tile, nof_channels, nof_antennas, nof_pols, nof_beams,
                 array=None, bw=400e6, fch1=50.78125e6, polarisation_fpga_map=None):
        """ Beamforming weight generation class
        :param tile: Reference to tile object
        :param nof_antennas: Number of antennas
        :param nof_beams: Number of beams
        :param array: AntennaArray object, required for source pointing
        :param bw: Array bandwidth
        :param fch1: Center frequency of first channel
        :param polarisation_fpga_map: Mapping between polarisation index and FPGA index
        """

        # Initialise class member
        self.tile = tile
        self.chan_bw = 400e6 / 512
        self.nof_channels = int(round(bw / self.chan_bw))
        self.nof_antennas = nof_antennas
        self.nof_pols = nof_pols
        self.nof_beams = nof_beams
        self.bandwidth = bw
        self.start_channel_frequency = fch1
        self.start_channel = int(round(fch1 / self.chan_bw))
        self.pol_fpga_map = polarisation_fpga_map if polarisation_fpga_map is not None else {0: 0, 1: 1}
        self.array = array
        self.weights = None
        self.delay = None

        # Get displacement vectors to each antenna from reference antenna
        if self.array is not None:
            self._baseline_vectors_ecef = self._calculate_ecef_baseline_vectors()
            self._baseline_vectors_enu = self._rotate_ecef_to_enu(self._baseline_vectors_ecef, self.array._ref_lat,
                                                                  self.array._ref_lon)

    def initialize(self):
        self.tile.tpm.beamf_fd[0].initialise_beamformer()
        self.tile.tpm.beamf_fd[1].initialise_beamformer()
        self.tile.tpm.beamf_fd[0].set_regions([[self.start_channel, self.nof_channels, 0]])
        self.tile.tpm.beamf_fd[1].set_regions([[self.start_channel, self.nof_channels, 0]])
        self.tile.tpm.beamf_fd[0].antenna_tapering = ([4.0] * 8)
        self.tile.tpm.beamf_fd[1].antenna_tapering = ([4.0] * 8)
        self.tile.tpm.beamf_fd[0].compute_calibration_coefs()
        self.tile.tpm.beamf_fd[1].compute_calibration_coefs()

        ## Interface towards beamformer in FPGAs

    ## Set regions. The method computes the total number of channels and the arrays and writes in the registers
    def set_regions(self, region_array):
        """ Set frequency regions.
            Regions are defined in a 2-d array, for a maximum of 16 regions. 
            Each element in the array defines a region, with 
            the form [start_ch, nof_ch, beam_index]
            - start_ch:    region starting channel (currently must be a 
                           multiple of 2, LS bit discarded)
            - nof_ch:      size of the region: must be multiple of 8 chans
            - beam_index:  beam used for this region, range [0:8) 
            Total number of channels must be <= 384 
            The routine computes the arrays beam_index, region_off, region_sel, 
            and the total number of channels nof_chans, and programs it in the HW"""
        self.tile.tpm.beamf_fd[0].set_regions(region_array)
        self.tile.tpm.beamf_fd[1].set_regions(region_array)

    ## Set delay for a beam
    def set_delay(self, delay_array, beam_index):
        """ The method specifies the delay in seconds and the delay rate in seconds/seconds. 
            The delay_array specifies the delay and delay rate for each antenna. 
            beam_index specifies which beam is described (range 0:7).
            Delay is updated inside the delay engine at the time specified
            by method load_delay"""
        self.tile.tpm.beamf_fd[0].set_delay(delay_array[0:8], beam_index)
        self.tile.tpm.beamf_fd[1].set_delay(delay_array[8:], beam_index)

    ## Loads the delay
    def load_delay(self, load_time=0):
        self.tile.tpm.beamf_fd[0].load_delay(load_time)
        self.tile.tpm.beamf_fd[1].load_delay(load_time)

    ## Calibration 
    def load_calibration(self, antenna, calibration_coefs):
        """Loads calibration coefficients. 
           calibration_coefs is a bidimensional complex array of the form
           calibration_coefs[channel, polarization], with each element representing
           a normalized coefficient, with (1.0, 0.0) the normal, expected response for 
           an ideal antenna. 
           Channel is the index specifying the channels at the beamformer output, 
           i.e. considering only those channels actually processed and beam assignments. 
           The polarization index ranges from 0 to 3. 
           0: X polarization direct element
           1: X->Y polarization cross element 
           2: Y->X polarization cross element 
           3: Y polarization direct element
           The calibration coefficients may include any rotation matrix (e.g.
           the parallactic angle), but do not include the geometric delay. """
        if antenna < 8:
            self.tile.tpm.beamf_fd[0].load_calibration(antenna, calibration_coefs)
        else:
            self.tile.tpm.beamf_fd[1].load_calibration(antenna - 8, calibration_coefs)

    def load_antenna_tapering(self, tapering_coefs):
        """Tapering_coefs is a vector of 16 values, one per antenna.
           Default (at initialization) is 1.0"""
        self.tile.tpm.beamf_fd[0].load_antenna_tapering(tapering_coefs[0:8])
        self.tile.tpm.beamf_fd[1].load_antenna_tapering(tapering_coefs[8:])

    def load_beam_angle(self, angle_coefs):
        """Angle_coefs is an array of one element per beam, specifying a rotation
           angle, in radians, for the specified beam. The rotation is the same 
           for all antennas. Default is 0 (no rotation). 
           A positive pi/4 value transfers the X polarization to the Y polarization. 
           The rotation is applied after regular calibration.
           Rot_matrix[beam] then contains the rotation matrix of each beam"""
        self.tile.tpm.beamf_fd[0].load_beam_angle(angle_coefs)
        self.tile.tpm.beamf_fd[1].load_beam_angle(angle_coefs)

    def compute_calibration_coefs(self):
        """Compute the calibration coefficients and load them in the hardware."""
        self.tile.tpm.beamf_fd[0].compute_calibration_coefs()
        self.tile.tpm.beamf_fd[1].compute_calibration_coefs()

    # -------------------------------- POINTING FUNCTIONS -------------------------------------

    def point_array_static(self, altitude, azimuth):
        """ Calculate the delay given the altitude and azimuth coordinates of a sky object as astropy angles
        :param altitude: altitude coordinates of a sky object as astropy angle
        :param azimuth: azimuth coordinates of a sky object as astropy angles
        :return: The (delay,delay rate) tuple for each antenna
        """

        # Type conversions if required
        if type(altitude) is not Angle:
            if type(altitude) is str:
                altitude = Angle(altitude)
            else:
                altitude = Angle(altitude, u.deg)

        if type(azimuth) is not Angle:
            if type(azimuth) is str:
                azimuth = Angle(azimuth)
            else:
                azimuth = Angle(azimuth, u.deg)

        # Calculate East-North-Up vector
        vectors_enu = self._rotate_ecef_to_enu(self._baseline_vectors_ecef, self.array._ref_lat, self.array._ref_lon)

        # Compute the delays
        delay = self._delays_from_altitude_azimuth(altitude.rad, azimuth.rad, vectors_enu)
        return delay

    def point_array(self, right_ascension, declination, pointing_time=None):
        """ Calculate the phase shift between two antennas which is given by the phase constant (2 * pi / wavelength)
        multiplied by the projection of the baseline vector onto the plane wave arrival vector
        :param right_ascension: Right ascension of source (astropy angle, or string that can be converted to angle)
        :param declination: Declination of source (astropy angle, or string that can be converted to angle)
        :param pointing_time: Time of observation (in format astropy time)
        :return: The (delay,delay rate) tuple for each antenna
        """

        if pointing_time is None:
            pointing_time = Time(datetime.datetime.utcnow(), scale='utc')

        pointing_time1 = pointing_time + TimeDelta(1.0, format='sec')

        reference_antenna_loc = EarthLocation.from_geodetic(self.array._ref_lon, self.array._ref_lat,
                                                            height=self.array._ref_height,
                                                            ellipsoid='WGS84')
        alt, az = self._ra_dec_to_alt_az(Angle(right_ascension), Angle(declination),
                                         Time(pointing_time), reference_antenna_loc)
        alt1, az1 = self._ra_dec_to_alt_az(Angle(right_ascension), Angle(declination),
                                           Time(pointing_time1), reference_antenna_loc)

        delay_0 = self.point_array_static(alt, az)
        delay_1 = self.point_array_static(alt1, az1) - delay_0

        delay = np.array([delay_0, delay_1])
        self.delay = delay.transpose()

        # load the delays in beam 0
        self.set_delay(self.delay, 0)

    def _calculate_ecef_baseline_vectors(self):
        """ Calculate the displacement vectors in metres for each antenna relative to master antenna.
        :return: an (n, 3) array of displacement vectors for each antenna
        """
        displacement_vectors = np.full([self.nof_antennas, 3], np.nan)
        array_positions = self.array.positions()

        ref_loc = EarthLocation.from_geodetic(lon=self.array.lon, lat=self.array.lat,
                                              height=self.array.height, ellipsoid='WGS84')
        ant_ref_pos = ref_loc.to_geocentric()
        ant_ref_values = np.array(
            [ant_ref_pos[0].to(u.m).value, ant_ref_pos[1].to(u.m).value, ant_ref_pos[2].to(u.m).value])

        for i in range(0, self.nof_antennas):
            ant_i_pos = (array_positions[0][i], array_positions[1][i], array_positions[2][i])
            ant_i_pos_values = np.array(
                [ant_i_pos[0].to(u.m).value, ant_i_pos[1].to(u.m).value, ant_i_pos[2].to(u.m).value])
            displacement_vectors[i, :] = ant_i_pos_values - ant_ref_values

        return displacement_vectors

    def _rotate_ecef_to_enu(self, ecef_vectors, latitude, longitude):
        """
        Rotates a set of vectors expressed in the geocentric Earth-Centred Earth-Fixed coordinate system to the local
        East-North-Up surface tangential (horizontal) coordinate system with the specified latitude, longitude.
        :param ecef_vectors: (n, 3) array of vectors expressed in ECEF
        :param latitude: Geodectic latitude (astropy angle)
        :param longitude: Geodectic longitude (astropy angle)
        :return: array containing vectors rotated to East North Up coordinate system
        """

        rotation_matrix = self._rotation_matrix_ecef_to_enu(latitude, longitude)
        vectors_enu = np.dot(rotation_matrix, np.array(ecef_vectors).transpose())
        return vectors_enu.transpose()

    @staticmethod
    def _ra_dec_to_alt_az(right_ascension, declination, time, location):
        """ Calculate the altitude and azimuth coordinates of a sky object from right ascension and declination and time
        :param right_ascension: Right ascension of source (in astropy Angle on string which can be converted to Angle)
        :param declination: Declination of source (in astropy Angle on string which can be converted to Angle)
        :param time: Time of observation (as astropy Time")
        :param location: astropy EarthLocation
        :return: Array containing altitude and azimuth of source as astropy angle
        """

        # Initialise SkyCoord object using the default frame (ICRS) and convert to horizontal
        # coordinates (altitude/azimuth) from the antenna's perspective.
        sky_coordinates = SkyCoord(ra=right_ascension, dec=declination)
        c_altaz = sky_coordinates.transform_to(AltAz(obstime=time, location=location))

        return c_altaz.alt, c_altaz.az

    @staticmethod
    def _rotation_matrix_ecef_to_enu(latitude, longitude):
        """
        Compute a rotation matrix for converting vectors in ECEF to local
        ENU (horizontal) cartesian coordinates at the specified latitude, longitude.
        :param latitude: The geodetic latitude of the reference point
        :param longitude: The geodetic longitude of the reference point
        :return: A 3*3 rotation (direction cosine) matrix.
        """
        sin_lat = np.sin(latitude)
        cos_lat = np.cos(latitude)
        sin_long = np.sin(longitude)
        cos_long = np.cos(longitude)

        rotation_matrix = np.zeros((3, 3), dtype=float)
        rotation_matrix[0, 0] = -sin_long
        rotation_matrix[0, 1] = cos_long
        rotation_matrix[0, 2] = 0

        rotation_matrix[1, 0] = -sin_lat * cos_long
        rotation_matrix[1, 1] = -sin_lat * sin_long
        rotation_matrix[1, 2] = cos_lat

        rotation_matrix[2, 0] = cos_lat * cos_long
        rotation_matrix[2, 1] = cos_lat * sin_long
        rotation_matrix[2, 2] = sin_lat

        return rotation_matrix

    @staticmethod
    def _delays_from_altitude_azimuth(altitude, azimuth, displacements):
        """
        Calculate the delay using a target altitude Azimuth
        :param altitude: The altitude of the target astropy angle
        :param azimuth: The azimuth of the target astropy angle
        :param displacements: Numpy array: The displacement vectors between the antennae in East, North, Up
        :return: The delay in seconds for each antenna
        """
        scale = np.array([-np.sin(azimuth) * np.cos(altitude),
                          -np.cos(azimuth) * np.cos(altitude),
                          -np.sin(altitude)])

        path_length = np.dot(scale, displacements.transpose())

        k = 1.0 / constants.c.value
        delay = np.multiply(k, path_length)
        return delay

    def is_above_horizon(self, right_ascension, declination, pointing_time):
        """
        Determine whether the target is above the horizon, at the specified time for the reference antenna.
        :param right_ascension: The right ascension of the target as a astropy angle
        :param declination: The declination of the target as an astropy angle.
        :param pointing_time: The observation time as an astropy Time.
        :return: True if the target coordinates are above the horizon at the specified time, false otherwise.
        """
        reference_antenna_loc = EarthLocation.from_geodetic(self._array.lon, self._array.lat,
                                                            height=self._array.height,
                                                            ellipsoid='WGS84')
        alt, az = self._ra_dec_to_alt_az(Angle(right_ascension), Angle(declination), Time(pointing_time),
                                         reference_antenna_loc)

        return alt > 0.0

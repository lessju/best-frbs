from matplotlib import pyplot as plt
from optparse import OptionParser
import numpy as np
import os
import sys

# Channel data type
complex_8t = np.dtype([('real', np.int8), ('imag', np.int8)])

if __name__ == "__main__":
    # Command line options
    p = OptionParser()
    p.set_usage('channel_plotter.py [options]')
    p.set_description(__doc__)

    p.add_option('-f', '--file', dest='file', action='store', default="beam_output.dat",
                 help="Data file to process (default: 'channel_output.dat')")
    p.add_option('-s', '--samples', dest='nof_samples', action='store', default=1024,
                 type='int', help='Number of samples to plot (default: 1024)')
    p.add_option('-c', '--channels', dest='nof_channels', action='store', default=352,
                 type='int', help='Number of channels (default: 352)')
    p.add_option('-a', '--antennas', dest='nof_antennas', action='store', default=24,
                 type='int', help='Number of antennas (default: 24)')
    opts, args = p.parse_args(sys.argv[1:])

    # Check that directory exists
    if not os.path.exists(opts.file):
        print("Specified data file [%s] does not exist" % opts.file)
        exit()

    data = np.fromfile(opts.file, dtype=complex_8t, count=opts.nof_channels * opts.nof_antennas * opts.nof_samples)
    #data = np.abs((data['real'] + 1j * data['imag']).astype(np.complex64))
    data = (data['real'] + 1j * data['imag']).astype(np.complex64).real

    data = np.reshape(data, (opts.nof_samples, opts.nof_channels, opts.nof_antennas))

    for i in range(32):
        plt.plot(data[:, 0, i], label='Antenna {}'.format(i))

    plt.legend()
    plt.show()

import os
import threading
import matplotlib.pyplot as plt
from multiprocessing.pool import ThreadPool
from optparse import OptionParser
from threading import Thread
import time
import sys
import numpy as np

# Custom numpy type for creating complex signed 8-bit data
complex_8t = np.dtype([('real', np.int8), ('imag', np.int8)])

# Pharos parameters
nof_samples = None  # Populated at runtime
nof_beams = 1  # Overridden at runtime
nof_antennas = 24
nof_channels = 2

# Global variables
open_data_file = None
nof_blocks = None
output = None
counter = 0
skip = 64
threads = 8

# Numpy-based ring buffer for communication between consumer and producer
consumer_lock = threading.Lock()
ring_buffer = None
producer = 0
consumer = 0

# Calibration coefficients for channel 1
calib_coeffs_1 = np.empty((4, 24), dtype=np.complex64)
calib_coeffs_1[0, :] = [1.000000+0.000000j, 0.585489+0.758270j, 0.818036-0.570244j, 0.936414-0.538460j, 
                        -0.899697+0.549478j, 0.795408+0.728058j, 0.294317+0.970476j, 0.888182+0.502027j, 
                        1.046113+0.266266j, 1.074448+0.033028j, -0.124922+1.072354j, 0.880750+0.619620j, 
                        -0.662482-0.722036j, -0.443496-0.881392j, 1.064204-0.198717j, -0.229207-1.106148j, 
                        1.001825+0.433718j, 0.942423+0.493234j, 0.912281+0.529275j, 0.897073+0.907441j, 
                        -0.673465-0.805016j, -0.984839+0.000610j, 0.187262-1.059167j, 1.392167-0.819615j]
calib_coeffs_1[1, :] = [1.000000+0.000000j, 0.845180-0.451045j, -0.959933+0.269980j, 0.021545+1.079975j, 
                        -0.899697+0.549478j, 0.850346-0.663064j, 0.040705-1.013306j, -0.867412+0.537117j, 
                        1.046113+0.266266j, 0.211486-1.053947j, 0.470149-0.971858j, -0.966927+0.474028j, 
                        -0.662482-0.722036j, -0.942938+0.290533j, -1.070441-0.161782j, 1.079995+0.331226j, 
                        1.001825+0.433718j, 0.643281-0.847130j, -0.687876-0.799510j, -1.227103+0.349868j, 
                        -0.673465-0.805016j, -0.163396+0.971190j, -0.524701+0.938931j, 0.048763+1.614782j]
calib_coeffs_1[2, :] = [1.000000+0.000000j, -0.650186+0.703583j, -0.585403+0.807257j, -0.922556-0.561873j, 
                        -0.899697+0.549478j, -0.585440+0.905540j, -0.596693-0.820002j, 0.012809-1.020164j, 
                        1.046113+0.266266j, 0.146353+1.064946j, -0.234162-1.053905j, 0.119475-1.070222j, 
                        -0.662482-0.722036j, 0.795234-0.584076j, -0.939926+0.537175j, -0.859453+0.733102j, 
                        1.001825+0.433718j, -0.329412+1.011399j, -1.035498-0.200334j, 0.363945-1.223002j, 
                        -0.673465-0.805016j, -0.164599-0.970987j, 0.170948+1.061922j, -1.388296-0.826154j]
calib_coeffs_1[3, :] = [1.000000+0.000000j, -0.933396-0.215736j, -0.397708-0.914434j, 0.914430+0.575003j, 
                        -0.899697+0.549478j, -1.076670-0.059367j, 1.009461-0.097135j, -0.027391+1.019877j, 
                        1.046113+0.266266j, -0.852225+0.655165j, 1.026653+0.333962j, -0.134761+1.068405j, 
                        -0.662482-0.722036j, 0.901541+0.400955j, 0.015072-1.082493j, 0.869845-0.720742j, 
                        1.001825+0.433718j, -1.041642+0.215459j, 0.698803-0.789977j, -0.381391+1.217674j, 
                        -0.673465-0.805016j, 0.761584-0.624419j, -1.001431-0.392477j, 1.376345+0.845915j]

# Calibration coefficients for channel 2
calib_coeffs_2 = np.empty((4, 24), dtype=np.complex64)
calib_coeffs_2[0, :] = [1.000000+0.000000j, 0.588373+0.758924j, 0.848441-0.551248j, 0.947186-0.522623j, 
                        -0.931887+0.485113j, 0.764037+0.757858j, 0.246394+0.985182j, 0.876751+0.548091j, 
                        1.018670+0.357046j, 1.076672+0.125161j, -0.256419+1.054614j, 0.838109+0.700540j, 
                        -0.556517-0.800379j, -0.341094-0.939892j, 1.079270-0.039714j, -0.061641-1.129409j, 
                        0.888017+0.632072j, 0.823863+0.662824j, 0.814987+0.679124j, 0.745783+1.051305j, 
                        -0.479541-0.934768j, -0.978416-0.195218j, 0.408218-0.998323j, 1.562042-0.503427j]
calib_coeffs_2[1, :] = [1.000000+0.000000j, 0.845082-0.456054j, -0.981097+0.247334j, 0.011207+1.081744j, 
                        -0.931887+0.485113j, 0.872816-0.629519j, 0.085358-1.011933j, -0.898192+0.512198j, 
                        1.018670+0.357046j, 0.299903-1.041607j, 0.583614-0.915072j, -1.014110+0.405911j, 
                        -0.556517-0.800379j, -0.983081+0.182463j, -1.034146-0.311356j, 1.023686+0.481072j, 
                        0.888017+0.632072j, 0.788870-0.704110j, -0.551650-0.906144j, -1.279202+0.158351j, 
                        -0.479541-0.934768j, -0.352912+0.933199j, -0.709061+0.812725j, -0.297141+1.614038j]
calib_coeffs_2[2, :] = [1.000000+0.000000j, -0.652249+0.704784j, -0.624650+0.795951j, -0.909154-0.586290j, 
                        -0.931887+0.485113j, -0.622411+0.877899j, -0.551679-0.852610j, 0.067019-1.031797j, 
                        1.018670+0.357046j, 0.052965+1.082627j, -0.098319-1.080877j, 0.219570-1.070034j, 
                        -0.556517-0.800379j, 0.871291-0.490502j, -1.008466+0.386519j, -0.965250+0.589624j, 
                        0.888017+0.632072j, -0.518857+0.921343j, -0.990784-0.379158j, 0.572189-1.155004j, 
                        -0.479541-0.934768j, 0.032247-0.997180j, -0.063527+1.076687j, -1.183698-1.136782j]
calib_coeffs_2[3, :] = [1.000000+0.000000j, -0.935023-0.218817j, -0.364221-0.943965j, 0.897040+0.604662j, 
                        -0.931887+0.485113j, -1.070785-0.107337j, 1.014826-0.037704j, -0.087993+1.030220j, 
                        1.018670+0.357046j, -0.915061+0.580991j, 0.978961+0.468611j, -0.241290+1.065346j, 
                        -0.556517-0.800379j, 0.856960+0.515132j, 0.183947-1.064220j, 0.977044-0.569868j, 
                        0.888017+0.632072j, -1.057388+0.004158j, 0.832742-0.657232j, -0.595565+1.143126j, 
                        -0.479541-0.934768j, 0.882878-0.464688j, -0.892545-0.605520j, 1.160329+1.160625j]

def beamform(input_data, nof_antennas, nof_channels):
    # Apply calibration coefficients

    power = np.zeros((nof_channels, nof_beams))
    for i in range(nof_beams):
         ch1 = np.sum(input_data[0, :, :] * calib_coeffs_1[i, :], axis=1)
         ch2 = np.sum(input_data[1, :, :] * calib_coeffs_2[i, :], axis=1)

         # Calculate power and integrate in time
         power[0, i] = 10 * np.log10(np.sum(np.power(np.abs(ch1), 2)) / nof_samples)
         power[1, i] = 10 * np.log10(np.sum(np.power(np.abs(ch2), 2)) / nof_samples)

    # Return for two channels
    return power


def process_parallel(iteration):
    global ring_buffer
    global consumer
    global nof_samples
    global nof_blocks
    global counter
    global output

    # Read next data block (data is in channel/antenna/pol/time)
    try:
        # Wait for data to be available in
        consumer_lock.acquire()
        while not ring_buffer[consumer]['full']:
            time.sleep(0.1)

        # We have data, process it
        data = ring_buffer[consumer]['data']
        data = np.transpose(data, (1, 0, 2)).copy()
        index = ring_buffer[consumer]['index']

        # All done, release lock
        ring_buffer[consumer]['full'] = False
        consumer = (consumer + 1) % threads
        counter += 1
        consumer_lock.release()

        data = (data['real'] + 1j * data['imag']).astype(np.complex64)

        sys.stdout.write(
            "Processed %d of %d [%.2f%%]      \r" % (counter, nof_blocks,
                                                     (counter / float(nof_blocks)) * 100))
        sys.stdout.flush()

        # Perform correlation
        output[index, :, :] = beamform(data, nof_antennas, nof_channels)
    except Exception as e:
        import traceback
        print e, traceback.format_exc()


def reader():
    global open_data_file
    global ring_buffer
    global nof_samples
    global nof_blocks
    global producer
    global consumer

    for i in range(nof_blocks):

        # Copy to ring buffer
        while ring_buffer[producer]['full']:
            time.sleep(0.1)

        # Read data
        open_data_file.seek(i * nof_samples * skip * nof_antennas * nof_channels * 2)
        data = np.fromfile(open_data_file, dtype=complex_8t, count=nof_samples * nof_channels * nof_antennas)
        ring_buffer[producer]['data'][:] = np.reshape(data, (nof_samples, nof_channels, nof_antennas))

        ring_buffer[producer]['index'] = i
        ring_buffer[producer]['full'] = True

        # Update producer pointerdirectory
        producer = (producer + 1) % threads


def beamformer(data_file, samples, beams, skip_param):
    global open_data_file
    global ring_buffer
    global nof_samples
    global nof_blocks
    global nof_beams
    global counter
    global output
    global skip

    # Check that directory exists
    if not os.path.exists(data_file):
        print("Specified data file [%s] does not exist" % data_file)
        exit()

    nof_samples = samples
    nof_beams = beams
    if skip_param == 0:
        skip = 1
    else:
        skip = skip_param

    # Get number of samples in file
    nof_samples = samples
    total_samples = os.path.getsize(data_file) / (nof_antennas * nof_channels * 2)
    nof_blocks = (total_samples / samples) / skip

    # Open file for processing
    open_data_file = open(data_file, 'rb')

    # Create output buffer
    output = np.zeros((nof_blocks, nof_channels, nof_beams))

    # Create ring buffer
    ring_buffer = []
    for i in range(threads):
        ring_buffer.append(
            {'full': False, 'index': 0, 'data': np.zeros((nof_samples, nof_channels, nof_antennas), dtype=complex_8t)})

    # Initialise producer thread
    producer_thread = Thread(target=reader)
    producer_thread.start()

    # Start up consumer thread pool
    start = time.time()
    pool = ThreadPool(threads)
    pool.map(process_parallel, range(nof_blocks))
    end = time.time()
    print "Took {}s to correlate {} blocks of {} samples each".format(end - start, nof_blocks, nof_samples)

    # Join producer
    producer_thread.join()

    # All done, save data
    np.save("beamformed_data", output)

    # Plot beam
    plt.figure(figsize=(10, 8))
    for beam in range(nof_beams):
        for chan in range(nof_channels):
            plt.plot(output[:, chan, beam], label="Channel {}, Beam {}".format(chan, beam))
    plt.ylabel("Arbitrary power")
    plt.xlabel("Time")
    plt.legend()
    plt.savefig("integrated_beams.png")


if __name__ == "__main__":
    # Command line options
    p = OptionParser()
    p.set_usage('offline_beamformer.py [options] INPUT_FILE')
    p.set_description(__doc__)

    p.add_option('-f', '--file', dest='file', action='store', default="channel_output.dat",
                 help="Data file to process (default: 'channel_output.dat')")
    p.add_option('-s', '--samples', dest='nof_samples', action='store', default=1048576,
                 type='int', help='Number of samples (default: 1048576)')
    p.add_option('-b', '--beams', dest='nof_beams', action='store', default=1,
                 type='int', help='Number of beams (default: 1)')
    p.add_option('--skip', dest='skip', action='store', default=1,
                  type='int', help='Number of sampels to skip per block (default: 0)')
    opts, args = p.parse_args(sys.argv[1:])

    beamformer(opts.file, opts.nof_samples, opts.nof_beams, opts.skip)

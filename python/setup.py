import distutils.log as log
import os

from setuptools import setup, find_packages
from setuptools.command.install import install


HOME = os.environ['HOME']
CONFIG_PATH = 'configuration'
DEPENDENCIES = ['matplotlib', 'configparser', 'enum34', 'numpy', 'pyfabil',
                 'pytz', 'astropy', 'configparser', 'ephem', 'h5py']
DEPENDENCIES_LINKS = [] # ['https://bitbucket.org/lessju/pyfabil/get/master.zip#egg=pyfabil']


def list_dir(root_path):
    """
    Iterate over all the files (including sub-directories) of the root path

    :param root_path:
    :return:
    """

    file_paths = []
    for item in os.listdir(root_path):
        file_path = os.path.join(root_path, item)
        if os.path.isfile(file_path):
            file_paths.append(file_path)

    return file_paths


class InstallWrapperCommand(install):
    """
    A wrapper function for the standard setuptools installation command
    in order to add Custom Pre and Post processing commands

    """
    LOCAL_DIRS = [
        '/var/log/frbs',
        os.path.join(HOME, '.best-frbs'),
        os.path.join(HOME, '.best-frbs/configuration'),
    ]

    def run(self):
        # Run the (custom) initialisation functions
        self._init()
        # Run the standard PyPi copy
        install.run(self)

    def _init(self):
        # Pre-processing section
        log.info('Initialising local directories in ~/.frbs')
        for d in self.LOCAL_DIRS:
            if not os.path.exists(d):
                log.info('Created directory in {}'.format(d))
                os.makedirs(d)

    def _check(self):
        # todo - add sanity checks here (check that all the dependencies are installed)
        pass


setup(
    name='pyfrbs',
    version='0.5',
    packages=find_packages(),
    license='',
    author='Alessio Magro',
    author_email='alessio.magro@um.edu.mt',
    description='Python software for the BEST FRBs',
    # include_package_data=True,
    zip_safe=False,
    install_requires=DEPENDENCIES,
    setup_requires=DEPENDENCIES,
    dependency_links=DEPENDENCIES_LINKS,
    cmdclass={
        'install': InstallWrapperCommand,  # override the standard installation command to add custom logic
    },
)
